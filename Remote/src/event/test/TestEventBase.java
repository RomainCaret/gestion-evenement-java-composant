package event.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import event.classes.EventBase;
import event.classes.samu.ConsciousFall;
import event.classes.samu.HealthAlarm;
import event.interfaces.AtomicEventI;
import event.interfaces.ComplexEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;

class TestEventBase {
	AtomicEventI ha1 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha2 = new HealthAlarm(LocalTime.now(), TypeOfHealthAlarm.MEDICAL, new AbsolutePosition(4, 4));
	ArrayList<EventI> list = new ArrayList<EventI>();
	ComplexEventI cs = new ConsciousFall(LocalTime.MIDNIGHT, list);
	EventBaseI eb = new EventBase();

	@Test
	void testNumberOfEventAndAdd() {
		assert eb.numberOfEvent() == 0 : "Erreur numberOfEvent";
		eb.addEvent(ha1);
		eb.addEvent(cs);
		assert eb.numberOfEvent() == 2 : "Erreur numberOfEvent";
		eb.addEvent(ha2);
		assert eb.numberOfEvent() == 3 : "Erreur numberOfEvent";
	}

	@Test
	void testAppearsIn() {
		assert eb.appearsIn(ha1) == false : "Erreur appearsIn";
		eb.addEvent(ha1);
		eb.addEvent(cs);
		assert eb.appearsIn(ha1) == true : "Erreur appearsIn1";
		assert eb.appearsIn(cs) == true : "Erreur appearsIn1";
		assert eb.appearsIn(ha2) == false : "Erreur appearsIn1";

	}

	@Test
	void testRemoveEvent() {
		eb.addEvent(ha1);
		eb.addEvent(cs);
		assert eb.appearsIn(ha1) == true : "Erreur appearsIn1";
		eb.removeEvent(ha1);
		assert eb.appearsIn(ha1) == false : "Erreur appearsIn2";
	}

	@Test
	void testGetEventIndex() {
		assert eb.getEvent(0) == null : "Erreur getEvent";
		eb.addEvent(ha1);
		eb.addEvent(cs);
		eb.addEvent(ha2);
		assert eb.getEvent(0) == cs : "Erreur getEvent";
		assert eb.getEvent(1) == ha1 : "Erreur getEvent";
		assert eb.getEvent(2) == ha2 : "Erreur getEvent";
	}

	@Test
	void testGetEvent() {
		assert eb.getEvent("HealthAlarm") == null : "Erreur getEvent";
		eb.addEvent(ha1);
		eb.addEvent(cs);
		eb.addEvent(ha2);
		assert eb.getEvent("HealthAlarm").get(0) == ha1 : "Erreur getEvent";
		assert eb.getEvent("HealthAlarm").get(1) == ha2 : "Erreur getEvent";
		assert eb.getEvent("HealthAlarm").size() == 2 : "Erreur getEvent";
	}


}
