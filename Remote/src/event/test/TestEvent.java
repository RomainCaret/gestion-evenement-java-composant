package event.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import event.classes.samu.ConsciousFall;
import event.classes.samu.HealthAlarm;
import event.interfaces.AtomicEventI;
import event.interfaces.ComplexEventI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;

class TestEvent {
	AtomicEventI ha1 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha2 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.MEDICAL, new AbsolutePosition(4, 4));
	ArrayList<EventI> list = new ArrayList<EventI>();
	ComplexEventI cs = new ConsciousFall(LocalTime.MIDNIGHT, list);

	@Test
	void testIfPutProperty() {
		assert ha1.hasProperty("test") == false : "Function put : Erreur HealthAlarm Property type";
		assert ha1.putProperty("test", "test") == "test" : "Function put : Erreur putProperty";
		assert ha1.hasProperty("test") == true : "Function put : Erreur HealthAlarm Property test";

	}

	@Test
	void testIfhasPropertyAtomicEvent() {
		assert ha1.hasProperty("test") == false : "Function put : Erreur HealthAlarm Property test";
		assert ha1.hasProperty("type") == true : "Function has :  Erreur HealthAlarm Property type";

	}

	@Test
	void testIfhasPropertyComplexEvent() {
		list.add(ha1);
		list.add(ha2);
		ha2.putProperty("testCs", "testCs");
		assert cs.hasProperty("type") == true : "testIfhasPropertyComplexEvent Property type";
		assert cs.hasProperty("test") == false : "testIfhasPropertyComplexEvent Property test";
		assert cs.hasProperty("testCs") == true : "testIfhasPropertyComplexEvent Property test";
	}

	@Test
	void testGetPropertyValue() {
		list.add(ha1);
		list.add(ha2);
		ha2.putProperty("testCs", "testCs");
		assert ha1.getPropertyValue("type") == TypeOfHealthAlarm.EMERGENCY
				: "testGetPropertyValue : Erreur HealthAlarm Property Value not Urgence";
		assert cs.getPropertyValue("testCs") == "testCs"
				: "testGetPropertyValue : Erreur HealthAlarm Property Value not Urgence";

	}

	@Test
	void testRemoveProperty() {
		ha2.putProperty("testCs", "testCs");
		assert ha2.hasProperty("testCs") == true : "testIfhasPropertyComplexEvent Property testCs";
		ha2.removeProperty("testCs");
		assert ha2.hasProperty("testCs") == false : "testRemoveProperty testCs";
	}

	@Test
	void testCorrelatedEvents() {
		assert cs.getCorrelatedEvents() == list : "Erreur getCorrelatedEvent list";
	}

	@Test
	void testTimeStamp() {
		assert ha1.getTimeStamp() == LocalTime.MIDNIGHT : "Erreur TimeStamp";
		assert cs.getTimeStamp() == LocalTime.MIDNIGHT : "Erreur TimeStamp";
		System.out.println("TestEventSuccess");
	}
}
