package event.classes.trafficLight;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;

public class RequestPriority extends AtomicEventAbstract {

	/**
	 * RequestPriority est le constructeur de la classe RequestPriority
	 * 
	 * @param time_stamp         de type LocalTime
	 * @param priority           de type TypeOfTrafficLightPriority
	 * @param destination_finale de type AbsolutePosition
	 * @param intersection       de type IntersectionPosition
	 * 
	 */

	public RequestPriority(LocalTime time_stamp, TypeOfTrafficLightPriority priority, String vehicleId,
			AbsolutePosition destination_finale, IntersectionPosition intersection) {
		super(time_stamp);
		putProperty("priority", priority);
		putProperty("intersection", intersection);
		putProperty("destinationFinale", destination_finale);
		putProperty("vehiculeId", vehicleId);

	}

}
