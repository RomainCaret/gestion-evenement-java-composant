package event.classes.trafficLight;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class WaitingVehiculePass extends ComplexEventAbstract {

	/**
	 * WaitingVehiculePass est le constructeur de la classe WaitingVehiculePass
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param l    de type ArrayList<EventI> une liste d'événements
	 * 
	 */
	public WaitingVehiculePass(LocalTime time_stamp, ArrayList<EventI> l) {
		super(time_stamp, l);

	}

	
}