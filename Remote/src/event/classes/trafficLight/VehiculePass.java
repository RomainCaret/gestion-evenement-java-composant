package event.classes.trafficLight;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;
import fr.sorbonne_u.cps.smartcity.grid.Direction;



public class VehiculePass extends AtomicEventAbstract {

	/**
	 * VehiculePass est le constructeur de la classe VehiculePass
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param direction    de type Direction
	 * 
	 */

	public VehiculePass(String vehiculeId, LocalTime time_stamp, Direction direction) {
		super(time_stamp);

		putProperty("direction", direction);
		putProperty("vehiculeId",vehiculeId );
	}

}
