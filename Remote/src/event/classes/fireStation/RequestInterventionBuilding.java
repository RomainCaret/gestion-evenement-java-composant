package event.classes.fireStation;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class RequestInterventionBuilding extends ComplexEventAbstract {

	/**
	 * RequestInterventionBuilding est le constructeur de la classe
	 * RequestInterventionBuilding
	 * 
	 * @param time_stamp de type LocalTime
	 * @param list       est une liste d'évenements
	 * @param type       de type String
	 */

	public RequestInterventionBuilding(LocalTime time_stamp, ArrayList<EventI> list) {
		super(time_stamp, list);
	}

}