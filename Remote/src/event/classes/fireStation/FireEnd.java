package event.classes.fireStation;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFire;

public class FireEnd extends AtomicEventAbstract {

	/**
	 * FireEnd est le constructeur de la classe FireEnd
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param localisation de type AbsolutePosition
	 * 
	 */

	public FireEnd(LocalTime time_stamp, fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition localisation) {
		super(time_stamp);
		putProperty("localisation", localisation);
	}

}
