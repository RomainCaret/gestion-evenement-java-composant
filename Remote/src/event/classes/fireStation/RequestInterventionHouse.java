package event.classes.fireStation;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class RequestInterventionHouse extends ComplexEventAbstract {

	/**
	 * RequestInterventionHouse est le constructeur de la classe RequestInterventionHouse
	 * 
	 * @param time_stamp de type LocalTime
	 * @param list       est une liste d'évenements
	 * @param type       de type String
	 */

	public RequestInterventionHouse(LocalTime time_stamp, ArrayList<EventI> list) {
		super(time_stamp, list);
	}

}