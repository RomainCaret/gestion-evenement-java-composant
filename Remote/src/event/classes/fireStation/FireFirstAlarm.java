package event.classes.fireStation;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class FireFirstAlarm extends ComplexEventAbstract {
	
	/**
	 * FireFirstAlarm est le constructeur de la classe FireFirstAlarm
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param l de type ArrayList<EventI>
	 * 
	 */

	public FireFirstAlarm(LocalTime time_stamp, ArrayList<EventI> l) {
		super(time_stamp, l);
	}

	
}