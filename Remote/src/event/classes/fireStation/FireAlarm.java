package event.classes.fireStation;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFire;

public class FireAlarm extends AtomicEventAbstract {

	/**
	 * FireAlarm est le constructeur de la classe FireAlarm
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param value        de type TypeOfFire
	 * @param position de type AbsolutePosition
	 * 
	 */

	public FireAlarm(LocalTime time_stamp, TypeOfFire value, AbsolutePosition position) {
		super(time_stamp);
		putProperty("type", value);
		putProperty("localisation", position);
	}

}
