package event.classes.fireStation;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class LadderAvailable extends AtomicEventAbstract {

	/**
	 * LadderAvailable est le constructeur de la classe LadderAvailable
	 * 
	 * @param time_stamp   de type LocalTime
	 * 
	 */
	
	public LadderAvailable(LocalTime time_stamp) {
		super(time_stamp);
	}

}
