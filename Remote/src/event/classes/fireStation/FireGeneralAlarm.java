package event.classes.fireStation;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class FireGeneralAlarm extends ComplexEventAbstract {

	/**
	 * FireGeneralAlarm est le constructeur de la classe FireGeneralAlarm
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param l de type ArrayList<EventI>
	 * 
	 */
	public FireGeneralAlarm(LocalTime time_stamp, ArrayList<EventI> l) {
		super(time_stamp, l);
		
	}

	
}
