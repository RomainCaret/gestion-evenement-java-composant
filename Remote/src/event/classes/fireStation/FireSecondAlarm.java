package event.classes.fireStation;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class FireSecondAlarm extends ComplexEventAbstract {

	
	/**
	 * FireSecondAlarm est le constructeur de la classe FireSecondAlarm
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param l de type ArrayList<EventI>
	 * 
	 */
	
	public FireSecondAlarm(LocalTime time_stamp, ArrayList<EventI> l) {
		super(time_stamp, l);
	}

}
