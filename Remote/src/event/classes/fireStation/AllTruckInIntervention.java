package event.classes.fireStation;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class AllTruckInIntervention extends AtomicEventAbstract {

	/**
	 * AllMedecinInIntervention est le constructeur de la classe
	 * AllMedecinInIntervention
	 * 
	 * @param time_stamp de type LocalTime
	 * 
	 */
	public AllTruckInIntervention(LocalTime time_stamp) {
		super(time_stamp);
	}

}
