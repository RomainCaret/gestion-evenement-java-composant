package event.classes.fireStation;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class TruckAvailable extends AtomicEventAbstract {

	/**
	 * TruckAvailable est le constructeur de la classe TruckAvailable
	 * 
	 * @param time_stamp de type LocalTime
	 */

	public TruckAvailable(LocalTime time_stamp) {
		super(time_stamp);
	}

}
