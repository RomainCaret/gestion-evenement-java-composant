package event.classes;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import descriptor.Data;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;

public class EventBase implements EventBaseI {

	protected Map<String, ArrayList<EventI>> base_event = new HashMap<String, ArrayList<EventI>>();

	public EventBase() {

	}

	/**
	 * addEvent est une méthode qui permet d'ajouter un événement dans la base des events
	 * @param e de type EventI et c'est l'énémet à ajouter
	 * @return void
	 */
	@Override
	public void addEvent(EventI e) {
		String key = e.getClass().getSimpleName();
		if (base_event.containsKey(key))
			base_event.get(key).add(e);
		else {
			ArrayList<EventI> event_list = new ArrayList<EventI>();
			event_list.add(e);
			base_event.put(key, event_list);
		}

	}
	
	/**
	 * removeEvent est une méthode qui permet de supprimer un événement de la base d'événements
	 * @param e de type EventI et c'est l'énémet à supprimer
	 * @return void
	 */

	@Override
	public void removeEvent(EventI e) {
		String key = e.getClass().getSimpleName();
		if (base_event.containsKey(key)) {
			if (base_event.get(key).remove(e) == false){
				System.out.println("Event doesn't exist");
			}
			else{
				if (base_event.get(key).size() == 0){
					base_event.remove(key);
				}
			}
				
		}
		
		else
			System.out.println("Event doesn't exist : "+e);

	}
	
	/**
	 * getEvent est une méthode qui permet de récupérer un événement selon son index
	 * @param i de type int et c'est l'index de l'événement qu'on veut récupérer
	 * @return EventI qui est l'événement correspondant à l'index i s'il existe
	 */

	@Override
	public EventI getEvent(int i) {
		int ct = 0;
		for (Map.Entry<String, ArrayList<EventI>> mapentry : base_event.entrySet()) {
			for (EventI iterator : mapentry.getValue()) {
				if (ct == i)
					return iterator;
				ct++;
			}
		}
		return null;
	}
	
	/**
	 * getEvent est une méthode qui permet de récupérer une liste d'événements selon des clés
	 * @param key de type String et c'est la clé de l'événement qu'on veut récupérer
	 * @return ArrayList<EventI> une liste d'événements
	 */

	@Override
	public ArrayList<EventI> getEvent(String key) {
		return base_event.get(key);
	}

	/**
	 * numberOfEvent est une méthode qui retourne le nombre d'événements dans une base d'event
	 * @return int qui est le nombre d'événements
	 */
	@Override
	public int numberOfEvent() {
		int taille = 0;
		/*for (Map.Entry<String, ArrayList<EventI>> mapentry : base_event.entrySet()) {
			taille += mapentry.getValue().size();
		}
		System.out.println(taille);
		return taille;
		*/
		for (String map_entry : base_event.keySet()){
			taille+= base_event.get(map_entry).size();
		}

		return taille;

	}

	/**
	 * appearsIn est une méthode qui permet de test si un événement apparait dans la base d'event
	 * @param e de type EventI et c'est l'événement qu'on cherche
	 * @return boolean
	 * 
	 */
	@Override
	public boolean appearsIn(EventI e) {
		String key = e.getClass().getSimpleName();
		if (base_event.containsKey(key)) {
			return base_event.get(key).contains(e);
		}
		return false;
	}
	
	/**
	 * clearEvent est une méthode qui permet de supprimer un événement après une durée précise
	 * @param d de type Duration
	 * @return void
	 */

	@Override
	public void clearEvent(Duration d) {
		if (d == null) {
			base_event.clear();
			return;
		}
		LocalTime now = Data.getCurrentTime();
		LocalTime time_delete = now.minus(d);
		ArrayList<EventI> list_delete = new ArrayList<EventI>();

		for (String map_entry : base_event.keySet()){
			ArrayList<EventI> list_event = base_event.get(map_entry);
			for (EventI iterator : list_event) {
				if (iterator.getTimeStamp().isAfter(time_delete)) {
					list_delete.add(iterator);
				}

			}
		}
		for (EventI iterator : list_delete) {
			removeEvent(iterator);
		}
	}

}
