package event.classes.samu;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class RequestIntervention extends ComplexEventAbstract {

	/**
	 * RequestIntervention est le constructeur de la classe RequestIntervention
	 * 
	 * @param time_stamp de type LocalTime
	 * @param list       est une liste d'évenements
	 */

	public RequestIntervention(LocalTime time_stamp, ArrayList<EventI> list) {
		super(time_stamp, list);
		
	}

}