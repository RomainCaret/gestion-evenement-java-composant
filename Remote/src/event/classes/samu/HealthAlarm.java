package event.classes.samu;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;

public class HealthAlarm extends AtomicEventAbstract {

	/**
	 * HealthAlarm est le constructeur de la classe HealthAlarm
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param value        de type TypeOfHealthAlarm
	 * @param localisation de type AbsolutePosition
	 * 
	 */

	public HealthAlarm(LocalTime time_stamp, TypeOfHealthAlarm value, AbsolutePosition localisation) {
		super(time_stamp);
		putProperty("type", value);
		putProperty("localisation", localisation);
	}

}
