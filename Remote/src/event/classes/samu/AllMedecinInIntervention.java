package event.classes.samu;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class AllMedecinInIntervention extends AtomicEventAbstract {

	/**
	 * AllMedecinInIntervention est le constructeur de la classe
	 * AllMedecinInIntervention
	 * 
	 * @param time_stamp de type LocalTime
	 * 
	 */
	public AllMedecinInIntervention(LocalTime time_stamp) {
		super(time_stamp);
	}

}
