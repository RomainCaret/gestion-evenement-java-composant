package event.classes.samu;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class AllSamuInIntervention extends AtomicEventAbstract {

	/**
	 * AllSamuInIntervention est le constructeur de la classe AllSamuInIntervention
	 * 
	 * @param time_stamp de type LocalTime
	 * 
	 */
	public AllSamuInIntervention(LocalTime time_stamp) {
		super(time_stamp);
	}

}
