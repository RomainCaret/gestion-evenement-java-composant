package event.classes.samu;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class SamuAvailable extends AtomicEventAbstract {

	/**
	 * SamuAvailable est le constructeur de la classe SamuAvailable
	 * 
	 * @param time_stamp de type LocalTime
	 * 
	 */
	public SamuAvailable(LocalTime time_stamp) {
		super(time_stamp);
	}

}
