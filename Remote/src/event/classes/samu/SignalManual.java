package event.classes.samu;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class SignalManual extends AtomicEventAbstract {

	/**
	 * SignalManual est le constructeur de la classe SignalManual
	 * 
	 * @param time_stamp   de type LocalTime
	 * @param personID       de type String
	 * 
	 */
	public SignalManual(LocalTime time_stamp, String personID) {
		super(time_stamp);
		putProperty("personID", personID);
	}

}
