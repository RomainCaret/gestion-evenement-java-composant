package event.classes.samu;

import java.time.LocalTime;

import event.abstracts.AtomicEventAbstract;

public class MedecinAvailable extends AtomicEventAbstract {

	/**
	 * MedecinAvailable est le constructeur de la classe MedecinAvailable
	 * 
	 * @param time_stamp de type LocalTime
	 * 
	 * 
	 */
	public MedecinAvailable(LocalTime time_stamp) {
		super(time_stamp);
	}

}
