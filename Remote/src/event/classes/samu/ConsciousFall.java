package event.classes.samu;

import java.time.LocalTime;
import java.util.ArrayList;

import event.abstracts.ComplexEventAbstract;
import event.interfaces.EventI;

public class ConsciousFall extends ComplexEventAbstract{
	
	/**
	 * ConsciousFall est le constructeur de la classe ConsciousFall
	 * @param time_stamp de type LocalTime
	 * @param l est une liste de EventI
	 */

	public ConsciousFall(LocalTime time_stamp, ArrayList<EventI> l) {
		super(time_stamp, l);
		
	}
	
}
