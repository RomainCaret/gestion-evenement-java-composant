package event.abstracts;

import java.io.Serializable;
import java.time.LocalTime;

import java.util.HashMap;
import java.util.Map;

import event.interfaces.EventI;

public abstract class EventAbstract implements EventI {

	protected Map<String, Serializable> event_properties = new HashMap<String, Serializable>();
	protected LocalTime time_stamp;

	/**
	 * EventAbstract est le consrtructeur de la classe EventAbstract
	 * 
	 * @param time_stamp est de type LocalTime
	 */
	public EventAbstract(LocalTime time_stamp) {
		this.time_stamp = time_stamp;
	}

	/**
	 * getTimeStamp est une méthode qui permet de récupérer un LocalTime
	 * 
	 * @return time_stamp de type LocalTime
	 */
	@Override
	public LocalTime getTimeStamp() {
		return time_stamp;
	}

	/**
	 * hasProperty est une méthode qui test si une propriété existe
	 * 
	 * @param name de type String qui est le nom de la propriété
	 * @return boolean
	 */
	@Override
	public boolean hasProperty(String name) {
		return event_properties.containsKey(name);
	}

	/**
	 * getPropertyValue est une méthode qui permet de récupérer le type d'une
	 * propriété
	 * 
	 * @param name de type String correspondant au nom de la propriété
	 * @return Serialisable
	 */

	@Override
	public Serializable getPropertyValue(String name) {
		return event_properties.get(name);
	}

}
