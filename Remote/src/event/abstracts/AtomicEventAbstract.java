package event.abstracts;

import java.io.Serializable;
import java.time.LocalTime;

import event.interfaces.AtomicEventI;

public abstract class AtomicEventAbstract extends EventAbstract implements AtomicEventI {

	/**
	 * AtomicEventAbstract est le constructeur de la classe AtomicEventAbstract
	 * @param time_stamp de type LocalTime
	 */
	public AtomicEventAbstract(LocalTime time_stamp) {
		super(time_stamp);
	}
	
	/**
	 * putProperty est une méthode qui permet d'ajouter une propriété à un évenement atomique, elle prend deux paramètres qui sont :
	 * @param name de type String qui est le nom de la propriété
	 * @param value de type Serialisable qui est le type de l'événement
	 * @return value de type Serializable
	 */

	public Serializable putProperty(String name, Serializable value) {
		super.event_properties.put(name, value);

		return value;
	}
	
	/**
	 * removeProperty est une méthode qui permet de supprimer une propriété d'un événement 
	 * @param name de type String correspondant au nom de la propriété
	 */

	public void removeProperty(String name) {
		super.event_properties.remove(name);
	}

}
