package event.abstracts;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;

import event.interfaces.ComplexEventI;
import event.interfaces.EventI;

public abstract class ComplexEventAbstract extends EventAbstract implements ComplexEventI {

	protected ArrayList<EventI> list_event;

	/**
	 * ComplexEventAbstract est le constructeur de la classe ComplexEventAbstract
	 * @param time_stamp de type LocalTime
	 * @param l qui est une liste de EventI
	 */
	public ComplexEventAbstract(LocalTime time_stamp, ArrayList<EventI> l) {
		super(time_stamp);
		this.list_event = l;
	}
	
	/**
	 * getCorrelatedEvents est une méthode qui permet de récupérer les évenements corrélés 
	 * @return list_event de type ArrayList<EventI> qui est une liste d'événements
	 */

	@Override
	public ArrayList<EventI> getCorrelatedEvents() {
		return list_event;
	}

	/**
	 * hasProperty est une méthode qui test si une propriété existe
	 * @param name de type String qui est le nom de la propriété
	 * @return boolean
	 */
	@Override
	public boolean hasProperty(String name) {
		for (EventI iterator : list_event) {
			if (iterator.hasProperty(name))
				return true;
		}

		return event_properties.containsKey(name);

	}
	
	/**
	 * getPropertyValue est une méthode qui permet de récupérer le type d'une propriété
	 * @param name de type String correspondant au nom de la propriété
	 * @return Serialisable
	 */

	@Override
	public Serializable getPropertyValue(String name) {
		for (EventI iterator : list_event) {
			if (iterator.hasProperty(name))
				return iterator.getPropertyValue(name);
		}
		if (super.hasProperty(name))
			return event_properties.get(name);
		return null;
	}
}
