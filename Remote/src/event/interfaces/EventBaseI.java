package event.interfaces;

import java.time.Duration;
import java.util.ArrayList;

public interface EventBaseI {
	public void addEvent(EventI e);
	public void removeEvent(EventI e);
	public EventI getEvent(int i);
	public int numberOfEvent();
	public boolean appearsIn(EventI e);
	public void clearEvent (Duration d);
	public ArrayList<EventI> getEvent(String key);
}
