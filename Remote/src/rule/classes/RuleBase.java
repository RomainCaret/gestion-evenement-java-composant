package rule.classes;

import java.util.ArrayList;

import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import rule.interfaces.CorrelatorStateI;
import rule.interfaces.RuleBaseI;
import rule.interfaces.RuleI;

public class RuleBase implements RuleBaseI {
	protected ArrayList<RuleI> list_rule;

	/**
	 * RuleBase est le constructeur de la classe RuleBase
	 */
	public RuleBase() {
		list_rule = new ArrayList<RuleI>();
	}

	/**
	 * addRule est une méthode qui permet d'ajouter une règle dans la base de
	 * règles, elle ne retourne rien
	 * 
	 * @param rule de type RuleI
	 * @return void
	 */
	public void addRule(RuleI rule) {
		list_rule.add(rule);
	}

	/**
	 * fireFirstOn une fonction qui permet de chercher la première règle qui peut
	 * être activée, on parcourt toutes les règles à la recherche d'une règle à
	 * réaliser puis on remet les itérateurs des champs de la classe au debut
	 * ensuite au retour de match les itérateurs repartent depuis là où ils se sont
	 * arrêtés et permettent de ne pas reprendre les même events pour prendre toutes
	 * les combinaisons de match possibles
	 * 
	 * @param eb de type EventBaseI qui est la base d'événements
	 * @param c de type CorrelatorStateI
	 * @return boolean
	 */

	// Fonction qui cherche la premiere regle qui peut être activée
	public boolean fireFirstOn(EventBaseI eb, CorrelatorStateI c) {
		ArrayList<EventI> matched_ev;
		// On parcourt toutes les règles à la recherche d'une règle à réaliser
		for (RuleI ruleI : list_rule) {
			// Remets les iterateurs des champs de la classe au debut
			// Au retour de match les iterateurs repartent depuis là où ils se sont arretés
			// et permettent
			// de ne pas reprendre les même events pour prendre toutes les combinaisons de
			// matchs possibles
			ruleI.set_start();
			while ((matched_ev = ruleI.match(eb)) != null) {
				// Gestion de match multiples
				if (ruleI.correlate(matched_ev) && ruleI.filter(matched_ev, c)) {
					ruleI.act(matched_ev, c);
					ruleI.update(matched_ev, eb);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * fireAllOn une fonction qui permet de chercher toutes les règles qui peuvent
	 * être activées, on parcourt toutes les règles à la recherche d'une règle à
	 * réaliser puis on remet les itérateurs des champs de la classe au debut
	 * ensuite au retour de match les itérateurs repartent depuis là où ils se sont
	 * arrêtés et permettent de ne pas reprendre les mêmes events pour prendre toutes
	 * les combinaisons de match possibles
	 * 
	 * @param eb de type EventBaseI qui est la base d'événements
	 * @param c de type CorrelatorStateI
	 * @return boolean
	 */
	// Fonction qui cherche toutes les regles qui peuvent être activées
	public boolean fireAllOn(EventBaseI eb, CorrelatorStateI c) {
		
		ArrayList<EventI> matched_ev;
		boolean at_least_one_rule = false;
		// On parcourt toutes les règles à la recherche d'une règle à réaliser
		for (RuleI ruleI : list_rule) {
			// Remets les iterateurs des champs de la classe au debut
			// Au retour de match les iterateurs repartent depuis là où ils se sont arretés
			// et permettent
			// de ne pas reprendre les même events pour prendre toutes les combinaisons de
			// matchs possibles
			ruleI.set_start();
			while ((matched_ev = ruleI.match(eb)) != null) {
				// Gestion de match multiples
				if (ruleI.correlate(matched_ev) && ruleI.filter(matched_ev, c)) {
					ruleI.act(matched_ev, c);
					ruleI.update(matched_ev, eb);
					at_least_one_rule = true;
				}
			}
		}
		return at_least_one_rule;
	}
}
