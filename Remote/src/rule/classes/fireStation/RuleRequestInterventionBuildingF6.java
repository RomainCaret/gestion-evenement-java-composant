package rule.classes.fireStation;

import java.util.ArrayList;

import correlator.classe.FireCorrelatorState;
import descriptor.Data;
import event.classes.samu.RequestIntervention;
import event.interfaces.AtomicEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleRequestInterventionBuildingF6 extends RuleAbstract {

	public RuleRequestInterventionBuildingF6() {
		list_iterator.add(0);
	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {

		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listFA = eb.getEvent("RequestInterventionBuilding");
		if (listFA == null)
			return null;
		while (list_iterator.get(0) < listFA.size()) {
			event = listFA.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			list_matched_event.add(event);
			return list_matched_event;
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		return true;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		FireCorrelatorState fc = (FireCorrelatorState) c;
		if (!fc.isLadder_available()) {
			String next_barracks_available = fc.getBarracks_nearby_available(matchedEvents.get(0));
			if (next_barracks_available != null) {
				((AtomicEventI) matchedEvents.get(0)).putProperty("nextBarracks", next_barracks_available);
				return true;
			}
		}
		return false;
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleF6");
		FireCorrelatorState fc = (FireCorrelatorState) c;
		ArrayList<String> list_Barrack_Deja_Visit;
		if (((EventI) matchedEvents.get(0)).hasProperty("BarrackDejaVisit")) {
			list_Barrack_Deja_Visit = (ArrayList<String>) ((EventI) matchedEvents.get(0))
					.getPropertyValue("BarrackDejaVisit");
			list_Barrack_Deja_Visit.add(fc.getExec_uri());
		} else {
			list_Barrack_Deja_Visit = new ArrayList<String>();
			list_Barrack_Deja_Visit.add(fc.getExec_uri());
			((AtomicEventI) matchedEvents.get(0)).putProperty("BarrackDejaVisit", list_Barrack_Deja_Visit);
		}
		fc.sendEvent(matchedEvents.get(0));
	}
}
