package rule.classes.fireStation;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;

import correlator.classe.FireCorrelatorState;
import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.classes.fireStation.FireSecondAlarm;
import event.classes.samu.RequestIntervention;
import event.interfaces.AtomicEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFire;

public class RuleFireFirstAlarmF12 extends RuleAbstract {

	public RuleFireFirstAlarmF12() {
		list_iterator.add(0);
		list_iterator.add(0);

	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listHA1 = eb.getEvent("FireFirstAlarm");
		ArrayList<EventI> listHA2 = eb.getEvent("FireAlarm");

		
		if (listHA1 == null || listHA2 == null)
			return null;
		
		while (list_iterator.get(0) < listHA1.size()) {
			event = listHA1.get(list_iterator.get(0));
			if (event.getPropertyValue("type") == TypeOfFire.House) {
				list_matched_event.add(event);
			
				while (list_iterator.get(1) < listHA2.size()) {
					event = listHA2.get(list_iterator.get(1));
					list_iterator.set(1, list_iterator.get(1) + 1);
					if (event.getPropertyValue("type") == TypeOfFire.House) {

						list_matched_event.add(event);
						return list_matched_event;
					}
				}
				list_iterator.set(1, 0);
			}
			list_matched_event.clear();
			list_iterator.set(0, list_iterator.get(0) + 1);
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {

		if (matchedEvents.get(0).getPropertyValue("localisation")
				.equals(matchedEvents.get(1).getPropertyValue("localisation"))) {
			Duration d = Duration.ofMinutes(15);
			LocalTime t0 = matchedEvents.get(0).getTimeStamp();
			LocalTime t1 = matchedEvents.get(1).getTimeStamp();
			if (t0.isAfter(t1)) {
				t1 = t1.plus(d);
				return t0.isBefore(t1);
			}
			t0 = t0.plus(d);
			return t1.isBefore(t0);
		}

		return false;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		FireCorrelatorState fc = (FireCorrelatorState) c;
		if (!fc.isTruck_available()) {
			String next_barracks_available = fc.getBarracks_nearby_available(matchedEvents.get(0));
			if (next_barracks_available != null) {
				((AtomicEventI) matchedEvents.get(0)).putProperty("nextBarracks", next_barracks_available);
				return true;
			}
		}
		return false;

	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleF12");
		FireCorrelatorState fc = (FireCorrelatorState) c;
		ArrayList<String> list_Barrack_Deja_Visit;
		if (((EventI) matchedEvents.get(0)).hasProperty("BarrackDejaVisit")) {
			list_Barrack_Deja_Visit = (ArrayList<String>) ((EventI) matchedEvents.get(0))
					.getPropertyValue("BarrackDejaVisit");
			list_Barrack_Deja_Visit.add(fc.getExec_uri());
		} else {
			list_Barrack_Deja_Visit = new ArrayList<String>();
			list_Barrack_Deja_Visit.add(fc.getExec_uri());
			((AtomicEventI) matchedEvents.get(0)).putProperty("BarrackDejaVisit", list_Barrack_Deja_Visit);
		}
		FireSecondAlarm ri = new FireSecondAlarm(matchedEvents.get(0).getTimeStamp(), matchedEvents);
		fc.sendEvent(ri);

	}

	@Override
	public void update(ArrayList<EventI> matchedEvents, EventBaseI eb) {
		FireSecondAlarm fga = new FireSecondAlarm(matchedEvents.get(0).getTimeStamp(), matchedEvents);
		eb.removeEvent(matchedEvents.get(0));
		list_iterator.set(0, 0);
		eb.addEvent(fga);
	}
}
