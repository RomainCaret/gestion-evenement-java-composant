package rule.classes.fireStation;

import java.util.ArrayList;

import correlator.classe.FireCorrelatorState;
import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleAllTruckInInterventionF16 extends RuleAbstract {

	public RuleAllTruckInInterventionF16() {
		list_iterator.add(0);

	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listHA = eb.getEvent("AllTruckInIntervention");
		if (listHA == null)
			return null;
		while (list_iterator.get(0) < listHA.size()) {
			event = listHA.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			list_matched_event.add(event);
			return list_matched_event;

		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		return true;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		return true;
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleF16");
		FireCorrelatorState fc = (FireCorrelatorState) c;
		fc.setTruck_available(false);
	}

}
