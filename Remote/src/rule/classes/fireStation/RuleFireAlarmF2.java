package rule.classes.fireStation;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;

import correlator.classe.FireCorrelatorState;
import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.classes.fireStation.FireFirstAlarm;
import event.interfaces.AtomicEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFire;

public class RuleFireAlarmF2 extends RuleAbstract {

	public RuleFireAlarmF2() {
		list_iterator.add(0);

	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listFA = eb.getEvent("FireAlarm");
		if (listFA == null)
			return null;
		while (list_iterator.get(0) < listFA.size()) {
			event = listFA.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			if (event.getPropertyValue("type") == TypeOfFire.House) {
				list_matched_event.add(event);
				return list_matched_event;
			}
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {

		return true;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		FireCorrelatorState fc = (FireCorrelatorState) c;
		if (fc.inZone((AbsolutePosition) (matchedEvents.get(0).getPropertyValue("localisation")))) {
			if (fc.isTruck_available()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleF2");
		FireCorrelatorState fc = (FireCorrelatorState) c;
		try {
			fc.triggerHouseFirstAlarm((AbsolutePosition) matchedEvents.get(0).getPropertyValue("localisation"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(ArrayList<EventI> matchedEvents, EventBaseI eb) {
		FireFirstAlarm fa = new FireFirstAlarm(matchedEvents.get(0).getTimeStamp(), matchedEvents);
		eb.removeEvent(matchedEvents.get(0));
		list_iterator.set(0, 0);
		eb.addEvent(fa);
	}
}