package rule.classes.fireStation;

import java.util.ArrayList;
import correlator.classe.FireCorrelatorState;
import descriptor.Data;
import event.classes.fireStation.FireSecondAlarm;
import event.interfaces.AtomicEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleFireSecondAlarmF14bis extends RuleAbstract {

	public RuleFireSecondAlarmF14bis() {
		list_iterator.add(0);
	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listFA = eb.getEvent("FireSecondAlarm");
		if (listFA == null)
			return null;
		while (list_iterator.get(0) < listFA.size()) {
			event = listFA.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			list_matched_event.add(event);
			return list_matched_event;
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		return true;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		FireCorrelatorState fc = (FireCorrelatorState) c;
		if (!fc.isTruck_available()) {
			String next_barracks_available = fc.getBarracks_nearby_available(matchedEvents.get(0));
			if (next_barracks_available == null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleF14bis");
	}

}
