package rule.classes.fireStation;

import java.util.ArrayList;

import correlator.classe.FireCorrelatorState;
import descriptor.Data;
import event.classes.fireStation.FireEnd;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleFireGeneralAlarmF21 extends RuleAbstract {

	public RuleFireGeneralAlarmF21() {
		list_iterator.add(0);
		list_iterator.add(0);

	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listHA1 = eb.getEvent("FireGeneralAlarm");
		ArrayList<EventI> listHA2 = eb.getEvent("FireEnd");

		if (listHA1 == null || listHA2 == null)
			return null;
		
		while (list_iterator.get(0) < listHA1.size()) {
			event = listHA1.get(list_iterator.get(0));
			list_matched_event.add(event);
		
			while (list_iterator.get(1) < listHA2.size()) {
				event = listHA2.get(list_iterator.get(1));
				list_iterator.set(1, list_iterator.get(1) + 1);
				list_matched_event.add(event);
				return list_matched_event;
			}
			list_matched_event.clear();
			list_iterator.set(1, 0);
			list_iterator.set(0, list_iterator.get(0) + 1);
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {

		if (matchedEvents.get(0).getPropertyValue("localisation")
				.equals(matchedEvents.get(1).getPropertyValue("localisation"))) {

			return true;
		}
		return false;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		return true;
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleF21");
		FireCorrelatorState fc = (FireCorrelatorState) c;
		try {
			fc.triggerFireGeneralAlarm((AbsolutePosition)(matchedEvents.get(0).getPropertyValue("localisation")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AbsolutePosition loc = (AbsolutePosition) matchedEvents.get(0).getPropertyValue("localisation");
		FireEnd fe = new FireEnd(matchedEvents.get(0).getTimeStamp(), loc);
		fc.sendEvent(fe);
	}

}
