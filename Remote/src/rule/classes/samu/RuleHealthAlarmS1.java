package rule.classes.samu;

import java.util.ArrayList;

import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfSAMURessources;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;


public class RuleHealthAlarmS1 extends RuleAbstract {

	public RuleHealthAlarmS1() {
		list_iterator.add(0);
	}

	/**
	 * match est une méthode qui définit les événements devant être présents pour
	 * activer une règle
	 * 
	 * @param eb de type EventBaseI et c'est la base d'événéments
	 * @return ArrayList<EventI> la liste d'événements pouvants activer une règle
	 */

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listHA = eb.getEvent("HealthAlarm");
		if (listHA == null)
			return null;
		while (list_iterator.get(0) < listHA.size()) {
			event = listHA.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			if (event.getPropertyValue("type") == TypeOfHealthAlarm.EMERGENCY) {
				list_matched_event.add(event);
				return list_matched_event;
			}
			list_matched_event.clear();
		}
		return null;
	}

	/**
	 * correlate est une méthode permettant de vérifier des conditions entre les
	 * événements appariés telle que le fait qu’ils se sont produits à la même
	 * position
	 * 
	 * @param matchedEvents de type ArrayList<EventI> et c'est la liste des
	 *                      événements pouvants activer une règle
	 * @return boolean
	 */

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		return true;
	}

	/**
	 * filter est une méthode de filtrage qui vérifie des conditions ne faisant pas
	 * partie des événements mais plutôt du contexte ou de l’environnement
	 * 
	 * @param matchedEvents de type ArrayList<EventI> et c'est la liste des
	 *                      événements pouvants activer une règle
	 * @param c             de type CorrelatorStateI
	 * @return boolean
	 */

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		HealthCorrelatorState hc = (HealthCorrelatorState) c;
		if (hc.inZone((AbsolutePosition) (matchedEvents.get(0).getPropertyValue("localisation")))) {
			if (hc.isSamu_available()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * act est une méthode qui permet d’exécuter des actions sur le système comme
	 * déclencher des alarmes ou de propager des événements vers d’autres
	 * corrélateurs
	 * 
	 * @param matchedEvents de type ArrayList<EventI> et c'est la liste des
	 *                      événements pouvants activer une règle
	 * @param c             de type CorrelatorStateI
	 * @return void
	 */

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if(Data.DEBUG_RULE)
			System.out.println("Application RuleS1");
		HealthCorrelatorState hc = (HealthCorrelatorState) c;
		try {
			hc.triggerIntervention((AbsolutePosition)matchedEvents.get(0).getPropertyValue("localisation"),
					(String)matchedEvents.get(0).getPropertyValue("personID"), TypeOfSAMURessources.AMBULANCE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
