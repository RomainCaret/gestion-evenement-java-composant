package rule.classes.samu;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;

import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.classes.samu.ConsciousFall;
import event.interfaces.AtomicEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleHealthAlarmS8 extends RuleAbstract {

	public RuleHealthAlarmS8() {
		list_iterator.add(0);
		list_iterator.add(0);

	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		
		ArrayList<EventI> listHA1 = eb.getEvent("HealthAlarm");
		ArrayList<EventI> listHA2 = eb.getEvent("SignalManual");

		
		if (listHA1 == null || listHA2 == null)
			return null;
		
		while (list_iterator.get(0) < listHA1.size()) {
			event = listHA1.get(list_iterator.get(0));
			if (event.getPropertyValue("type") == TypeOfHealthAlarm.TRACKING) {
				list_matched_event.add(event);
				
				while (list_iterator.get(1) < listHA2.size()) {
					event = listHA2.get(list_iterator.get(1));
					list_iterator.set(1, list_iterator.get(1) + 1);
					list_matched_event.add(event);
					return list_matched_event;
				}
				list_iterator.set(1, 0);
			}
			list_matched_event.clear();
			list_iterator.set(0, list_iterator.get(0) + 1);
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {

		if (matchedEvents.get(0).getPropertyValue("personID")
				.equals(matchedEvents.get(1).getPropertyValue("personID"))) {
			Duration d = Duration.ofMinutes(10);
			LocalTime t0 = matchedEvents.get(0).getTimeStamp();
			LocalTime t1 = matchedEvents.get(1).getTimeStamp();
			if (t0.isAfter(t1)) {
				t1 = t1.plus(d);
				return t0.isBefore(t1);
			}
			t0 = t0.plus(d);
			return t1.isBefore(t0);
		}

		return false;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		HealthCorrelatorState hc = (HealthCorrelatorState) c;
		if (!hc.isMedecin_available()) {
			String next_samu_available = hc.getSamu_nearby_available(matchedEvents.get(0));
			if (next_samu_available != null) {
				((AtomicEventI) matchedEvents.get(0)).putProperty("nextStation", next_samu_available);
				return true;
			}
		}
		return false;

	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleS8");
		ArrayList<String> list_Samu_Deja_Visit;
		HealthCorrelatorState hc = (HealthCorrelatorState) c;

		if (((EventI) matchedEvents.get(0)).hasProperty("SamuDejaVisit")) {

			list_Samu_Deja_Visit = (ArrayList<String>) ((EventI) matchedEvents.get(0))
					.getPropertyValue("SamuDejaVisit");
			list_Samu_Deja_Visit.add(hc.getExec_uri());
		} else {
			list_Samu_Deja_Visit = new ArrayList<String>();
			list_Samu_Deja_Visit.add(hc.getExec_uri());
			((AtomicEventI) matchedEvents.get(0)).putProperty("SamuDejaVisit", list_Samu_Deja_Visit);
		}

		ConsciousFall cf = new ConsciousFall(matchedEvents.get(0).getTimeStamp(), matchedEvents);
		hc.sendEvent(cf);

	}

}
