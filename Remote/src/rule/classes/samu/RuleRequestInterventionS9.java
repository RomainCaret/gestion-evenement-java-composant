package rule.classes.samu;

import java.util.ArrayList;

import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfSAMURessources;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;


public class RuleRequestInterventionS9 extends RuleAbstract {

	
	public RuleRequestInterventionS9() {
		list_iterator.add(0);
	}
	
	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listRI = eb.getEvent("RequestIntervention");
		if (listRI == null)
			return null;
		while (list_iterator.get(0) < listRI.size()) {
			event = listRI.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			if (event.getPropertyValue("type") == TypeOfHealthAlarm.EMERGENCY) {
				list_matched_event.add(event);
				return list_matched_event;
			}
			list_matched_event.clear();
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		return true;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		HealthCorrelatorState hc = (HealthCorrelatorState) c;
		return hc.isSamu_available();
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleS9");
		HealthCorrelatorState hc = (HealthCorrelatorState) c;
		try {
			hc.triggerIntervention((AbsolutePosition) (matchedEvents.get(0).getPropertyValue("localisation")),
					(String) matchedEvents.get(0).getPropertyValue("personID"), TypeOfSAMURessources.AMBULANCE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
