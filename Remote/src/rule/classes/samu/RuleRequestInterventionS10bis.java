package rule.classes.samu;

import java.util.ArrayList;

import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleRequestInterventionS10bis extends RuleAbstract {

	
	public RuleRequestInterventionS10bis() {
		list_iterator.add(0);
	}
	
	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listHA = eb.getEvent("RequestIntervention");
		if (listHA == null)
			return null;
		
		while (list_iterator.get(0) < listHA.size()) {
			event = listHA.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			if (event.getPropertyValue("type") == TypeOfHealthAlarm.EMERGENCY) {
				list_matched_event.add(event);
				return list_matched_event;
			}
			list_matched_event.clear();
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		return true;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		HealthCorrelatorState hc = (HealthCorrelatorState) c;
		
		if (!hc.isSamu_available()) {
			String next_samu_available = hc.getSamu_nearby_available(matchedEvents.get(0));
			if (next_samu_available == null) {
				return true;
			}
		}
		return false;

	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleS10bis");
	}

}
