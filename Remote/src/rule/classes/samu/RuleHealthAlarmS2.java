package rule.classes.samu;

import java.time.LocalTime;
import java.util.ArrayList;

import correlator.classe.FireCorrelatorState;
import correlator.classe.HealthCorrelatorState;
import descriptor.Data;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import event.classes.samu.RequestIntervention;
import event.interfaces.AtomicEventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleHealthAlarmS2 extends RuleAbstract {

	public RuleHealthAlarmS2() {
		list_iterator.add(0);

	}

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listHA = eb.getEvent("HealthAlarm");
		if (listHA == null)
			return null;
		while (list_iterator.get(0) < listHA.size()) {
			event = listHA.get(list_iterator.get(0));
			list_iterator.set(0, list_iterator.get(0) + 1);
			if (event.getPropertyValue("type") == TypeOfHealthAlarm.EMERGENCY) {
				list_matched_event.add(event);
				return list_matched_event;
			}
			list_matched_event.clear();
		}
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		return true;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {

		HealthCorrelatorState hc = (HealthCorrelatorState) c;
		if (hc.inZone((AbsolutePosition) (matchedEvents.get(0).getPropertyValue("localisation")))) {
			if (!hc.isSamu_available()) {
				
				String next_samu_available=hc.getSamu_nearby_available(matchedEvents.get(0));
				if (next_samu_available != null) {
				((AtomicEventI) matchedEvents.get(0)).putProperty("nextStation",next_samu_available);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if(Data.DEBUG_RULE)
			System.out.println("Application RuleS2");
		ArrayList<String> list_Samu_Deja_Visit;
		HealthCorrelatorState hc = (HealthCorrelatorState) c;

		if (((EventI) matchedEvents.get(0)).hasProperty("SamuDejaVisit")) {

			list_Samu_Deja_Visit = (ArrayList<String>) ((EventI) matchedEvents.get(0))
					.getPropertyValue("SamuDejaVisit");
			list_Samu_Deja_Visit.add(hc.getExec_uri());
		} else {
			list_Samu_Deja_Visit = new ArrayList<String>();
			list_Samu_Deja_Visit.add(hc.getExec_uri());
			((AtomicEventI) matchedEvents.get(0)).putProperty("SamuDejaVisit", list_Samu_Deja_Visit);

		}
		RequestIntervention ri = new RequestIntervention(LocalTime.now(), matchedEvents);
		if (Data.test)
			hc.sendEvent(ri);
	}

}
