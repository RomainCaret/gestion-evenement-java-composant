package rule.classes.trafficLight;

import java.util.ArrayList;

import correlator.classe.TrafficLightCorrelatorState;
import descriptor.Data;
import event.classes.trafficLight.WaitingVehiculePass;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;
import rule.abstracts.RuleAbstract;
import rule.interfaces.CorrelatorStateI;

public class RuleTrafficLightC4 extends RuleAbstract {

	public RuleTrafficLightC4() {
		list_iterator.add(0);
		list_iterator.add(0);
	}

	/**
	 * match est une méthode qui défini les événements devant être présents pour
	 * activer une règle
	 * 
	 * @param eb de type EventBaseI et c'est la base d'événéments
	 * @return ArrayList<EventI> la liste d'événements pouvants activer une règle
	 */
	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		ArrayList<EventI> list_matched_event = new ArrayList<EventI>();
		EventI event;
		ArrayList<EventI> listHA1 = eb.getEvent("RequestPriority");
		ArrayList<EventI> listHA2 = eb.getEvent("RequestPriority");
		
		if (listHA1 == null || listHA2 == null)
			return null;
		
		while (list_iterator.get(0) < listHA1.size()) {
			event = listHA1.get(list_iterator.get(0));
			if (event.getPropertyValue("priority") == TypeOfTrafficLightPriority.EMERGENCY) {
				list_matched_event.add(event);
				
				while (list_iterator.get(1) < listHA2.size()) {
					event = listHA2.get(list_iterator.get(1));
					list_iterator.set(1, list_iterator.get(1) + 1);
					if (event.getPropertyValue("priority") != TypeOfTrafficLightPriority.EMERGENCY) {
						list_matched_event.add(event);
						return list_matched_event;
					}
				}
				list_iterator.set(1, 0);
			}
			list_matched_event.clear();
			list_iterator.set(0, list_iterator.get(0) + 1);
		}
		return null;
	}

	/**
	 * correlate est une méthode permettant de vérifier des conditions entre les
	 * événements appariés telle que le fait qu’ils se sont produits à la même
	 * position
	 * 
	 * @param matchedEvents de type ArrayList<EventI> et c'est la liste des
	 *                      événements pouvants activer une règle
	 * @return boolean
	 */

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {

		if (!matchedEvents.get(0).getPropertyValue("vehiculeId")
				.equals(matchedEvents.get(1).getPropertyValue("vehiculeId"))) {

			return true;
		}
		return false;
	}

	/**
	 * filter est une méthode de filtrage qui vérifie des conditions ne faisant pas
	 * partie des événements mais plutôt du contexte ou de l’environnement
	 * 
	 * @param matchedEvents de type ArrayList<EventI> et c'est la liste des
	 *                      événements pouvants activer une règle
	 * @param c             de type CorrelatorStateI
	 * @return boolean
	 */

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		return true;
	}

	/**
	 * act est une méthode qui permet d’exécuter des actions sur le système comme
	 * déclencher des alarmes ou de propager des événements vers d’autres
	 * corrélateurs
	 * 
	 * @param matchedEvents de type ArrayList<EventI> et c'est la liste des
	 *                      événements pouvants activer une règle
	 * @param c             de type CorrelatorStateI
	 * @return void
	 */

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		if (Data.DEBUG_RULE)
			System.out.println("Application RuleC4");
		TrafficLightCorrelatorState tc = (TrafficLightCorrelatorState) c;
		tc.setIntersection(TypeOfTrafficLightPriority.EMERGENCY);
	}

	@Override
	public void update(ArrayList<EventI> matchedEvents, EventBaseI eb) {
		WaitingVehiculePass wvp = new WaitingVehiculePass(matchedEvents.get(0).getTimeStamp(), matchedEvents);
		eb.removeEvent(matchedEvents.get(0));
		list_iterator.set(0, 0);
		eb.addEvent(wvp);
	}

}
