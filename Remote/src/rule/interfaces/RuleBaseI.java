package rule.interfaces;

import event.interfaces.EventBaseI;

public interface RuleBaseI {
	
	public void addRule(RuleI rule);

	public boolean fireFirstOn(EventBaseI eb, CorrelatorStateI c);

	public boolean fireAllOn(EventBaseI eb, CorrelatorStateI c);

}
