package rule.interfaces;

import components.Correlator;

public interface CorrelatorStateI {
    public void setCorrelator(Correlator correlator);
}
