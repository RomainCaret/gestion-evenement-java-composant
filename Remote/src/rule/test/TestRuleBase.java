package rule.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import correlator.classe.HealthCorrelatorState;
import event.classes.EventBase;
import event.classes.samu.ConsciousFall;
import event.classes.samu.HealthAlarm;
import event.classes.samu.SignalManual;
import event.interfaces.AtomicEventI;
import event.interfaces.ComplexEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import rule.classes.RuleBase;
import rule.classes.samu.RuleHealthAlarmS1;
import rule.classes.samu.RuleHealthAlarmS2;
import rule.classes.samu.RuleHealthAlarmS3;
import rule.classes.samu.RuleHealthAlarmS4;
import rule.classes.samu.RuleHealthAlarmS5;
import rule.classes.samu.RuleHealthAlarmS6;
import rule.classes.samu.RuleHealthAlarmS7;

class TestRuleBase {
	AtomicEventI ha1 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha2 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha3 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha4 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.MEDICAL, new AbsolutePosition(4, 4));
	AtomicEventI ha5 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.MEDICAL, new AbsolutePosition(4, 4));
	AtomicEventI ha6 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.TRACKING, new AbsolutePosition(4, 4));
	AtomicEventI ha7 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.TRACKING, new AbsolutePosition(4, 4));
	AtomicEventI ha8 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.TRACKING, new AbsolutePosition(4, 4));
	AtomicEventI ha9 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.TRACKING, new AbsolutePosition(4, 4));
	AtomicEventI sm1 = new SignalManual(LocalTime.MIDNIGHT, "Romain");
	AtomicEventI sm2 = new SignalManual(LocalTime.MIDNIGHT, "Miassa");
	AtomicEventI sm3 = new SignalManual(LocalTime.now(), "Manon");

	ArrayList<EventI> list = new ArrayList<EventI>();
	ComplexEventI cs = new ConsciousFall(LocalTime.MIDNIGHT, list);
	EventBaseI eb = new EventBase();
	RuleBase rb = new RuleBase();
	ArrayList<EventI> temp;

	RuleHealthAlarmS1 rha1 = new RuleHealthAlarmS1();
	RuleHealthAlarmS2 rha2 = new RuleHealthAlarmS2();
	RuleHealthAlarmS3 rha3 = new RuleHealthAlarmS3();
	RuleHealthAlarmS4 rha4 = new RuleHealthAlarmS4();
	RuleHealthAlarmS5 rha5 = new RuleHealthAlarmS5();
	RuleHealthAlarmS6 rha6 = new RuleHealthAlarmS6();
	RuleHealthAlarmS7 rha7 = new RuleHealthAlarmS7();

	HealthCorrelatorState h1 = new HealthCorrelatorState(new AbsolutePosition(4, 4), "samuDispo");
	HealthCorrelatorState h2 = new HealthCorrelatorState(new AbsolutePosition(4, 4), "samuNonDispo");

	@Test
	void testRuleBasefireFirstOn() {
		rb.addRule(rha7);
		rb.addRule(rha1);
		rb.addRule(rha2);
		rb.addRule(rha3);
		rb.addRule(rha4);
		rb.addRule(rha5);
		rb.addRule(rha6);

		assert eb.numberOfEvent() == 0 : "Erreur numberOfEvent";
		assert rb.fireFirstOn(eb, h1) == false : "Erreur fireFirstOn";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(cs);

		h1.setSamu_available(true);
		h1.setMedecin_available(true);

		assert (eb.appearsIn(ha1) && eb.appearsIn(ha2) && eb.appearsIn(ha3)) == true : "Erreur appearsIn";
		assert eb.numberOfEvent() == 8 : "Erreur numberOfEvent";
		assert rb.fireFirstOn(eb, h1) == true : "Erreur fireFirstOn";
		assert eb.numberOfEvent() == 7 : "Erreur fireFirstOn";
		assert (eb.appearsIn(ha1) && eb.appearsIn(ha2) && eb.appearsIn(ha3)) == false : "Erreur fireFirstOn";

		assert eb.numberOfEvent() == 7 : "Erreur numberOfEvent";
		assert rb.fireFirstOn(eb, h2) == true : "Erreur fireFirstOn";
		assert eb.numberOfEvent() == 6 : "Erreur fireFirstOn";

	}

	@Test
	void testRuleBasefireAllOn() {
		rb.addRule(rha7);
		rb.addRule(rha5);
		rb.addRule(rha1);
		rb.addRule(rha3);

		assert eb.numberOfEvent() == 0 : "Erreur numberOfEvent";
		assert rb.fireAllOn(eb, h1) == false : "Erreur fireAllOn";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(cs);
		eb.addEvent(sm2);
		eb.addEvent(sm3);

		h1.setSamu_available(true);
		h1.setMedecin_available(true);

		assert eb.numberOfEvent() == 10 : "Erreur numberOfEvent";
		assert rb.fireAllOn(eb, h1) == true : " Erreur fireAllOn";
		assert eb.appearsIn(ha1) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha2) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha3) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha4) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha5) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha6) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha7) == false : "Erreur fireAllOn";
		assert eb.appearsIn(cs) == true : "Erreur fireAllOn";
		assert eb.numberOfEvent() == 3 : "Erreur numberOfEvent";
		assert eb.appearsIn(sm2) == true : "Erreur fireAllOn";
		assert eb.appearsIn(sm3) == true : "Erreur fireAllOn";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(ha8);
		eb.addEvent(ha9);
		eb.addEvent(sm1);

		assert eb.numberOfEvent() == 13 : "Erreur numberOfEvent";
		assert rb.fireAllOn(eb, h2) == false : " Erreur fireAllOn";
		assert eb.numberOfEvent() == 13 : "Erreur numberOfEvent";

		rb.addRule(rha2);
		rb.addRule(rha4);
		rb.addRule(rha6);
		assert rb.fireAllOn(eb, h1) == true : " Erreur fireAllOn";
		assert eb.appearsIn(ha1) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha2) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha3) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha4) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha5) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha6) == false : "Erreur fireAllOn";
		assert eb.appearsIn(ha7) == false : "Erreur fireAllOn";
		assert eb.appearsIn(cs) == true : "Erreur fireAllOn";
		assert eb.numberOfEvent() == 3 : "Erreur numberOfEvent";

		assert eb.appearsIn(sm2) == true : "Erreur fireAllOn";
		assert eb.appearsIn(sm3) == true : "Erreur fireAllOn";
		assert eb.appearsIn(sm1) == false : "Erreur fireAllOn";
		System.out.println("TestRuleBase Success");
	}
}
