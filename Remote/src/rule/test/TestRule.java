package rule.test;


import java.time.LocalTime;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import correlator.classe.HealthCorrelatorState;
import event.classes.EventBase;
import event.classes.samu.ConsciousFall;
import event.classes.samu.HealthAlarm;
import event.classes.samu.SignalManual;
import event.interfaces.AtomicEventI;
import event.interfaces.ComplexEventI;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import rule.classes.RuleBase;
import rule.classes.samu.RuleHealthAlarmS1;
import rule.classes.samu.RuleHealthAlarmS2;
import rule.classes.samu.RuleHealthAlarmS3;
import rule.classes.samu.RuleHealthAlarmS4;
import rule.classes.samu.RuleHealthAlarmS5;
import rule.classes.samu.RuleHealthAlarmS6;
import rule.classes.samu.RuleHealthAlarmS7;

class TestRule {
	AtomicEventI ha1 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha2 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha3 = new HealthAlarm(LocalTime.MIDNIGHT, TypeOfHealthAlarm.EMERGENCY, new AbsolutePosition(4, 4));
	AtomicEventI ha4 = new HealthAlarm(LocalTime.now(), TypeOfHealthAlarm.MEDICAL, new AbsolutePosition(4, 4));
	AtomicEventI ha5 = new HealthAlarm(LocalTime.now(), TypeOfHealthAlarm.MEDICAL, new AbsolutePosition(4, 4));
	AtomicEventI ha6 = new HealthAlarm(LocalTime.now(), TypeOfHealthAlarm.TRACKING, new AbsolutePosition(4, 4));
	AtomicEventI ha7 = new HealthAlarm(LocalTime.now(), TypeOfHealthAlarm.TRACKING, new AbsolutePosition(4, 4));
	AtomicEventI sm1 = new SignalManual(LocalTime.MIDNIGHT, "JeanLouis");

	ArrayList<EventI> list = new ArrayList<EventI>();
	ComplexEventI cs = new ConsciousFall(LocalTime.MIDNIGHT, list);
	EventBaseI eb = new EventBase();
	RuleBase rb = new RuleBase();
	ArrayList<EventI> temp;

	RuleHealthAlarmS1 rha1 = new RuleHealthAlarmS1();
	RuleHealthAlarmS2 rha2 = new RuleHealthAlarmS2();
	RuleHealthAlarmS3 rha3 = new RuleHealthAlarmS3();
	RuleHealthAlarmS4 rha4 = new RuleHealthAlarmS4();
	RuleHealthAlarmS5 rha5 = new RuleHealthAlarmS5();
	RuleHealthAlarmS6 rha6 = new RuleHealthAlarmS6();
	RuleHealthAlarmS7 rha7 = new RuleHealthAlarmS7();

	HealthCorrelatorState h1 = new HealthCorrelatorState(new AbsolutePosition(4, 4), "samuDispo");
	HealthCorrelatorState h2 = new HealthCorrelatorState(new AbsolutePosition(4, 4), "samuNonDispo");

	@Test
	void testMatchRule1() {
		temp = rha1.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(cs);

		temp = rha1.match(eb);
		assert temp.get(0) == ha1 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha1.match(eb);
		assert temp.get(0) == ha2 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha1.match(eb);
		assert temp.get(0) == ha3 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha1.match(eb);
		assert temp == null : "Erreur match";

		rha1.set_start();
		temp = rha1.match(eb);
		assert temp.get(0) == ha1 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

	}

	@Test
	void testMatchRule2() {
		temp = rha2.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(cs);

		temp = rha2.match(eb);
		assert temp.get(0) == ha1 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha2.match(eb);
		assert temp.get(0) == ha2 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha2.match(eb);
		assert temp.get(0) == ha3 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha2.match(eb);
		assert temp == null : "Erreur match";

		rha2.set_start();
		temp = rha2.match(eb);
		assert temp.get(0) == ha1 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

	}

	@Test
	void testMatchRule3() {
		temp = rha3.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(cs);

		temp = rha3.match(eb);
		assert temp.get(0) == ha4 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha3.match(eb);
		assert temp.get(0) == ha5 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha3.match(eb);
		assert temp == null : "Erreur match";

		rha3.set_start();
		temp = rha3.match(eb);
		assert temp.get(0) == ha4 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

	}

	@Test
	void testMatchRule4() {
		temp = rha4.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(cs);

		temp = rha4.match(eb);
		assert temp.get(0) == ha4 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha4.match(eb);
		assert temp.get(0) == ha5 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha4.match(eb);
		assert temp == null : "Erreur match";

		rha4.set_start();
		temp = rha4.match(eb);
		assert temp.get(0) == ha4 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

	}

	@Test
	void testMatchRule5() {
		temp = rha5.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(cs);

		temp = rha5.match(eb);
		assert temp.get(0) == ha6 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha5.match(eb);
		assert temp.get(0) == ha7 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha5.match(eb);
		assert temp == null : "Erreur match";

		rha5.set_start();
		temp = rha5.match(eb);
		assert temp.get(0) == ha6 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

	}

	@Test
	void testMatchRule6() {
		temp = rha6.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(cs);

		temp = rha6.match(eb);
		assert temp.get(0) == ha6 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha6.match(eb);
		assert temp.get(0) == ha7 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

		temp = rha6.match(eb);
		assert temp == null : "Erreur match";

		rha6.set_start();
		temp = rha6.match(eb);
		assert temp.get(0) == ha6 : "Erreur match";
		assert temp.size() == 1 : "Erreur match";

	}

	@Test
	void testMatchRule7() {
		temp = rha7.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(cs);
		eb.addEvent(sm1);

		temp = rha7.match(eb);
		assert temp.get(0) == ha6 : "Erreur match";
		assert temp.get(1) == sm1 : "Erreur match";
		assert temp.size() == 2 : "Erreur match";

		temp = rha7.match(eb);
		assert temp == null : "Erreur match";

		rha7.set_start();
		temp = rha7.match(eb);
		assert temp.get(0) == ha6 : "Erreur match";
		assert temp.get(1) == sm1 : "Erreur match";
		assert temp.size() == 2 : "Erreur match";

	}

	@Test
	void testCorrelateRule1_6() {
		temp = rha1.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(cs);

		temp = rha1.match(eb);
		assert rha1.correlate(temp) == true : "Erreur correlate";
		assert rha2.correlate(temp) == true : "Erreur correlate";
		assert rha3.correlate(temp) == true : "Erreur correlate";
		assert rha4.correlate(temp) == true : "Erreur correlate";
		assert rha5.correlate(temp) == true : "Erreur correlate";
		assert rha6.correlate(temp) == true : "Erreur correlate";
	}

	@Test
	void testFilterRule1() {
		temp = rha1.match(eb);
		assert temp == null : "Erreur match";

		eb.addEvent(ha1);
		eb.addEvent(ha2);
		eb.addEvent(ha3);
		eb.addEvent(ha4);
		eb.addEvent(ha5);
		eb.addEvent(ha6);
		eb.addEvent(ha7);
		eb.addEvent(cs);

		temp = rha1.match(eb);
		assert rha1.filter(temp, h1) == false : "Erreur filter";
		h1.setSamu_available(true);
		assert rha1.filter(temp, h1) == true : "Erreur filter";
		System.out.println("TestRule Success");
	}
}
