package rule.abstracts;

import java.util.ArrayList;

import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import rule.interfaces.CorrelatorStateI;
import rule.interfaces.RuleI;

public class RuleAbstract implements RuleI {

	protected ArrayList<Integer> list_iterator = new ArrayList<Integer>();

	@Override
	public ArrayList<EventI> match(EventBaseI eb) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean correlate(ArrayList<EventI> matchedEvents) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean filter(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void act(ArrayList<EventI> matchedEvents, CorrelatorStateI c) {
		// TODO Auto-generated method stub

	}

	/**
	 * update est une méthode de mise à jour permettant de modifier la base
	 * d’événements du corrélateur en retirant des événements corrélés ou en
	 * ajoutant des événements complexes, par exemple
	 * 
	 * @param matchedEvents de type ArrayList<EventI> et c'est la liste des
	 *                      événements pouvants activer une règle
	 * @param eb            de type EventBaseI et c'est la base d'événements
	 * @return void
	 */
	@Override
	public void update(ArrayList<EventI> matchedEvents, EventBaseI eb) {
		int i = 0;

		if (matchedEvents.size()> 2) {
			System.out.println(matchedEvents.get(0)+"bizzare");
			System.out.println(matchedEvents.get(1)+"bizzare");
			System.out.println(matchedEvents.get(2)+"bizzare");
			}
		for (EventI iterator : matchedEvents) {
			eb.removeEvent(iterator);
			list_iterator.set(i, Math.max(0,list_iterator.get(i) - 1));
			i++;
			
		}
	}

	/**
	 * set_start est une méthode permettant de remettre l'itératteur à zéro pour
	 * pouvoir effectuer (match, correlate, filter, act et update) autant de fois
	 * que l'on souhaite plusieurs fois sur la même règle
	 */

	@Override
	public void set_start() {
		for (int i = 0; i < list_iterator.size(); i++) {
			list_iterator.set(i, 0);
		}
	}

}
