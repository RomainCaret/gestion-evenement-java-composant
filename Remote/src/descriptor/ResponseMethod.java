package descriptor;


public enum ResponseMethod {
		/** standard fire truck wi no or just a small ladder.					*/
		RegisterCorrelator,
		/** larger fire truck with a high ladder.								*/
		GetExecutor, RegisterEmitter
}
