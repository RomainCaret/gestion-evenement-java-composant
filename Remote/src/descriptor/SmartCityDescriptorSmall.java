package descriptor;


// Copyright Jacques Malenfant, Sorbonne Universite.
// Jacques.Malenfant@lip6.fr
//
// This software is a computer program whose purpose is to provide a
// basic component programming model to program with components
// distributed applications in the Java programming language.
//
// This software is governed by the CeCILL-C license under French law and
// abiding by the rules of distribution of free software.  You can use,
// modify and/ or redistribute the software under the terms of the
// CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
// URL "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL-C license and that you accept its terms.

import fr.sorbonne_u.components.AbstractPort;
import fr.sorbonne_u.cps.smartcity.descriptions.AbstractSmartCityDescriptor;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import java.util.HashMap;

// -----------------------------------------------------------------------------
/**
 * The class <code>SmartCityDescriptor</code> defines the information
 * about the smart city assets in the small smart city simulator.
 *
 * <p><strong>Description</strong></p>
 * 
 * <p><strong>Invariant</strong></p>
 * 
 * <pre>
 * invariant	true
 * </pre>
 * 
 * <p>Created on : 2022-02-10</p>
 * 
 * @author	<a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
public abstract class	SmartCityDescriptorSmall
extends		AbstractSmartCityDescriptor
{
	/** the identifier of the zone covering the entire city.				*/
	public static final String	UNIQUE_ZONE_ID = "zone0";

	/**
	 * initialise the smart city descriptor.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	true		// no precondition.
	 * post	true		// no postcondition.
	 * </pre>
	 *
	 */
	public static void	initialise()
	{
		numberOfStreets = 3;
		numberOfAvenues = 3;

		AbstractSmartCityDescriptor.initialise();

		southeastCorner = new AbsolutePosition(numberOfStreets + 0.5,
											   numberOfAvenues + 0.5);
		cityZone = new Zone(northwestCorner, southeastCorner);

		fireStations = new HashMap<>();
		fireStations.put(
				"FireStation-1",
				new FireStation(
						"FireStation-1",
						new AbsolutePosition(0.5, 1.0),
						new Zone(northwestCorner,
								 new AbsolutePosition(numberOfStreets + 0.5,
										 			  1.99)),
						AbstractPort.generatePortURI()));
		fireStations.put(
				"FireStation-2",
				new FireStation(
						"FireStation-2",
						new AbsolutePosition(2.0, 3.5),
						new Zone(new AbsolutePosition(0.0, 2.0),
								 southeastCorner),
						AbstractPort.generatePortURI()));

		samuStations = new HashMap<>();
		samuStations.put(
				"SAMU-1",
				new SAMUStation(
						"SAMU-1",
						new AbsolutePosition(3.0, 3.5),
						new Zone(new AbsolutePosition(2.0, 0.5),
								 southeastCorner),
						AbstractPort.generatePortURI()));
		samuStations.put(
				"SAMU-2",
				new SAMUStation(
						"SAMU-2",
						new AbsolutePosition(1.0, 1.5),
						new Zone(northwestCorner,
								 new AbsolutePosition(1.99,
										 			  numberOfAvenues + 0.5)),
						AbstractPort.generatePortURI()));

		trafficLights = new HashMap<>();
		for (int i = 1 ; i <= numberOfStreets ; i++) {
			for (int j = 1 ; j <= numberOfAvenues ; j++) {
				IntersectionPosition p = new IntersectionPosition(i, j);
				trafficLights.put(
						p,
						new TrafficLight(p.toString(), p,
										 AbstractPort.generatePortURI()));
			}
		}
	}
}
// -----------------------------------------------------------------------------