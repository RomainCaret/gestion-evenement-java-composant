package descriptor;

import java.time.LocalTime;

import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.utils.TimeManager;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;

public class Data {
    public static final String uri_cepbus1 = "uri_cepbus1";
    public static final String uri_cepbus2 = "uri_cepbus2";
    public static final String uri_cepbus3 = "uri_cepbus3";
    public static final String uri_correlator = "uri_correlator";
    public static final String uri_emitter_exec1 = "uri_emitter_exec1";
    public static final String uri_emitter_exec2 = "uri_emitter_exec2";
    public static final String uri_emitter_exec3 = "uri_emitter_exec3";
    
    public static final String uri_cepbus_mip = "cbmip_uri";
    public static final String uri_cepbus_erip = "erib_uri";
    public static final String uri_cepbus_eeip = "eeib_uri";
    //Debug temporaire
    public static final boolean DEBUG_TMP = false;
    
    //Debug simple affiche sendEvent et receiveEnvent + les notifications recus
    public static final boolean DEBUG = false;
    
    //Permet de voir l'application des rules
    public static final boolean DEBUG_RULE = true;
    

    
    //public static LocalTime TIME_NOW = LocalTime.now();
	public static int numeroCVM = -1;
	public static int nb_bus = 3;
	public static boolean test = true;

    public static final String				SAMU_1_ID = "SAMU Station 0";
	/** position of the corresponding samu station.							*/
	public static final fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition	POSITION_SAMU_1 =
												new fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition(1.5, 2.0);
	
    public static final String				SAMU_2_ID = "SAMU Station 1";
	/** position of the corresponding samu station.							*/
	public static final AbsolutePosition	POSITION_SAMU_2 =
												new fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition(0.5, 2.0);

	/** identifier of the corresponding fire station.						*/
	public static final String				FIRESTATION_1_ID = "Fire Station 0";
	/** position of the corresponding fire station.							*/
	public static final AbsolutePosition	POSITION_FIRESTATION_1 =
												new AbsolutePosition(0.5, 1.0);
	/** identifier of the corresponding fire station.						*/
	public static final String				FIRESTATION_2_ID = "Fire Station 0";
	/** position of the corresponding fire station.							*/
	public static final AbsolutePosition	POSITION_FIRESTATION_2 =
												new AbsolutePosition(1.0, 1.0);
    public static final IntersectionPosition POSITION_TRAFFIC_1 = new IntersectionPosition(1.0, 1.0);
	
    public static final IntersectionPosition POSITION_TRAFFIC_2 = new IntersectionPosition(1.0, 2.0);

    public static final IntersectionPosition POSITION_TRAFFIC_3 = new IntersectionPosition(2.0, 1.0);
    
    public static final IntersectionPosition POSITION_TRAFFIC_4 = new IntersectionPosition(2.0, 2.0);
    
    public static LocalTime getCurrentTime() {
    	return TimeManager.get().getCurrentLocalTime();
    }
        
}
