package descriptor;

import rule.classes.*;
import rule.classes.fireStation.RuleAllLadderInInterventionF15;
import rule.classes.fireStation.RuleAllTruckInInterventionF16;
import rule.classes.fireStation.RuleFireAlarmF1;
import rule.classes.fireStation.RuleFireAlarmF2;
import rule.classes.fireStation.RuleFireAlarmF3;
import rule.classes.fireStation.RuleFireAlarmF4;
import rule.classes.fireStation.RuleFireFirstAlarmF11;
import rule.classes.fireStation.RuleFireFirstAlarmF12;
import rule.classes.fireStation.RuleFireFirstAlarmF19;
import rule.classes.fireStation.RuleFireFirstAlarmF9;
import rule.classes.fireStation.RuleFireGeneralAlarmF10;
import rule.classes.fireStation.RuleFireGeneralAlarmF21;
import rule.classes.fireStation.RuleFireSecondAlarmF13;
import rule.classes.fireStation.RuleFireSecondAlarmF14;
import rule.classes.fireStation.RuleFireSecondAlarmF14bis;
import rule.classes.fireStation.RuleFireSecondAlarmF20;
import rule.classes.fireStation.RuleLadderAvailableF17;
import rule.classes.fireStation.RuleRequestInterventionBuildingF5;
import rule.classes.fireStation.RuleRequestInterventionBuildingF6;
import rule.classes.fireStation.RuleRequestInterventionBuildingF6bis;
import rule.classes.fireStation.RuleRequestInterventionHouseF7;
import rule.classes.fireStation.RuleRequestInterventionHouseF8;
import rule.classes.fireStation.RuleRequestInterventionHouseF8bis;
import rule.classes.fireStation.RuleTruckAvailableF18;
import rule.classes.samu.RuleAllMedecinInterventionS17;
import rule.classes.samu.RuleAllSamuInInterventionS16;
import rule.classes.samu.RuleConsciousFallS13;
import rule.classes.samu.RuleConsciousFallS14;
import rule.classes.samu.RuleConsciousFallS15;
import rule.classes.samu.RuleHealthAlarmS1;
import rule.classes.samu.RuleHealthAlarmS2;
import rule.classes.samu.RuleHealthAlarmS3;
import rule.classes.samu.RuleHealthAlarmS4;
import rule.classes.samu.RuleHealthAlarmS5;
import rule.classes.samu.RuleHealthAlarmS6;
import rule.classes.samu.RuleHealthAlarmS7;
import rule.classes.samu.RuleHealthAlarmS8;
import rule.classes.samu.RuleMedecinAvailableS19;
import rule.classes.samu.RuleRequestInterventionS10;
import rule.classes.samu.RuleRequestInterventionS10bis;
import rule.classes.samu.RuleRequestInterventionS11;
import rule.classes.samu.RuleRequestInterventionS12;
import rule.classes.samu.RuleRequestInterventionS12bis;
import rule.classes.samu.RuleRequestInterventionS9;
import rule.classes.samu.RuleSamuAvailableS18;
import rule.classes.trafficLight.RuleTrafficLightC1;
import rule.classes.trafficLight.RuleTrafficLightC2;
import rule.classes.trafficLight.RuleTrafficLightC3;
import rule.classes.trafficLight.RuleTrafficLightC4;
import rule.classes.trafficLight.RuleTrafficLightC5;

public class CorrelatorSetRule {
    
    public static RuleBase setRuleSamu(RuleBase rb){
        
    	rb.addRule(new RuleHealthAlarmS5());
        rb.addRule(new RuleHealthAlarmS6());
        rb.addRule(new RuleHealthAlarmS7());
        rb.addRule(new RuleHealthAlarmS8());
    	
        rb.addRule(new RuleHealthAlarmS1());
        rb.addRule(new RuleHealthAlarmS2());
        rb.addRule(new RuleHealthAlarmS3());
        rb.addRule(new RuleHealthAlarmS4());
        
        
        rb.addRule(new RuleRequestInterventionS9());
        rb.addRule(new RuleRequestInterventionS10());
        rb.addRule(new RuleRequestInterventionS11());
        rb.addRule(new RuleRequestInterventionS12());
        
        rb.addRule(new RuleConsciousFallS13());
        rb.addRule(new RuleConsciousFallS14());
        rb.addRule(new RuleConsciousFallS15());

        rb.addRule(new RuleAllSamuInInterventionS16());
        rb.addRule(new RuleAllMedecinInterventionS17());

        rb.addRule(new RuleSamuAvailableS18());
        rb.addRule(new RuleMedecinAvailableS19());
        
        rb.addRule(new RuleRequestInterventionS10bis());       
        rb.addRule(new RuleRequestInterventionS12bis());

        
        return rb;
    }

    public static RuleBase setRuleFireStation(RuleBase rb){
    	rb.addRule(new RuleFireAlarmF1());
    	rb.addRule(new RuleFireAlarmF2());
    	rb.addRule(new RuleFireAlarmF3());
    	rb.addRule(new RuleFireAlarmF4());
    	
    	rb.addRule(new RuleRequestInterventionBuildingF5());
    	rb.addRule(new RuleRequestInterventionBuildingF6());
    	
    	rb.addRule(new RuleRequestInterventionHouseF7());
    	rb.addRule(new RuleRequestInterventionHouseF8());
    	
    	rb.addRule(new RuleFireFirstAlarmF9());
    	
    	rb.addRule(new RuleFireGeneralAlarmF10());
    	
    	rb.addRule(new RuleFireFirstAlarmF11());
    	rb.addRule(new RuleFireFirstAlarmF12());
    	
    	rb.addRule(new RuleFireSecondAlarmF13());
    	rb.addRule(new RuleFireSecondAlarmF14());
    	
    	rb.addRule(new RuleAllLadderInInterventionF15());
    	
    	rb.addRule(new RuleAllTruckInInterventionF16());
    	
    	rb.addRule(new RuleLadderAvailableF17());
    	
    	rb.addRule(new RuleTruckAvailableF18());
    	
    	rb.addRule(new RuleFireFirstAlarmF19());
    
    	rb.addRule(new RuleFireSecondAlarmF20());
    	
    	rb.addRule(new RuleFireGeneralAlarmF21());
    	
    	rb.addRule(new RuleFireSecondAlarmF14bis());
    	rb.addRule(new RuleRequestInterventionBuildingF6bis());
    	rb.addRule(new RuleRequestInterventionHouseF8bis());




    	return rb;
    }

    public static RuleBase setRuleTrafficLight(RuleBase rb){
    	
    	rb.addRule(new RuleTrafficLightC1());
    	rb.addRule(new RuleTrafficLightC2());
    	rb.addRule(new RuleTrafficLightC3());
    	rb.addRule(new RuleTrafficLightC4());
    	rb.addRule(new RuleTrafficLightC5());


        return rb;
    }
}
