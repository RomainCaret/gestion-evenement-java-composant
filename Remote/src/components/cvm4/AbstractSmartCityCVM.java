package components.cvm4;


//Copyright Jacques Malenfant, Sorbonne Universite.
//Jacques.Malenfant@lip6.fr
//
//This software is a computer program whose purpose is to provide a
//basic component programming model to program with components
//distributed applications in the Java programming language.
//
//This software is governed by the CeCILL-C license under French law and
//abiding by the rules of distribution of free software.  You can use,
//modify and/ or redistribute the software under the terms of the
//CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
//URL "http://www.cecill.info".
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability. 
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or 
//data to be ensured and,  more generally, to use and operate it in the 
//same conditions as regards security. 
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL-C license and that you accept its terms.

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.AbstractPort;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.traffic.components.TrafficLightsSimulator;
import fr.sorbonne_u.cps.smartcity.utils.TimeManager;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import components.proxy3.FireStationProxy;
import components.proxy3.SAMUStationProxy;
import components.proxy3.TrafficLightProxy;

import descriptor.SmartCityDescriptorSmall;

//-----------------------------------------------------------------------------
/**
* The class <code>AbstractSmartCityCVM</code>
*
* <p><strong>Description</strong></p>
* 
* <p><strong>Invariant</strong></p>
* 
* <pre>
* invariant		true
* </pre>
* 
* <p>Created on : 2022-03-02</p>
* 
* @author	<a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
*/
public abstract class	AbstractSmartCityCVM
extends		AbstractCVM
{
	// -------------------------------------------------------------------------
	// Constants and variables
	// -------------------------------------------------------------------------

	public static final boolean	DEBUG = false;
	/** delay before the beginning of the smart city simulation after launching
	 *  the program.														*/
	protected static final long	START_DELAY = 6000L;
	/** the start time of the simulation as a Java {@code LocalTime}.		*/
	protected static LocalTime simulatedStartTime;
	/** the end time of the simulation as a Java {@code LocalTime}.			*/
	protected static LocalTime simulatedEndTime;

	/** map that will contain the URI of the action inbound ports used
	 *  in proxy components to offer their services in the smart city
	 *  and the URI of notification inbound ports used by events emitter
	 *  components to receive the notifications from the smart city.	*/
	private	Map<String,String>				facadeNotificationInboundPortsURI;
	/** URI of the fire stations and SAMU centers inbound port used by the
	 *  traffic lights simulator to notify them of events concerning them.	*/
	protected final Map<String,String>					stationsEventIBPURI;
	/** URI of the traffic lights simulator inbound port used by the fire
	 *  stations and SAMU centers to execute the actions concerning them.	*/
	protected final Map<IntersectionPosition,String>	trafficLightsIBPURI;

	public				AbstractSmartCityCVM() throws Exception
	{
		// initialise the basic simulator smart city descriptor.
		SmartCityDescriptorSmall.initialise();
		assert	simulatedStartTime != null && simulatedEndTime != null
				&& simulatedEndTime.isAfter(simulatedStartTime);
		long realTimeOfStart = System.currentTimeMillis() + START_DELAY;
		new TimeManager(realTimeOfStart, simulatedStartTime, simulatedEndTime);
		// create a map that will contain the URI of the notification inbound
		// ports used in event emitter components to receive the notifications
		// from the smart city.
		this.facadeNotificationInboundPortsURI = new HashMap<>();

		AbstractCVM.getThisJVMURI();

		this.stationsEventIBPURI = new HashMap<>();
		Iterator<String> iterStation =
							SmartCityDescriptorSmall.createFireStationIdIterator();
		while (iterStation.hasNext()) {
			String id = iterStation.next();
			this.stationsEventIBPURI.put(id, AbstractPort.generatePortURI());
		}
		iterStation = SmartCityDescriptorSmall.createSAMUStationIdIterator();
		while (iterStation.hasNext()) {
			stationsEventIBPURI.put(iterStation.next(),
									AbstractPort.generatePortURI());
		}

		this.trafficLightsIBPURI = new HashMap<>();
		Iterator<IntersectionPosition> iterTL =
				SmartCityDescriptorSmall.createTrafficLightPositionIterator();
		while (iterTL.hasNext()) {
			this.trafficLightsIBPURI.put(iterTL.next(),
										 AbstractPort.generatePortURI());
		}
	}

	// -------------------------------------------------------------------------
	// Methods
	// -------------------------------------------------------------------------

	/**
	 * return true if the asset has already a URI registered, false otherwise.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code assetId != null && !assetId.isEmpty()}
	 * post	true		// no postcondition.
	 * </pre>
	 *
	 * @param assetId	asset identifier as define the the smart city descriptor.
	 * @return			true if the asset has already a URI registered, false otherwise.
	 */
	protected boolean	registered(String assetId)
	{
		assert	assetId != null && !assetId.isEmpty();
		return this.facadeNotificationInboundPortsURI.containsKey(assetId);
	}

	/**
	 * register the URI if the notification inbound port used in the events
	 * emitter component associated with the asset identifier {@code assetId}.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code assetId != null && !assetId.isEmpty()}
	 * pre	{@code !registered(assetId)}
	 * pre	{@code uri != null && !uri.isEmpty()}
	 * post	{@code registered(assetId)}
	 * </pre>
	 *
	 * @param assetId	asset identifier as define the the smart city descriptor.
	 * @param uri		URI of the notification inbound port of the corresponding events emitter component.
	 */
	protected void		register(String assetId, String uri)
	{
		assert	assetId != null && !assetId.isEmpty();
		assert	!this.registered(assetId);
		assert	uri != null && !uri.isEmpty();
		this.facadeNotificationInboundPortsURI.put(assetId, uri);
	}

	/**
	 * @see fr.sorbonne_u.components.cvm.AbstractCVM#deploy()
	 */
	@Override
	public void			deploy() throws Exception
	{
		AbstractComponent.createComponent(
				TrafficLightsSimulator.class.getCanonicalName(),
				new Object[]{this.stationsEventIBPURI,
							 this.trafficLightsIBPURI});

		Iterator<String> iterStation =
						SmartCityDescriptorSmall.createFireStationIdIterator();
		while (iterStation.hasNext()) {
			String id = iterStation.next();
			AbstractComponent.createComponent(
					FireStationProxy.class.getCanonicalName(),
					new Object[]{
							SmartCityDescriptorSmall.getActionInboundPortURI(id),
							this.facadeNotificationInboundPortsURI.get(id),
							id,
							SmartCityDescriptorSmall.getPosition(id),
							this.stationsEventIBPURI.get(id),
							2,
							2
							});
		}

		iterStation = SmartCityDescriptorSmall.createSAMUStationIdIterator();
		while (iterStation.hasNext()) {
			String id = iterStation.next();
			AbstractComponent.createComponent(
					SAMUStationProxy.class.getCanonicalName(),
					new Object[]{
							SmartCityDescriptorSmall.getActionInboundPortURI(id),
							this.facadeNotificationInboundPortsURI.get(id),
							id,
							SmartCityDescriptorSmall.getPosition(id),
							this.stationsEventIBPURI.get(id),
							2,
							2
							});
		}

		Iterator<IntersectionPosition> trafficLightsIterator =
					SmartCityDescriptorSmall.createTrafficLightPositionIterator();
		while (trafficLightsIterator.hasNext()) {
			IntersectionPosition p = trafficLightsIterator.next();
			AbstractComponent.createComponent(
					TrafficLightProxy.class.getCanonicalName(),
					new Object[]{
							p,
							SmartCityDescriptorSmall.getActionInboundPortURI(p),
							this.facadeNotificationInboundPortsURI.
								 							get(p.toString()),
							this.trafficLightsIBPURI.get(p)
							});
		}

		super.deploy();
	}
}
//-----------------------------------------------------------------------------