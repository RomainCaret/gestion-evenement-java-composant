package components.interfaces;

public enum FireStationActions implements ActionI {
    SendFirstAlarm, SendSecondAlarm, SendGeneralAlarm
}
