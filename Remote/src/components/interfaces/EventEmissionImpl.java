package components.interfaces;

import event.interfaces.EventI;

public interface EventEmissionImpl {
	public void sendEvent(String emitterURI, EventI event) throws Exception;

	public void sendEvents(String emitterURI, EventI[] events) throws Exception;

}
