package components.interfaces;

import java.rmi.Remote;

import event.interfaces.EventI;

public interface EventReceptionImpl extends Remote{

	public void receiveEvent(String emitterURI, EventI e) throws Exception;

	public void receiveEvents(String emitterURI, EventI[] e) throws Exception;
}
