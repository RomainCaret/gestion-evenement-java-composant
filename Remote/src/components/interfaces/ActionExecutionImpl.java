package components.interfaces;

import java.io.Serializable;

public interface ActionExecutionImpl {
	public  void execute(ActionI a , Serializable[] params) throws Exception;

}

