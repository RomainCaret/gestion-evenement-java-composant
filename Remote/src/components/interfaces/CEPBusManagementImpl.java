package components.interfaces;

public interface CEPBusManagementImpl {
	public void registerEmitter(String uri, String inboundPortResponseURI) throws Exception;

	public void unregisterEmitter(String uri) throws Exception;

	public void registerCorrelator(String uri, String inboundPortURI,String inboundPortResponseURI) throws Exception;

	public void unregisterCorrelator(String uri) throws Exception;

	public void registerExecutor(String uri, String inboundPortURI ) throws Exception;

	public void getExecutorInboundPortURI(String uri, String inboundPortResponseURI) throws Exception;

	public void unregisterExecutor(String uri) throws Exception;

	public void subscribe(String subscriberURI, String emitterURI) throws Exception;

	public void unsubscribe(String subscriberURI, String emitterURI) throws Exception;
}
