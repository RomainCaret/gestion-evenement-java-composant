package components.interfaces;

import java.io.Serializable;

import fr.sorbonne_u.components.interfaces.OfferedCI;
import fr.sorbonne_u.components.interfaces.RequiredCI;

public interface ActionExecutionCI extends OfferedCI, RequiredCI {
	
	public  void execute(ActionI a , Serializable[] params) throws Exception;

}
