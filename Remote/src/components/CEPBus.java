package components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import components.connector.ConnectorEventReception;
import components.connector.ResultReceptionConnector;
import components.interfaces.CEPBusManagementCI;
import components.interfaces.CEPBusManagementImpl;
import components.interfaces.EventEmissionCI;
import components.interfaces.EventEmissionImpl;
import components.interfaces.EventReceptionCI;
import components.interfaces.EventReceptionImpl;
import components.interfaces.ResultReceptionCI;
import components.ports.CEPBusManagementInboundPort;
import components.ports.EventEmissionInboundPort;
import components.ports.EventReceptionInboundPort;
import components.ports.EventReceptionOutboundPort;
import components.ports.ResultReceptionOutboundPort;
import descriptor.Data;
import descriptor.ResponseMethod;
import event.classes.fireStation.RequestInterventionBuilding;
import event.interfaces.AtomicEventI;
import event.interfaces.ComplexEventI;
import event.interfaces.EventI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;

@OfferedInterfaces(offered = { CEPBusManagementCI.class, EventEmissionCI.class, EventReceptionCI.class })
@RequiredInterfaces(required = { EventReceptionCI.class, ResultReceptionCI.class })

public class CEPBus extends AbstractComponent implements CEPBusManagementImpl, EventEmissionImpl, EventReceptionImpl {
	protected String cepbusID;
	protected ConcurrentHashMap<String, EventReceptionOutboundPort> correlator_map = new ConcurrentHashMap<String, EventReceptionOutboundPort>();
	protected ConcurrentHashMap<String, String> executor_map = new ConcurrentHashMap<String, String>();
	protected ConcurrentHashMap<String, List<String>> subscrib_map = new ConcurrentHashMap<String, List<String>>();

	protected CEPBusManagementInboundPort cbmip;
	public final String CBMIP_URI;

	protected EventEmissionInboundPort eeip;
	public final String EEIP_URI;

	protected EventReceptionInboundPort erip;
	public final String ERIP_URI;

	protected EventReceptionOutboundPort erop;

	protected static final String POOL_URI = "cepbus pool";
	protected static final int NTHREADS = 5;
	private int num_bus;

	/**
	 * CEPBus est le constructeur de la classe CEPBus
	 * 
	 * @param id      de type String
	 * @param num_bus de type int
	 * @throws Exception
	 */
	protected CEPBus(String id, int num_bus) throws Exception {
		// super(nbThreads, nbSchedulableThreads);
		super(10, 0);
		this.initialise();

		this.CBMIP_URI = Data.uri_cepbus_mip + num_bus;
		this.ERIP_URI = Data.uri_cepbus_erip + num_bus;
		this.EEIP_URI = Data.uri_cepbus_eeip + num_bus;

		this.cepbusID = id;
		this.cbmip = new CEPBusManagementInboundPort(CBMIP_URI, this);
		this.cbmip.publishPort();

		this.eeip = new EventEmissionInboundPort(EEIP_URI, this);
		this.eeip.publishPort();

		this.erip = new EventReceptionInboundPort(ERIP_URI, this);
		this.erip.publishPort();

		this.erop = new EventReceptionOutboundPort(this);
		this.erop.publishPort();
		this.num_bus = num_bus;

	}

	
	protected void initialise() throws Exception {
		this.createNewExecutorService(POOL_URI, NTHREADS, false);
	}

	@Override
	public synchronized void start() throws ComponentStartException {
		try {
			this.doPortConnection(this.erop.getPortURI(), Data.uri_cepbus_erip + ((num_bus + 1) % Data.nb_bus),
					ConnectorEventReception.class.getCanonicalName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.start();
	}

	
	@Override
	public synchronized void finalise() throws Exception {
		Iterator<String> it = correlator_map.keySet().iterator();
		String sub;
		this.doPortDisconnection(this.erop.getPortURI());
		while (it.hasNext()) {
			sub = it.next();
			this.doPortDisconnection(correlator_map.get(sub).getPortURI());
		}
		super.finalise();
	}


	@Override
	public synchronized void shutdown() throws ComponentShutdownException {
		try {
			this.cbmip.unpublishPort();
			this.eeip.unpublishPort();
			this.erip.unpublishPort();
			this.erop.unpublishPort();
			Iterator<String> it = correlator_map.keySet().iterator();
			String sub;
			while (it.hasNext()) {
				sub = it.next();
				this.correlator_map.get(sub).unpublishPort();
				;
			}

		} catch (Exception e) {
			throw new ComponentShutdownException(e);
		}
		super.shutdown();

	}

	/**
	 * registerEmitter est une méthode qui enregistre l’émetteur d’événements dont
	 * l’URI est donnée par uri et retourne l’URI de son port entrant offrant
	 * l’interface EventEmissionCI pour permettre à l’émetteur de se connecter au
	 * bus.
	 * 
	 * @param uri                    de type String
	 * @param inboundPortResponseURI de type String
	 */
	@Override
	public void registerEmitter(String uri, String inboundPortResponseURI) {
		CEPBus cp = this;
		this.baselineHandleRequest(POOL_URI, new AbstractComponent.AbstractService<Double>() {

			@Override
			public Double call() throws Exception {
				// to force a more lengthy operation
				subscrib_map.put(uri, Collections.synchronizedList(new ArrayList<String>()));

				ResultReceptionOutboundPort rrop = new ResultReceptionOutboundPort(cp);
				rrop.publishPort();
				cp.doPortConnection(rrop.getPortURI(), inboundPortResponseURI,
						ResultReceptionConnector.class.getCanonicalName());
				// computing and sending the result
				rrop.acceptResult(ResponseMethod.RegisterEmitter, EEIP_URI);
				cp.doPortDisconnection(rrop.getPortURI());
				rrop.unpublishPort();
				rrop.destroyPort();
				// connexion avec port pour emettre vers correlator et Correlator va
				// se connecter avec CEPbus
				return null;
			}
		});
	}

	/**
	 * unregisterEmitter est une méthode qui désenregistre l’émetteur d’événements
	 * dont l’URI est donnée par uri.
	 * 
	 * @param uri de type String
	 */
	@Override
	public void unregisterEmitter(String uri) {
		subscrib_map.remove(uri);
	}

	/**
	 * registerCorrelator est une méthode qui enregistre le corrélateur dont l’URI
	 * est donnée par uri avec l’URI de son port entrant offrant l’interface
	 * EventReceptionCI (ce qui permet au bus de s’y connecter) puis retourne l’URI
	 * de son port entrant offrant l’interface EventEmissionCI pour permettre au
	 * corrélateur de se connecter à lui pour émettre des événements le cas échéant.
	 * 
	 * @param uri                    de type String
	 * @param inboundPortURI         de type String
	 * @param inboundPortResponseURI de type String
	 */
	@Override
	public void registerCorrelator(String uri, String inboundPortURI, String inboundPortResponseURI) throws Exception {
		CEPBus cp = this;
		this.baselineHandleRequest(POOL_URI, new AbstractComponent.AbstractService<Double>() {
			@Override
			public Double call() throws Exception {
				// to force a more lengthy operation
				EventReceptionOutboundPort eeop = new EventReceptionOutboundPort(cp);
				eeop.publishPort();
				cp.doPortConnection(eeop.getPortURI(), inboundPortURI,
						ConnectorEventReception.class.getCanonicalName());
				correlator_map.put(uri, eeop);

				subscrib_map.put(uri, new ArrayList<String>());

				ResultReceptionOutboundPort rrop = new ResultReceptionOutboundPort(cp);
				rrop.publishPort();
				cp.doPortConnection(rrop.getPortURI(), inboundPortResponseURI,
						ResultReceptionConnector.class.getCanonicalName());
				// computing and sending the result
				rrop.acceptResult(ResponseMethod.RegisterCorrelator, EEIP_URI);
				cp.doPortDisconnection(rrop.getPortURI());
				rrop.unpublishPort();
				rrop.destroyPort();
				// connexion avec port pour emettre vers correlator et Correlator va
				// se connecter avec CEPbus
				return null;
			}
		});

	}

	/**
	 * unregisterCorrelator est une méthode qui désenregistre le corrélateur dont
	 * l’URI est donnée par uri.
	 * 
	 * @param uri de type String
	 */
	@Override
	public void unregisterCorrelator(String uri) throws Exception {
		EventReceptionOutboundPort eeop = correlator_map.get(uri);
		this.doPortDisconnection(eeop.getPortURI());
		eeop.unpublishPort();
		correlator_map.remove(uri);

	}

	/**
	 * registerExecutor est une méthode qui enregistre l’exécuteur d’actions dont
	 * l’URI est donnée par uri avec l’URI de son port entrant offrant l’interface
	 * ActionExecutionCI.
	 * 
	 * @param uri            de type String
	 * @param inboundPortURI de type String
	 */
	@Override
	public void registerExecutor(String uri, String inboundPortURI) {
		// connexion avec port pour emettre vers Executor
		executor_map.put(uri, inboundPortURI);
	}

	/**
	 * getExecutorInboundPortURI esu une méthode qui retourne l’URI du port entrant
	 * de l’exécuteur d’actions dont l’URI est donnée par uri ; cette opération
	 * permet aux corrélateurs de récupérer l’URI des exécuteurs d’actions auxquels
	 * ils souhaitent se connecter.
	 * 
	 * @param uri                    de type String
	 * @param inboundPortResponseURI de type String
	 */
	@Override
	public void getExecutorInboundPortURI(String uri, String inboundPortResponseURI) {
		CEPBus cp = this;

		this.baselineHandleRequest(POOL_URI, new AbstractComponent.AbstractService<Double>() {
			@Override
			public Double call() throws Exception {
				// to force a more lengthy operation
				if (Data.DEBUG)
					System.out.println(
							"CEPBusTrace : (getExecutorInboundPortURI) : " + uri + "exec : " + executor_map.get(uri));

				ResultReceptionOutboundPort rrop = new ResultReceptionOutboundPort(cp);
				rrop.publishPort();
				cp.doPortConnection(rrop.getPortURI(), inboundPortResponseURI,
						ResultReceptionConnector.class.getCanonicalName());
				// computing and sending the result
				String[] tab = new String[2];
				tab[0] = uri;
				tab[1] = executor_map.get(uri);
				rrop.acceptResult(ResponseMethod.GetExecutor, tab);
				cp.doPortDisconnection(rrop.getPortURI());
				rrop.unpublishPort();
				rrop.destroyPort();
				return null;
			}
		});

	}

	/**
	 * unregisterExecutor est une méthode qui désenregistre l’exécuteur d’actions
	 * dont l’URI est donnée par uri.
	 * 
	 * @paramuri de type String
	 */
	@Override
	public void unregisterExecutor(String uri) {
		executor_map.remove(uri);
	}

	/**
	 * subscribe est une méthode qui abonne le corrélateur dont l’URI est donnée par
	 * subscriberURI aux événements émis par l’émetteur d’événements ou le
	 * corrélateur dont l’URI est donnée par emitterURI.
	 * 
	 * @param subscriberURI de type String
	 * @param emitterURI    de type String
	 */

	@Override
	public void subscribe(String subscriberURI, String emitterURI) {
		if (subscrib_map.containsKey(emitterURI)) {
			subscrib_map.get(emitterURI).add(subscriberURI);
		} else {
			// ArrayList<String> list = new ArrayList<String>();
			// list.add(subscriberURI);
			System.out.println("CEPBusTrace (" + this.num_bus + ") : Erreur inscription emetteur : Emitter : "
					+ emitterURI + ", Subricriber : " + subscriberURI);
			// subscrib_map.put(emitterURI, list);
		} // c'est que les corrélateurs qui subscribe
	}

	/**
	 * unsubscribe est une méthode qui désabonne le corrélateur dont l’URI est
	 * donnée par subscriberURI aux événements émis par l’émetteur d’événements ou
	 * le corrélateur dont l’URI est donnée par emitterURI.
	 * 
	 * @param subscriberURI de type String
	 * @param emitterURI    de type String
	 */

	@Override
	public void unsubscribe(String subscriberURI, String emitterURI) {
		if (subscrib_map.containsKey(emitterURI)) {
			subscrib_map.get(emitterURI).remove(subscriberURI);
			if (subscrib_map.get(emitterURI).size() == 0)
				subscrib_map.remove(emitterURI);
		}
	}

	/**
	 * sendEvent est une méthode qui émet un événement event à propager vers les
	 * destinataires abonnés aux événements en provenance de l’émetteur identifié
	 * par emitterURI.
	 * 
	 * @param emitterURI de type String
	 * @param event      de type EventI
	 */
	@Override
	public void sendEvent(String emitterURI, EventI event) throws Exception {
		EventI event_tmp = event;
		if (Data.DEBUG)
			System.out.println("CEPBusTrace : Entre dans Send Event");

		if (event.hasProperty("bus_init")) {

			if ((int) event.getPropertyValue("bus_init") != this.num_bus) {
				// System.out.println("J'envoie un event ("+event+") dans le bus "
				// +((this.num_bus+1)%Data.nb_bus));
				this.erop.receiveEvent(emitterURI, event);
			} else {
				while (event_tmp instanceof ComplexEventI) {
					event_tmp = ((ComplexEventI) event_tmp).getCorrelatedEvents().get(0);
				}
				((AtomicEventI) event_tmp).removeProperty("bus_init");
				return;
			}
		} else {
			while (event_tmp instanceof ComplexEventI) {
				event_tmp = ((ComplexEventI) event_tmp).getCorrelatedEvents().get(0);
			}
			((AtomicEventI) event_tmp).putProperty("bus_init", this.num_bus);

			this.erop.receiveEvent(emitterURI, event);
		}

		if (emitterURI.contains(Data.uri_correlator)) { // Si le correlateur est émetter, alors envoie à tous les
														// corrélateurs
			Iterator<String> it = correlator_map.keySet().iterator();
			String corr;
			while (it.hasNext()) {
				corr = it.next();
				if (corr.equals(emitterURI))
					continue;
				final EventReceptionOutboundPort erop = correlator_map.get(corr);
				if (Data.DEBUG)
					System.out.println("EROP :" + erop);
				this.baselineHandleRequest(POOL_URI, new AbstractComponent.AbstractService<Double>() {
					@Override
					public Double call() throws Exception {
						erop.receiveEvent(emitterURI, event);
						return null;
					}
				});

			}
			return;
		}
		List<String> list_subscriber = subscrib_map.get(emitterURI); // send à tous ces URI abonnées

		if (list_subscriber == null) {
			// System.out.println("CEPBusTrace : Erreur Inscription");
			return;
		}
		if (Data.DEBUG)
			System.out.println("CEPBusTrace : (In SendEvent) LIST SUSCRIBER : " + list_subscriber + event);

		for (String iter : list_subscriber) {
			final EventReceptionOutboundPort erop = correlator_map.get(iter);
			if (Data.DEBUG)
				System.out.println("EROP :" + erop);
			this.baselineHandleRequest(POOL_URI, new AbstractComponent.AbstractService<Double>() {
				@Override
				public Double call() throws Exception {
					erop.receiveEvent(emitterURI, event);
					return null;
				}
			});
		}
		if (Data.DEBUG)
			System.out.println("CEPBusTrace : Sors de Send Event");
	}

	/**
	 * sendEvents est une méthode qui envoie un tableau d’événements events à
	 * propager vers les destinataires abonnés aux événements en provenance de
	 * l’émetteur identifié par emitterURI.
	 * 
	 * @param emitterURI de type String
	 * @param events     de type tableau d'EventI[]
	 */
	@Override
	public void sendEvents(String emitterURI, EventI[] events) throws Exception {
		for (EventI eventI : events) {
			sendEvent(emitterURI, eventI);
		}

		if (correlator_map.containsKey(emitterURI)) { // Si le correlateur est émetter, alors envoie à tous les
														// corrélateurs
			Iterator<String> it = correlator_map.keySet().iterator();
			String corr;
			while (it.hasNext()) {
				corr = it.next();
				if (corr.equals(emitterURI))
					continue;
				final EventReceptionOutboundPort erop = correlator_map.get(corr);
				if (Data.DEBUG)
					System.out.println("EROP :" + erop);
				this.baselineHandleRequest(POOL_URI, new AbstractComponent.AbstractService<Double>() {
					@Override
					public Double call() throws Exception {
						// to force a more lengthy operation
						erop.receiveEvents(emitterURI, events);
						return null;
					}
				});
			}
			return;
		}

		List<String> list_subscriber = subscrib_map.get(emitterURI); // Send à tous ces URI
		if (list_subscriber == null) {
			System.out.println("CEPBusTrace : Erreur Inscription");
			return;
		}
		for (String iter : list_subscriber) {
			final EventReceptionOutboundPort erop = correlator_map.get(iter);
			if (Data.DEBUG)
				System.out.println("EROP :" + erop);
			this.baselineHandleRequest(POOL_URI, new AbstractComponent.AbstractService<Double>() {
				@Override
				public Double call() throws Exception {
					// to force a more lengthy operation
					erop.receiveEvents(emitterURI, events);
					return null;
				}
			});
		}
	}


	@Override
	public void receiveEvent(String emitterURI, EventI e) throws Exception {
		if (Data.test == false)
			System.out.println("Je recois un event du bus " + ((this.num_bus - 1 + Data.nb_bus) % Data.nb_bus));
		this.sendEvent(emitterURI, e);
	}

	@Override
	public void receiveEvents(String emitterURI, EventI[] e) throws Exception {
		this.sendEvents(emitterURI, e);
	}

}
