package components.proxy3;



// Copyright Jacques Malenfant, Sorbonne Universite.
// Jacques.Malenfant@lip6.fr
//
// This software is a computer program whose purpose is to provide a
// basic component programming model to program with components
// distributed applications in the Java programming language.
//
// This software is governed by the CeCILL-C license under French law and
// abiding by the rules of distribution of free software.  You can use,
// modify and/ or redistribute the software under the terms of the
// CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
// URL "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL-C license and that you accept its terms.

import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.cps.smartcity.connections.FireStationNotificationConnector;
import fr.sorbonne_u.cps.smartcity.connections.FireStationNotificationOutboundPort;
import fr.sorbonne_u.cps.smartcity.components.AbstractStationProxy;
import fr.sorbonne_u.cps.smartcity.connections.FireStationActionInboundPort;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.grid.Direction;
import fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationCI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFire;
import fr.sorbonne_u.cps.smartcity.interfaces.FireStationActionCI;
import fr.sorbonne_u.cps.smartcity.interfaces.FireStationActionImplI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFirefightingResource;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfResourceI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;
import fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle;
import fr.sorbonne_u.cps.smartcity.utils.TimeManager;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import components.cvm4.AbstractSmartCityCVM;

// -----------------------------------------------------------------------------
/**
 * The class <code>FireStationProxy</code> implements a fire station for the
 * small smart city simulator.
 *
 * <p><strong>Description</strong></p>
 * 
 * <p>
 * This proxy executes a simulation scenario with a unique event notifications
 * program that is followed deterministically.
 * </p>
 * 
 * <p><strong>Invariant</strong></p>
 * 
 * <pre>
 * invariant	true
 * </pre>
 * 
 * <p>Created on : 2022-02-03</p>
 * 
 * @author	<a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
public class			FireStationProxy
extends		AbstractStationProxy
implements	FireStationActionImplI			
{
	// -------------------------------------------------------------------------
	// Constants and variables
	// -------------------------------------------------------------------------

	/** duration of a fire in a building.									*/
	protected static final int			BUILDING_FIRE_DURATION = 10;
	/** duration of a fire in a house.										*/
	protected static final int			HOUSE_FIRE_DURATION = 5;

	/** URI of the notification inbound port.								*/
	protected String								notificationInboundPortURI;
	/** notification outbound port.											*/
	protected FireStationNotificationOutboundPort	notificationOBP;
	/** actions inbound port.												*/
	protected FireStationActionInboundPort			actionIBP;

	protected final int					totalNumberOfStandardTrucks;
	protected final int					totalNumberOfHighLadders;
	protected int						currentNumberOfStandardTrucks;
	protected int						currentNumberOfHighLadders;
	/** id of the standard trucks in this fire station.						*/
	protected final Set<String>			standardTrucksId;
	/** id of the high ladders in this fire station.						*/
	protected final Set<String>			highLaddersId;
	/** id of resources currently on intervention.							*/
	protected final Set<String>			currentlyOperating;

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * create the fire station proxy component.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code actionInboundPortURI != null && !actionInboundPortURI.isEmpty()}
	 * pre	{@code notificationInboundPortURI != null && !notificationInboundPortURI.isEmpty()}
	 * pre	{@code stationId != null && !stationId.isEmpty()}
	 * pre	{@code position != null}
	 * pre	{@code totalNumberOfStandardTrucks > 0}
	 * pre	{@code totalNumberOfHighLadders > 0}
	 * post	true			// no postcondition.
	 * </pre>
	 *
	 * @param actionInboundPortURI			URI of the action inbound port.
	 * @param notificationInboundPortURI	URI of the notification inbound port of the component in charge of receiving the notifications from this proxy.
	 * @param stationId						fire station identifier.
	 * @param position						position of the fire station.
	 * @param stationEventIBPURI			URI of the inbound port to receive event notifications from the simulator.
	 * @param totalNumberOfStandardTrucks	total number of standard trucks in the fire station.
	 * @param totalNumberOfHighLadders		total number of high ladders in the fire station.
	 * @throws Exception 					<i>to do</i>.
	 */
	protected			FireStationProxy(
		String actionInboundPortURI,
		String notificationInboundPortURI,
		String stationId,
		AbsolutePosition position,
		String stationEventIBPURI,
		int totalNumberOfStandardTrucks,
		int totalNumberOfHighLadders
		) throws Exception
	{
		super(2, 1, stationId, position, stationEventIBPURI);

		assert	actionInboundPortURI != null && !actionInboundPortURI.isEmpty();
		assert	notificationInboundPortURI != null &&
										!notificationInboundPortURI.isEmpty();
		assert	totalNumberOfStandardTrucks > 0;
		assert	totalNumberOfHighLadders > 0;

		this.totalNumberOfStandardTrucks = totalNumberOfStandardTrucks;
		this.totalNumberOfHighLadders = totalNumberOfHighLadders;

		this.currentNumberOfStandardTrucks = this.totalNumberOfStandardTrucks;
		this.currentNumberOfHighLadders = this.totalNumberOfHighLadders;
		this.standardTrucksId = new HashSet<String>();
		for (int i = 0 ; i < this.totalNumberOfStandardTrucks ; i++) {
			this.standardTrucksId.add("fire-truck-" + stationId + "-" + i);
		}
		this.highLaddersId = new HashSet<String>();
		for (int i = 0 ; i < this.totalNumberOfHighLadders ; i++) {
			this.highLaddersId.add("high-ladder-" + stationId + "-" + i);
		}
		this.currentlyOperating = new HashSet<String>();

		this.addRequiredInterface(FireStationNotificationCI.class);
		this.addOfferedInterface(FireStationActionCI.class);

		this.notificationInboundPortURI = notificationInboundPortURI;
		this.notificationOBP = new FireStationNotificationOutboundPort(this);
		this.notificationOBP.publishPort();
		this.actionIBP =
			new FireStationActionInboundPort(actionInboundPortURI, this);
		this.actionIBP.publishPort();

		this.getTracer().setTitle("FireStationProxy " + this.stationId);
		this.getTracer().setRelativePosition(2, 1);
		this.toggleTracing();
	}

	// -------------------------------------------------------------------------
	// Component life-cycle
	// -------------------------------------------------------------------------

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#start()
	 */
	@Override
	public synchronized void	start() throws ComponentStartException
	{
		super.start();

		try {
			this.doPortConnection(
					this.notificationOBP.getPortURI(),
					this.notificationInboundPortURI,
					FireStationNotificationConnector.class.getCanonicalName());
		} catch (Exception e) {
			throw new ComponentStartException(e);
		}
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#execute()
	 */
	@Override
	public synchronized void	execute() throws Exception
	{
		LocalTime startTime	=
				TimeManager.get().getSimulatedStartTime().plusSeconds(2);
		long startTimeDelay =
				TimeManager.get().localTime2nanoDelay(startTime);
		this.scheduleTask(
				o -> {
					try {
						System.out.println("(Expected RuleF17)");
						System.out.println("(Expected RuleF18)");
						this.traceMessage("High ladder trucks available at " +
										  startTime + ".\n");
						notificationOBP.
								notifyHighLadderTrucksAvailable(startTime);
						this.traceMessage("Standard trucks available at " + 
										  startTime + ".\n");
						notificationOBP.
								notifyStandardTrucksAvailable(startTime);
					} catch (Throwable e) {
						e.printStackTrace();
					}
				},
				startTimeDelay, TimeUnit.NANOSECONDS);

		if (this.stationId.equals("FireStation-1")) {
			LocalTime fireAlarm1 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(10);
			long fireAlarm1NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm1);
			AbsolutePosition p1 = new AbsolutePosition(3.0, 0.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF1)");

							this.traceMessage(
									"Trigger alarm in building at " +
									fireAlarm1 + ".\n");
							notificationOBP.
								fireAlarm(p1, fireAlarm1, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm1NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm2 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(70);
			long fireAlarm2NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm2);
			AbsolutePosition p2 = new AbsolutePosition(2.0, 1.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF1)");
							System.out.println("(Expected RuleF15)");

							this.traceMessage(
									"Trigger alarm in building at " +
									fireAlarm2 + ".\n");
							notificationOBP.
								fireAlarm(p2, fireAlarm2, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm2NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm3 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(80);
			long fireAlarm3NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm3);
			AbsolutePosition p3 = new AbsolutePosition(1.0, 1.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF3)");
							System.out.println("(Expected RuleF5)");

							this.traceMessage(
										"Trigger alarm in building at " +
										fireAlarm3 + ".\n");
							notificationOBP.
								fireAlarm(p3, fireAlarm3, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm3NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm4 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(140);
			long fireAlarm4NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm4);
			AbsolutePosition p4 = new AbsolutePosition(1.5, 1.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF3)");
							System.out.println("(Expected RuleF5)");
							System.out.println("(Expected RuleF15)");

							this.traceMessage(
										"Trigger alarm in building at " +
										fireAlarm4 + ".\n");
							notificationOBP.
								fireAlarm(p4, fireAlarm4, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm4NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm5 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(165);
			long fireAlarm5NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm5);
			AbsolutePosition p5 = new AbsolutePosition(0.5, 1.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF3)");
							System.out.println("(Expected RuleF6bis)");

							this.traceMessage(
										"Trigger alarm in building at " +
										fireAlarm5 + ".\n");
							notificationOBP.
								fireAlarm(p5, fireAlarm5, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm5NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm6 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(640);
			long fireAlarm6NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm6);
			AbsolutePosition p6 = new AbsolutePosition(3.0, 0.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF2)");
							this.traceMessage(
										"Trigger alarm in house at " +
										fireAlarm6 + ".\n");
							notificationOBP.
								fireAlarm(p6, fireAlarm6, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm6NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm7 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(670);
			long fireAlarm7NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm7);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF12)");
							System.out.println("(Expected RuleF13)");

							this.traceMessage(
										"Trigger alarm in house at " +
										fireAlarm7 + ".\n");
							notificationOBP.
								fireAlarm(p6, fireAlarm7, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm7NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm8 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(805);
			long fireAlarm8NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm8);
			AbsolutePosition p8 = new AbsolutePosition(2.0, 0.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF1)");

							this.traceMessage(
										"Trigger alarm in building at " +
										fireAlarm8 + ".\n");
							notificationOBP.
								fireAlarm(p8, fireAlarm8, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm8NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm9 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(820);
			long fireAlarm9NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm9);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF9)");
							System.out.println("(Expected RuleF10bis)");

							this.traceMessage(
										"Trigger alarm in building at " +
										fireAlarm9 + ".\n");
							notificationOBP.
								fireAlarm(p8, fireAlarm9, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm9NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm10 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(880);
			long fireAlarm10NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm10);
			AbsolutePosition p10 = new AbsolutePosition(3.0, 1.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF1)");

							this.traceMessage(
										"Trigger alarm in building at " +
										fireAlarm8 + ".\n");
							notificationOBP.
								fireAlarm(p10, fireAlarm10, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm10NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm11 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(890);
			long fireAlarm11NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm11);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF9)");
							System.out.println("(Expected RuleF10)");

							this.traceMessage(
										"Trigger alarm in building at " +
										fireAlarm11 + ".\n");
							notificationOBP.
								fireAlarm(p10, fireAlarm11, TypeOfFire.Building);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm11NanoDelay, TimeUnit.NANOSECONDS);
	
			
			
			

			LocalTime fireAlarm12 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(1210);
			long fireAlarm12NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm12);
			AbsolutePosition p12 = new AbsolutePosition(1.0, 0.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF2)");

							this.traceMessage(
										"Trigger alarm in house at " +
										fireAlarm8 + ".\n");
							notificationOBP.
								fireAlarm(p12, fireAlarm12, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm12NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm13 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(1240);
			long fireAlarm13NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm13);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF11)");

							this.traceMessage(
										"Trigger alarm in house at " +
										fireAlarm13 + ".\n");
							notificationOBP.
								fireAlarm(p12, fireAlarm13, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm13NanoDelay, TimeUnit.NANOSECONDS);

		} else {

			assert	this.stationId.equals("FireStation-2");
			LocalTime fireAlarm1 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(25);
			long fireAlarm1NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm1);
			AbsolutePosition p1 = new AbsolutePosition(3.0, 2.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF2)");

							this.traceMessage(
									"Trigger alarm in house at " +
									fireAlarm1 + ".\n");
							notificationOBP.
								fireAlarm(p1, fireAlarm1, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm1NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm2 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(100);
			long fireAlarm2NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm2);
			AbsolutePosition p2 = new AbsolutePosition(1.0, 2.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF2)");
							System.out.println("(Expected RuleF16)");

							this.traceMessage(
									"Trigger alarm in house at " +
									fireAlarm2 + ".\n");
							notificationOBP.
								fireAlarm(p2, fireAlarm2, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm2NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm3 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(130);
			long fireAlarm3NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm3);
			AbsolutePosition p3 = new AbsolutePosition(2.0, 2.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF4)");
							System.out.println("(Expected RuleF7)");

							this.traceMessage(
									"Trigger alarm in house at " +
									fireAlarm3 + ".\n");
							notificationOBP.
								fireAlarm(p3, fireAlarm3, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm3NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm4 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(200);
			long fireAlarm4NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm4);
			AbsolutePosition p4 = new AbsolutePosition(1.0, 3.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF4)");
							System.out.println("(Expected RuleF7)");

							this.traceMessage(
									"Trigger alarm in house at " +
									fireAlarm4 + ".\n");
							notificationOBP.
								fireAlarm(p4, fireAlarm4, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm4NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime fireAlarm5 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(220);
			long fireAlarm5NanoDelay =
					TimeManager.get().localTime2nanoDelay(fireAlarm5);
			AbsolutePosition p5 = new AbsolutePosition(3.0, 3.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleF4)");
							System.out.println("(Expected RuleF8bis)");

							this.traceMessage(
									"Trigger alarm in house at " +
									fireAlarm5 + ".\n");
							notificationOBP.
								fireAlarm(p5, fireAlarm5, TypeOfFire.House);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					}, fireAlarm5NanoDelay, TimeUnit.NANOSECONDS);

		}
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#finalise()
	 */
	@Override
	public synchronized void	finalise() throws Exception
	{
		this.doPortDisconnection(this.notificationOBP.getPortURI());
		super.finalise();
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#shutdown()
	 */
	@Override
	public synchronized void	shutdown() throws ComponentShutdownException
	{
		try {
			this.notificationOBP.unpublishPort();
			this.actionIBP.unpublishPort();
		} catch (Exception e) {
			throw new ComponentShutdownException(e);
		}
		super.shutdown();
	}

	// -------------------------------------------------------------------------
	// Component services implementation
	// -------------------------------------------------------------------------

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationActionImplI#triggerFirstAlarm(fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition, fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFirefightingResource)
	 */
	@Override
	public void			triggerFirstAlarm(
		AbsolutePosition p,
		TypeOfFirefightingResource r
		) throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("FireStationProxy#triggerFirstAlarm");
		}
		this.traceMessage("First alarm triggered at " + p +
											" using resource type " + r + "\n");

		assert	position != null;
		assert	r != TypeOfFirefightingResource.StandardTruck ||
										this.currentNumberOfStandardTrucks > 0;
		assert	r != TypeOfFirefightingResource.HighLadderTruck ||
										this.currentNumberOfHighLadders > 0;

		this.sendResource(r, p);
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationActionImplI#triggerSecondAlarm(fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition)
	 */
	@Override
	public void			triggerSecondAlarm(AbsolutePosition p) throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("FireStationProxy#triggerSecondAlarm");
		}
		this.traceMessage("Second alarm triggered at " + p + "\n");

		assert	this.currentNumberOfStandardTrucks > 0;

		this.sendResource(TypeOfFirefightingResource.StandardTruck, p);
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationActionImplI#triggerGeneralAlarm(fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition)
	 */
	@Override
	public void			triggerGeneralAlarm(AbsolutePosition p)
	throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("FireStationProxy#triggerGeneralAlarm");
		}
		this.traceMessage("General alarm triggered at " + p + "\n");

		assert	this.currentNumberOfHighLadders > 0;

		this.sendResource(TypeOfFirefightingResource.HighLadderTruck, p);
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.traffic.components.StationEventTransmissionImplI#signalAtBase(fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle, java.time.LocalTime)
	 */
	@Override
	public void		signalAtBase(Vehicle v, LocalTime t)
	throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("FireStationProxy#signalAtBase");
		}
		this.traceMessage("Vehicle " + v.getId() + " comes back at station at "
						  + t + ".\n");
		this.notificationOBP.atStation(v.getId(), t);
		assert	this.currentlyOperating.contains(v.getId());
		this.currentlyOperating.remove(v.getId());
		if (this.standardTrucksId.contains(v.getId())) {
			if (this.currentNumberOfStandardTrucks == 0) {
				this.notificationOBP.notifyStandardTrucksAvailable(t);
			}
			this.currentNumberOfStandardTrucks++;
		} else {
			assert	this.highLaddersId.contains(v.getId());
			if (this.currentNumberOfHighLadders == 0) {
				this.notificationOBP.notifyHighLadderTrucksAvailable(t);
			}
			this.currentNumberOfHighLadders++;
		}
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.traffic.components.StationEventTransmissionImplI#signalAtDestination(fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle, java.time.LocalTime)
	 */
	@Override
	public void		signalAtDestination(Vehicle v, LocalTime t)
	throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("FireStationProxy#signalAtDestination");
		}
		this.traceMessage("Vehicle " + v.getId() + " arrives at destination at "
						  + t + ".\n");
		this.notificationOBP.atDestination(v.getId(), t);

		LocalTime current = TimeManager.get().getCurrentLocalTime();
		int duration;
		if (this.highLaddersId.contains(v.getId())) {
			duration = BUILDING_FIRE_DURATION;
		} else {
			duration = HOUSE_FIRE_DURATION;
		}
		LocalTime end = current.plusMinutes(duration);
		long delay = TimeManager.get().localTime2nanoDelay(end);
		this.scheduleTask(
				o -> {
					try {
						v.returnBase();
						Direction d = v.firstDirection();
						this.notificationOBP.endOfFire(v.getDestination(), end);
						this.requestPriority(v, v.getBase());
						this.traceMessage("Inserting vehicle " + v.toString()
										  + "\n");
						this.stationActionOBP.insertVehicle(
												v.getCurrentPosition(), v, d);
					} catch (Throwable e) {
						e.printStackTrace();
					}
				},
				delay, TimeUnit.NANOSECONDS);
	}

	// -------------------------------------------------------------------------
	// Component internal methods
	// -------------------------------------------------------------------------

	/**
	 * return the id of a vehicle corresponding to {@code tr}.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code tr != null && tr instanceof TypeOfFirefightingResource}
	 * post	{@code ret != null}
	 * </pre>
	 *
	 * @param tr			type of required fire fighting resource.
	 * @return				the id of a vehicle corresponding to {@code r}.
	 * @throws Exception	<i>to do</i>.
	 * @see fr.sorbonne_u.cps.smartcity.components.AbstractStationProxy#selectResource(fr.sorbonne_u.cps.smartcity.interfaces.TypeOfResourceI)
	 */
	protected String	selectResource(TypeOfResourceI tr)
	throws Exception
	{
		assert	tr != null && tr instanceof TypeOfFirefightingResource;
		TypeOfFirefightingResource r = (TypeOfFirefightingResource) tr;

		String ret = null;
		if (r == TypeOfFirefightingResource.StandardTruck) {
			this.currentNumberOfStandardTrucks--;
			for (String id : this.standardTrucksId) {
				if (!this.currentlyOperating.contains(id)) {
					ret = id;
					this.currentlyOperating.add(id);
					break;
				}
			}
			if (this.currentNumberOfStandardTrucks == 0) {
				this.notificationOBP.notifyNoStandardTruckAvailable(
									TimeManager.get().getCurrentLocalTime());
			}
		} else {
			assert r == TypeOfFirefightingResource.HighLadderTruck;
			this.currentNumberOfHighLadders--;
			for (String id : this.highLaddersId) {
				if (!this.currentlyOperating.contains(id)) {
					ret = id;
					this.currentlyOperating.add(id);
					break;
				}
			}
			if (this.currentNumberOfHighLadders == 0) {
				this.notificationOBP.notifyNoHighLadderTruckAvailable(
									TimeManager.get().getCurrentLocalTime());
			}
		}
		return ret;
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.components.AbstractStationProxy#requestPriority(fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle, fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition)
	 */
	@Override
	protected void		requestPriority(Vehicle v, AbsolutePosition p)
	throws Exception
	{
//		System.out.println("******** requestPriority " + v + " " + p);
		assert	v != null && p != null;
		assert	v.getDestination().equals(p) || v.getBase().equals(p);

		LocalTime t = TimeManager.get().getCurrentLocalTime();
		this.notificationOBP.requestPriority(
				v.getCurrentPosition(),
				TypeOfTrafficLightPriority.EMERGENCY,
				v.getId(),
				p,
				t);
	}
}
// -----------------------------------------------------------------------------