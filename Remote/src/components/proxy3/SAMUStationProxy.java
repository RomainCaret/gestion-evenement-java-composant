package components.proxy3;

// Copyright Jacques Malenfant, Sorbonne Universite.
// Jacques.Malenfant@lip6.fr
//
// This software is a computer program whose purpose is to provide a
// basic component programming model to program with components
// distributed applications in the Java programming language.
//
// This software is governed by the CeCILL-C license under French law and
// abiding by the rules of distribution of free software.  You can use,
// modify and/ or redistribute the software under the terms of the
// CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
// URL "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL-C license and that you accept its terms.

import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.cps.smartcity.connections.SAMUNotificationConnector;
import fr.sorbonne_u.cps.smartcity.connections.SAMUNotificationOutboundPort;
import fr.sorbonne_u.cps.smartcity.components.AbstractStationProxy;
import fr.sorbonne_u.cps.smartcity.connections.SAMUActionInboundPort;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.grid.Direction;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.interfaces.SAMUNotificationCI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfHealthAlarm;
import fr.sorbonne_u.cps.smartcity.interfaces.SAMUActionCI;
import fr.sorbonne_u.cps.smartcity.interfaces.SAMUActionImplI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfResourceI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfSAMURessources;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;
import fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle;
import fr.sorbonne_u.cps.smartcity.utils.TimeManager;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import components.cvm4.AbstractSmartCityCVM;
import descriptor.SmartCityDescriptorSmall;

// -----------------------------------------------------------------------------
/**
 * The class <code>SAMUStationProxy</code> implements a SAMU station for the
 * small smart city simulator.
 *
 * <p><strong>Description</strong></p>
 * 
 * <p>
 * This proxy executes a simulation scenario with a unique event notifications
 * program that is followed deterministically.
 * </p>
 * 
 * <p><strong>Invariant</strong></p>
 * 
 * <pre>
 * invariant	true
 * </pre>
 * 
 * <p>Created on : 2022-02-04</p>
 * 
 * @author	<a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
public class			SAMUStationProxy
extends		AbstractStationProxy
implements	SAMUActionImplI
{
	// -------------------------------------------------------------------------
	// Constants and variables
	// -------------------------------------------------------------------------

	/** duration of an intervention requiring an ambulance.					*/
	protected static final int				EMERGENCY_INTERVENTION_DURATION = 2;
	/** duration of an intervention requiring a medic.						*/
	protected static final int				MEDICAL_INTERVENTION_DURATION = 5;

	/** URI of the notification inbound port.								*/
	protected String						notificationInboundPortURI;
	/** notification outbound port.										 	*/
	protected SAMUNotificationOutboundPort	notificationOBP;
	/** action inbound port.												*/
	protected SAMUActionInboundPort			actionIBP;

	protected int					totalNumberOfAmbulances;
	protected int					totalNumberOfMedics;
	protected int					currentNumberOfAmbulances;
	protected int					currentNumberOfMedics;
	/** id of the ambulances in this SAMU center.							*/
	protected final Set<String>		ambulancesId;
	/** id of the medics in this SAMU center.								*/
	protected final Set<String>		medicsId;
	/** id of resources currently on intervention.							*/
	protected final Set<String>		currentlyOperating;

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * create the SAMU station proxy component.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code actionInboundPortURI != null && !actionInboundPortURI.isEmpty()}
	 * pre	{@code notificationInboundPortURI != null && !notificationInboundPortURI.isEmpty()}
	 * post	true			// no postcondition.
	 * </pre>
	 *
	 * @param actionInboundPortURI			URI of the action inbound port.
	 * @param notificationInboundPortURI	URI of the notification inbound port of the component in charge of receiving the notifications from this proxy.
	 * @param stationId						fire station identifier.
	 * @param position						position of the fire station.
	 * @param stationEventIBPURI			URI of the inbound port to receive event notifications from the simulator.
	 * @param totalNumberOfAmbulances		number of ambulances in the center.
	 * @param totalNumberOfMedics			number of medics in the center.
	 * @throws Exception 					<i>to do</i>.
	 */
	protected			SAMUStationProxy(
		String actionInboundPortURI,
		String notificationInboundPortURI,
		String stationId,
		AbsolutePosition position,
		String stationEventIBPURI,
		int totalNumberOfAmbulances,
		int totalNumberOfMedics
		) throws Exception
	{
		super(2, 1, stationId, position, stationEventIBPURI);

		assert	actionInboundPortURI != null &&
										!actionInboundPortURI.isEmpty();
		assert	notificationInboundPortURI != null &&
										!notificationInboundPortURI.isEmpty();
		assert	totalNumberOfAmbulances > 0;
		assert	totalNumberOfMedics > 0;

		this.totalNumberOfAmbulances = totalNumberOfAmbulances;
		this.totalNumberOfMedics = totalNumberOfMedics;
		this.currentNumberOfAmbulances = this.totalNumberOfAmbulances;
		this.currentNumberOfMedics = this.totalNumberOfMedics;
		this.ambulancesId = new HashSet<String>();
		for (int i = 0 ; i < this.totalNumberOfAmbulances ; i++) {
			this.ambulancesId.add("ambulance-" + this.stationId + "-" + i);
		}
		this.medicsId = new HashSet<String>();
		for (int i = 0 ; i < this.totalNumberOfMedics ; i++) {
			this.medicsId.add("medic-" + this.stationId + "-" + i);
		}
		this.currentlyOperating = new HashSet<String>();

		this.addOfferedInterface(SAMUActionCI.class);
		this.addRequiredInterface(SAMUNotificationCI.class);

		this.notificationInboundPortURI = notificationInboundPortURI;
		this.notificationOBP = new SAMUNotificationOutboundPort(this);
		this.notificationOBP.publishPort();
		this.actionIBP = new SAMUActionInboundPort(actionInboundPortURI, this);
		this.actionIBP.publishPort();

		this.getTracer().setTitle("SAMUStationProxy " + this.stationId);
		this.getTracer().setRelativePosition(2, 0);
		this.toggleTracing();
	}

	// -------------------------------------------------------------------------
	// Component life-cycle
	// -------------------------------------------------------------------------

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#start()
	 */
	@Override
	public synchronized void	start() throws ComponentStartException
	{
		try {
			this.doPortConnection(
					this.notificationOBP.getPortURI(),
					this.notificationInboundPortURI,
					SAMUNotificationConnector.class.getCanonicalName());
		} catch (Exception e) {
			throw new ComponentStartException(e) ;
		}
		super.start();
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#execute()
	 */
	@Override
	public synchronized void	execute() throws Exception
	{
		LocalTime startTime	=
				TimeManager.get().getSimulatedStartTime().plusSeconds(4);
		long startTimeDelay =
				TimeManager.get().localTime2nanoDelay(startTime);
		this.scheduleTask(
				o -> {
					try {
						System.out.println("(Expected RuleS18)");
						this.traceMessage("Ambulances available at " +
										  startTime + ".\n");
						System.out.println("(Expected RuleS19)");
						notificationOBP.notifyAmbulancesAvailable(startTime);
						this.traceMessage("Medics available at " + 
										  startTime + ".\n");
						notificationOBP.notifyMedicsAvailable(startTime);
					} catch (Throwable e) {
						e.printStackTrace();
					}
				},
				startTimeDelay, TimeUnit.NANOSECONDS);

		if (this.stationId.equals("SAMU-1")) {

			LocalTime healthAlarm1 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(15);
			long healthAlarm1NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm1);
			AbsolutePosition p1 = new AbsolutePosition(2.5, 2.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS1)");
							this.traceMessage(
								"Trigger health alarm of type EMERGENCY at " +
								healthAlarm1 + ".\n");
							notificationOBP.healthAlarm(
								p1, TypeOfHealthAlarm.EMERGENCY, healthAlarm1);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm1NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm2 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(55);
			long healthAlarm2NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm2);
			AbsolutePosition p2 = new AbsolutePosition(2.5, 1.0);
			this.scheduleTask(
					o -> {
						try {//System.out.println("Trigger health alarm of type EMERGENCY at " +healthAlarm2);
							System.out.println("(Expected RuleS1)");
							System.out.println("(Expected RuleS16)");
							this.traceMessage(
								"Trigger health alarm of type EMERGENCY at " +
								healthAlarm2 + ".\n");
							notificationOBP.healthAlarm(
								p2, TypeOfHealthAlarm.EMERGENCY, healthAlarm2);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm2NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm3 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(105);
			long healthAlarm3NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm3);
			AbsolutePosition p3 = new AbsolutePosition(2.5, 3.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS2)");
							System.out.println("(Expected RuleS9)");
							//System.out.println("Trigger health alarm of type EMERGENCY at " +
								//healthAlarm3);
							this.traceMessage(
								"Trigger health alarm of type EMERGENCY at " +
								healthAlarm3 + ".\n");
							notificationOBP.healthAlarm(
								p3, TypeOfHealthAlarm.EMERGENCY, healthAlarm3);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm3NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm4 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(140);
			long healthAlarm4NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm4);
			AbsolutePosition p4 = new AbsolutePosition(3.5, 2.0);
			this.scheduleTask(
					o -> {
						try {
							//System.out.println("Trigger health alarm of type TRACKING at " +
								//healthAlarm4);
							System.out.println("(Expected NONE)");
							this.traceMessage(
								"Trigger health alarm of type TRACKING at " +
								healthAlarm4 + ".\n");
							notificationOBP.trackingAlarm(
												p4, "personne-0", healthAlarm4);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm4NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm5 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(160);
			long healthAlarm5NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm5);
			AbsolutePosition p5 = new AbsolutePosition(3.5, 1.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS2)");
							System.out.println("(Expected RuleS9)");
							System.out.println("(Expected RuleS16 Plus d'ambulance)");
							//System.out.println("Trigger health alarm of type EMERGENCY at " +
							//	healthAlarm5);
							this.traceMessage(
								"Trigger health alarm of type EMERGENCY at " +
								healthAlarm5 + ".\n");
							notificationOBP.healthAlarm(
								p5, TypeOfHealthAlarm.EMERGENCY, healthAlarm5);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm5NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm6 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(170);
			long healthAlarm6NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm6);
			AbsolutePosition p6 = new AbsolutePosition(3.5, 2.0);
			this.scheduleTask(
					o -> {
						try {
							//System.out.println("BIS");
							//System.out.println(this.currentNumberOfAmbulances);
							System.out.println("(Expected RuleS2)");
							System.out.println("(Expected RuleS10bis)");
							this.traceMessage(									
								"Trigger health alarm of type EMERGENCY at " +
								healthAlarm6 + ".\n");
							notificationOBP.healthAlarm(
								p6, TypeOfHealthAlarm.EMERGENCY, healthAlarm6);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm6NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm7 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(285);
			long healthAlarm7NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm7);
			AbsolutePosition p7 = new AbsolutePosition(3.0, 3.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected NONE)");
							this.traceMessage(
								"Trigger health alarm of type TRACKING at " +
								healthAlarm7 + ".\n");
							notificationOBP.trackingAlarm(
												p7, "personne-2", healthAlarm7);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm7NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm8 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(325);
			long healthAlarm8NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm8);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS7)");
							this.traceMessage(
								"Trigger manual signal at " + healthAlarm8 +
								".\n");
							notificationOBP.manualSignal(
													"personne-2", healthAlarm8);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm8NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm9 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(860);
			long healthAlarm9NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm9);
			AbsolutePosition p9 = new AbsolutePosition(3.0, 3.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS5(delai10min))");
							System.out.println("(Expected RuleS3(delai10min))");
							System.out.println("(Expected RuleS3)");
							System.out.println("(Expected RuleS17)");
							this.traceMessage(
								"Trigger health alarm of type MEDICAL at " +
								healthAlarm9 + ".\n");
							notificationOBP.healthAlarm(
								p9, TypeOfHealthAlarm.MEDICAL, healthAlarm9);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm9NanoDelay, TimeUnit.NANOSECONDS);

		} else {

			assert	this.stationId.equals("SAMU-2");
			LocalTime healthAlarm1 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(35);
			long healthAlarm1NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm1);
			AbsolutePosition p1 = new AbsolutePosition(1.0, 0.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS3)");
							this.traceMessage(
								"Trigger health alarm of type MEDICAL at " +
								healthAlarm1 + ".\n");
							notificationOBP.healthAlarm(
								p1, TypeOfHealthAlarm.MEDICAL, healthAlarm1);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm1NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm2 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(75);
			long healthAlarm2NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm2);
			AbsolutePosition p2 = new AbsolutePosition(1.0, 3.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS3)");
							System.out.println("(Expected RuleS17)");
							this.traceMessage(
								"Trigger health alarm of type MEDICAL at " +
								healthAlarm2 + ".\n");
							notificationOBP.healthAlarm(
								p2, TypeOfHealthAlarm.MEDICAL, healthAlarm2);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm2NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm3 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(125);
			long healthAlarm3NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm3);
			AbsolutePosition p3 = new AbsolutePosition(1.0, 2.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS4)");
							System.out.println("(Expected RuleS11)");
							this.traceMessage(
								"Trigger health alarm of type MEDICAL at " +
								healthAlarm3 + ".\n");
							notificationOBP.healthAlarm(
								p3, TypeOfHealthAlarm.MEDICAL, healthAlarm3);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm3NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm4 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(260);
			long healthAlarm4NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm4);
			AbsolutePosition p4 = new AbsolutePosition(0.5, 2.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected NONE)");
							this.traceMessage(
								"Trigger health alarm of type TRACKING at " +
								healthAlarm4 + ".\n");
							notificationOBP.trackingAlarm(
												p4, "personne-1", healthAlarm4);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm4NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm5 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(795);
			long healthAlarm5NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm5);
			AbsolutePosition p5 = new AbsolutePosition(1.0, 2.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS3)");
							this.traceMessage(
								"Trigger health alarm of type MEDICAL at " +
								healthAlarm5 + ".\n");
							notificationOBP.healthAlarm(
								p5, TypeOfHealthAlarm.MEDICAL, healthAlarm5);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm5NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm6 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(820);
			long healthAlarm6NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm6);
			AbsolutePosition p6 = new AbsolutePosition(1.0, 3.5);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS3)");
							System.out.println("(Expected RuleS17)");
							this.traceMessage(
								"Trigger health alarm of type MEDICAL at " +
								healthAlarm6 + ".\n");
							notificationOBP.healthAlarm(
								p6, TypeOfHealthAlarm.MEDICAL, healthAlarm6);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm6NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm7 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(830);
			long healthAlarm7NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm7);
			AbsolutePosition p7 = new AbsolutePosition(0.5, 1.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected None)");
							this.traceMessage(
								"Trigger health alarm of type TRACKING at " +
								healthAlarm7 + ".\n");
							notificationOBP.trackingAlarm(
												p7, "personne-3", healthAlarm7);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm7NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm8 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(850);
			long healthAlarm8NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm8);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS8)");
							System.out.println("(Expected RuleS13)");
							this.traceMessage(
								"Trigger manual signal at " + healthAlarm8 +
								".\n");
							notificationOBP.manualSignal(
													"personne-3", healthAlarm8);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm8NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm9 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(910);
			long healthAlarm9NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm9);
			AbsolutePosition p9 = new AbsolutePosition(1.5, 1.0);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS6(delai10min))");
							System.out.println("(Expected RuleS12bis(delai10min))");
							System.out.println("(Expected NONE)");
							this.traceMessage(
								"Trigger health alarm of type TRACKING at " +
								healthAlarm9 + ".\n");
							notificationOBP.trackingAlarm(
												p9, "personne-4", healthAlarm9);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm9NanoDelay, TimeUnit.NANOSECONDS);
			LocalTime healthAlarm10 =
					TimeManager.get().getSimulatedStartTime().plusSeconds(940);
			long healthAlarm10NanoDelay =
					TimeManager.get().localTime2nanoDelay(healthAlarm10);
			this.scheduleTask(
					o -> {
						try {
							System.out.println("(Expected RuleS8)");
							System.out.println("(Expected RuleS15)");
							this.traceMessage(
								"Trigger manual signal at " + healthAlarm10 +
								".\n");
							notificationOBP.manualSignal(
													"personne-4", healthAlarm10);
						} catch (Throwable e) {
							e.printStackTrace();
						}
					},
					healthAlarm10NanoDelay, TimeUnit.NANOSECONDS);
		}
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#finalise()
	 */
	@Override
	public synchronized void	finalise() throws Exception
	{
		this.doPortDisconnection(this.notificationOBP.getPortURI());
		super.finalise();
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#shutdown()
	 */
	@Override
	public synchronized void	shutdown() throws ComponentShutdownException
	{
		try {
			this.notificationOBP.unpublishPort();
			this.actionIBP.unpublishPort();
		} catch (Exception e) {
			throw new ComponentShutdownException(e) ;
		}
		super.shutdown();
	}

	// -------------------------------------------------------------------------
	// Component services implementation
	// -------------------------------------------------------------------------

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.SAMUActionImplI#triggerIntervention(fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition, java.lang.String, fr.sorbonne_u.cps.smartcity.interfaces.TypeOfSAMURessources)
	 */
	@Override
	public void			triggerIntervention(
		AbsolutePosition position,
		String personId,
		TypeOfSAMURessources type
		) throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("SAMUStationProxy#triggerIntervention");
		}
		this.traceMessage(this.stationId + " triggers an intervention at " +
						  position + (personId != null ?
						  				" for person " + personId
						  			 :	"") +
						  " with resource type " + type + "\n");

		assert	position != null;
		assert	personId == null || !personId.isEmpty();
		assert	type != TypeOfSAMURessources.TELEMEDIC ||
									(personId != null && !personId.isEmpty());
		assert	type != TypeOfSAMURessources.AMBULANCE ||
											this.currentNumberOfAmbulances > 0;
		assert	(type != TypeOfSAMURessources.MEDIC
								&& type != TypeOfSAMURessources.TELEMEDIC) ||
											this.currentNumberOfMedics > 0;
		if (SmartCityDescriptorSmall.getPosition(this.stationId).equalAbsolutePosition(position))
			return;
		if (type != TypeOfSAMURessources.TELEMEDIC) {
			this.sendResource(type, position);
		}
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.traffic.components.StationEventTransmissionImplI#signalAtBase(fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle, java.time.LocalTime)
	 */
	@Override
	public void			signalAtBase(Vehicle v, LocalTime t) throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("SAMUStationProxy#signalAtBase");
		}
		this.notificationOBP.atStation(v.getId(), t);
		assert	this.currentlyOperating.contains(v.getId());
		this.currentlyOperating.remove(v.getId());
		if (this.ambulancesId.contains(v.getId())) {
			if (this.currentNumberOfAmbulances == 0) {
				this.notificationOBP.notifyAmbulancesAvailable(t);
			}
			this.currentNumberOfAmbulances++;
		} else {
			assert	this.medicsId.contains(v.getId());
			if (this.currentNumberOfMedics == 0) {
				this.notificationOBP.notifyMedicsAvailable(t);
			}
			this.currentNumberOfMedics++;
		}
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.traffic.components.StationEventTransmissionImplI#signalAtDestination(fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle, java.time.LocalTime)
	 */
	@Override
	public void			signalAtDestination(Vehicle v, LocalTime t)
	throws Exception
	{
		if (AbstractSmartCityCVM.DEBUG) {
			System.out.println("SAMUStationProxy#signalAtDestination");
		}
		assert	this.currentlyOperating.contains(v.getId());
		this.notificationOBP.atDestination(v.getId(), t);

		LocalTime current = TimeManager.get().getCurrentLocalTime();
		int duration;
		if (this.ambulancesId.contains(v.getId())) {
			duration = EMERGENCY_INTERVENTION_DURATION;
		} else {
			duration = MEDICAL_INTERVENTION_DURATION;
		}
		LocalTime end = current.plusMinutes(duration);
		long delay = TimeManager.get().localTime2nanoDelay(end);
		this.scheduleTask(
				o -> {
					try {
						v.returnBase();
						Direction d = v.firstDirection();
						this.requestPriority(v, v.getBase());
						this.traceMessage("Inserting vehicle " + v.toString()
										  + "\n");
						this.stationActionOBP.insertVehicle(
												v.getCurrentPosition(), v, d);
					} catch (Throwable e) {
						e.printStackTrace();
					}
				},
				delay, TimeUnit.NANOSECONDS);
	}

	// -------------------------------------------------------------------------
	// Component internal methods
	// -------------------------------------------------------------------------

	/**
	 * return the id of a vehicle corresponding to {@code r}.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code tr != null && tr instanceof TypeOfSAMURessources}
	 * post	{@code ret != null}
	 * </pre>
	 *
	 * @param r				type of required SAMU resource.
	 * @return				the id of a vehicle corresponding to {@code r}.
	 * @throws Exception	<i>to do</i>.
	 * @see fr.sorbonne_u.cps.smartcity.components.AbstractStationProxy#selectResource(fr.sorbonne_u.cps.smartcity.interfaces.TypeOfResourceI)
	 */
	@Override
	protected String	selectResource(TypeOfResourceI r) throws Exception
	{
		assert	r instanceof TypeOfSAMURessources;
		TypeOfSAMURessources ts = (TypeOfSAMURessources) r;
		String ret = null;
		if (ts == TypeOfSAMURessources.AMBULANCE) {
			this.currentNumberOfAmbulances--;
			for (String id : this.ambulancesId) {
				if (!this.currentlyOperating.contains(id)) {
					this.currentlyOperating.add(id);
					ret = id;
					break;
				}
			}
			if (this.currentNumberOfAmbulances == 0) {
				//System.out.println("this.notificationOBP.notifyNoAmbulanceAvailable:"+this.stationId);
				this.notificationOBP.notifyNoAmbulanceAvailable(
									TimeManager.get().getCurrentLocalTime());
			}
		} else {
			assert	ts == TypeOfSAMURessources.MEDIC;
			this.currentNumberOfMedics--;
			for (String id : this.medicsId) {
				if (!this.currentlyOperating.contains(id)) {
					this.currentlyOperating.add(id);
					ret = id;
					break;
				}
			}
			if (this.currentNumberOfMedics == 0) {
				this.notificationOBP.notifyNoMedicAvailable(
									TimeManager.get().getCurrentLocalTime());
			}
		}
		return ret;
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.components.AbstractStationProxy#requestPriority(fr.sorbonne_u.cps.smartcity.sim.vehicles.Vehicle, fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition)
	 */
	@Override
	protected void		requestPriority(Vehicle v, AbsolutePosition p)
	throws Exception
	{
//		System.out.println("******** requestPriority " + v + " " + p);
		assert	v != null && p != null;
		assert	v.getDestination().equals(p) || v.getBase().equals(p);
		
		if (v.firstDirection()==null)
			return;
		LocalTime t = TimeManager.get().getCurrentLocalTime();
		TypeOfTrafficLightPriority prio;
		if (this.ambulancesId.contains(v.getId())) {
			prio = TypeOfTrafficLightPriority.EMERGENCY;
		} else {
			if (v.firstDirection() == Direction.N ||
											v.firstDirection() == Direction.S) {
				prio = TypeOfTrafficLightPriority.NORTH_SOUTH;
			} else {
				assert	v.firstDirection() == Direction.E ||
											v.firstDirection() == Direction.W;
				prio = TypeOfTrafficLightPriority.EAST_WEST;
			}
		}
		this.notificationOBP.requestPriority(
				v.getCurrentPosition(),
				prio,
				v.getId(),
				p,
				t);
	}
}
// -----------------------------------------------------------------------------