package components.connector;

import components.interfaces.EventReceptionCI;
import event.interfaces.EventI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

public class ConnectorEventReception extends AbstractConnector implements EventReceptionCI {

	@Override
	public void receiveEvent(String emitterURI, EventI e) throws Exception {
		((EventReceptionCI) this.offering).receiveEvent(emitterURI, e);

	}

	@Override
	public void receiveEvents(String emitterURI, EventI[] e) throws Exception {
		((EventReceptionCI) this.offering).receiveEvents(emitterURI, e);

	}

}
