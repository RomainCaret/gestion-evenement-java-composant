package components.connector;

import java.io.Serializable;

import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionExecutionImpl;
import components.interfaces.ActionI;
import components.interfaces.ResponseI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

public class ConnectorActionExecution extends AbstractConnector implements ActionExecutionCI {

	@Override
	public void execute(ActionI a, Serializable[] params) throws Exception {
		((ActionExecutionCI) this.offering).execute(a, params);

	}

}
