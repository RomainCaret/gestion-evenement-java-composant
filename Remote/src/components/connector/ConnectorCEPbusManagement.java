package components.connector;

import components.interfaces.CEPBusManagementCI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

public class ConnectorCEPbusManagement extends AbstractConnector implements CEPBusManagementCI {

	@Override
	public void registerEmitter(String uri, String inboundPortResponseURI) throws Exception {
		((CEPBusManagementCI) this.offering).registerEmitter(uri, inboundPortResponseURI);
	}

	@Override
	public void unregisterEmitter(String uri) throws Exception {
		((CEPBusManagementCI) this.offering).unregisterEmitter(uri);

	}

	@Override
	public void registerCorrelator(String uri, String inboundPortURI, String inboundPortResponseURI) throws Exception {
		((CEPBusManagementCI) this.offering).registerCorrelator(uri, inboundPortURI, inboundPortResponseURI);

	}

	@Override
	public void unregisterCorrelator(String uri) throws Exception {
		((CEPBusManagementCI) this.offering).unregisterCorrelator(uri);

	}

	@Override
	public void registerExecutor(String uri, String inboundPortURI) throws Exception {
		((CEPBusManagementCI) this.offering).registerExecutor(uri, inboundPortURI);

	}

	@Override
	public void getExecutorInboundPortURI(String uri,String inboundPortResponseURI) throws Exception {
		((CEPBusManagementCI) this.offering).getExecutorInboundPortURI(uri,inboundPortResponseURI);

	}

	@Override
	public void unregisterExecutor(String uri) throws Exception {
		((CEPBusManagementCI) this.offering).unregisterExecutor(uri);

	}

	@Override
	public void subscribe(String subscriberURI, String emitterURI) throws Exception {
		((CEPBusManagementCI) this.offering).subscribe(subscriberURI, emitterURI);

	}

	@Override
	public void unsubscribe(String subscriberURI, String emitterURI) throws Exception {
		((CEPBusManagementCI) this.offering).unsubscribe(subscriberURI, emitterURI);

	}

}
