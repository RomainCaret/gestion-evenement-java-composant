package components.connector;

import components.interfaces.EventEmissionCI;
import event.interfaces.EventI;
import fr.sorbonne_u.components.connectors.AbstractConnector;

public class ConnectorEventEmission extends AbstractConnector implements EventEmissionCI {

	@Override
	public void sendEvent(String emitterURI, EventI event) throws Exception {
		 ((EventEmissionCI) this.offering).sendEvent(emitterURI, event);

	}

	@Override
	public void sendEvents(String emitterURI, EventI[] events) throws Exception {
		 ((EventEmissionCI) this.offering).sendEvents(emitterURI, events);

	}

}
