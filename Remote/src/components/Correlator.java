package components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Semaphore;

import components.connector.ConnectorActionExecution;
import components.connector.ConnectorCEPbusManagement;
import components.connector.ConnectorEventEmission;
import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionI;
import components.interfaces.CEPBusManagementCI;
import components.interfaces.EventEmissionCI;
import components.interfaces.EventReceptionCI;
import components.interfaces.EventReceptionImpl;
import components.interfaces.ResultReceptionCI;
import components.interfaces.ResultReceptorI;
import components.ports.ActionExecutionOutboundPort;
import components.ports.CEPBusManagementOutboundPort;
import components.ports.EventEmissionOutboundPort;
import components.ports.EventReceptionInboundPort;
import components.ports.ResultReceptionInboundPort;
import descriptor.CorrelatorSetRule;
import descriptor.Data;
import descriptor.ResponseMethod;
import event.classes.EventBase;
import event.classes.fireStation.AllLadderInIntervention;
import event.classes.fireStation.AllTruckInIntervention;
import event.classes.fireStation.FireAlarm;
import event.classes.fireStation.FireEnd;
import event.classes.fireStation.FireFirstAlarm;
import event.classes.fireStation.FireGeneralAlarm;
import event.classes.fireStation.FireSecondAlarm;
import event.classes.fireStation.LadderAvailable;
import event.classes.fireStation.RequestInterventionBuilding;
import event.classes.fireStation.RequestInterventionHouse;
import event.classes.fireStation.TruckAvailable;
import event.classes.samu.AllMedecinInIntervention;
import event.classes.samu.AllSamuInIntervention;
import event.classes.samu.ConsciousFall;
import event.classes.samu.HealthAlarm;
import event.classes.samu.MedecinAvailable;
import event.classes.samu.RequestIntervention;
import event.classes.samu.SamuAvailable;
import event.classes.samu.SignalManual;
import event.classes.trafficLight.RequestPriority;
import event.classes.trafficLight.VehiculePass;
import event.classes.trafficLight.WaitingVehiculePass;
import event.interfaces.EventBaseI;
import event.interfaces.EventI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import rule.classes.RuleBase;
import rule.interfaces.CorrelatorStateI;
import rule.interfaces.RuleBaseI;
import java.io.Serializable;

@OfferedInterfaces(offered = { EventReceptionCI.class, ResultReceptionCI.class })
@RequiredInterfaces(required = { ActionExecutionCI.class, EventEmissionCI.class, CEPBusManagementCI.class })

public class Correlator extends AbstractComponent implements EventReceptionImpl, ResultReceptorI {

	protected EventReceptionInboundPort erip;
	// public static final String ERIP_URI = "erip_uri";

	protected EventBaseI event_base;
	protected ArrayList<RuleBaseI> rule_bases;

	protected ArrayList<EventI> event_list = new ArrayList<EventI>();
	protected HashMap<String, CorrelatorStateI> map_state_exec;
	protected CEPBusManagementOutboundPort cepbus_managementOBP;
	protected EventEmissionOutboundPort event_emissionOBP;
	protected ResultReceptionInboundPort rrip;

	protected String correlatorID;
	protected ArrayList<String> subs_emitter;
	protected HashMap<String, ActionExecutionOutboundPort> map_action_executionOBP;
	protected Semaphore rule_fire;

	boolean isSamu = false;
	boolean isFireStation = false;
	boolean isTraffic = false;
	private int num_bus;

	/**
	 * Correlator est le constructeur de la classe Correlator
	 * 
	 * @param uri            de type String
	 * @param subs_emitter   de type ArrayList<String>
	 * @param map_state_exec de type HashMap<String, CorrelatorStateI>
	 * @param num_bus        de type int
	 * @throws Exception
	 */

	protected Correlator(String uri, ArrayList<String> subs_emitter, HashMap<String, CorrelatorStateI> map_state_exec,
			int num_bus) throws Exception {
		super(2, 0);
		this.map_state_exec = map_state_exec;
		this.correlatorID = uri;
		this.subs_emitter = subs_emitter;
		this.rule_bases = new ArrayList<RuleBaseI>();
		this.event_base = new EventBase();
		this.erip = new EventReceptionInboundPort(this);
		this.erip.publishPort();
		this.cepbus_managementOBP = new CEPBusManagementOutboundPort(this);
		this.cepbus_managementOBP.publishPort();
		this.event_emissionOBP = new EventEmissionOutboundPort(this);
		this.event_emissionOBP.publishPort();
		this.map_action_executionOBP = new HashMap<String, ActionExecutionOutboundPort>();
		this.rule_fire = new Semaphore(1);

		this.rrip = new ResultReceptionInboundPort(this);
		this.rrip.publishPort();

		this.num_bus = num_bus;
	}

	/**
	 * start est une méthode qui va connecter le correlateur au bus, elle ne
	 * retourne rien
	 */
	@Override
	public synchronized void start() throws ComponentStartException {
		super.start();
		try {

			this.doPortConnection(cepbus_managementOBP.getPortURI(), Data.uri_cepbus_mip + num_bus,
					ConnectorCEPbusManagement.class.getCanonicalName());

		} catch (Exception e) {
			throw new ComponentStartException(e);
		}
	}

	/**
	 * execute est une méthode qui permet au correlateur de s'enregister auprès du
	 * bus puis de s'abonner à ses emetteurs et enfin d'ajouter les règles en
	 * fonction de son emetteur.
	 */
	@Override
	public synchronized void execute() throws Exception {
		this.cepbus_managementOBP.registerCorrelator(correlatorID, erip.getPortURI(), this.rrip.getPortURI());

		Thread.sleep(1000L);
		for (String sub : subs_emitter) {
			cepbus_managementOBP.subscribe(correlatorID, sub);
		}
		Iterator<String> it = map_state_exec.keySet().iterator();
		String sub;

		while (it.hasNext()) {
			sub = it.next();

			CorrelatorStateI corr = map_state_exec.get(sub);
			corr.setCorrelator(this);
			if (corr.getClass().getSimpleName().equals("HealthCorrelatorState")) {
				this.rule_bases.add(CorrelatorSetRule.setRuleSamu(new RuleBase()));
				isSamu = true;
			}
			if (corr.getClass().getSimpleName().equals("FireCorrelatorState")) {
				this.rule_bases.add(CorrelatorSetRule.setRuleFireStation(new RuleBase()));
				isFireStation = true;
			}
			if (corr.getClass().getSimpleName().equals("TrafficLightCorrelatorState")) {
				this.rule_bases.add(CorrelatorSetRule.setRuleTrafficLight(new RuleBase()));
				isTraffic = true;
			}
			map_action_executionOBP.put(sub, new ActionExecutionOutboundPort(this));
			cepbus_managementOBP.getExecutorInboundPortURI(sub, this.rrip.getPortURI());

		}
		if (Data.DEBUG)
			System.out.println("CONNEXION_CORR1");
		super.execute();
	}

	/**
	 * finalise est une méthode qui permet de déconnecter toutes les connexions
	 * réalisées au cours de l'exécution.
	 */
	@Override
	public synchronized void finalise() throws Exception {
		this.doPortDisconnection(this.cepbus_managementOBP.getPortURI());

		this.doPortDisconnection(this.event_emissionOBP.getPortURI());

		Iterator<String> it = map_action_executionOBP.keySet().iterator();
		String sub;
		while (it.hasNext()) {
			sub = it.next();
			this.doPortDisconnection(map_action_executionOBP.get(sub).getPortURI());
		}
		super.finalise();
	}

	/**
	 * shutdown est une méthode qui permet de unpublish les ports
	 */
	@Override
	public synchronized void shutdown() throws ComponentShutdownException {
		try {
			this.event_emissionOBP.unpublishPort();
			this.cepbus_managementOBP.unpublishPort();
			Iterator<String> it = map_action_executionOBP.keySet().iterator();
			String sub;
			while (it.hasNext()) {
				sub = it.next();
				map_action_executionOBP.get(sub).unpublishPort();
			}
			this.rrip.unpublishPort();

			this.erip.unpublishPort();
		} catch (Exception e) {
			throw new ComponentShutdownException(e);
		}
		super.shutdown();

	}

	/**
	 * filter est une méthode qui retourne un boolean permettant de savoir si
	 * l'événement est destiné à ce correlateur
	 * 
	 * @param e de type EventI
	 * @return boolean
	 */
	public boolean filter(EventI e) {
		if (isFireStation == false) {
			if (e instanceof AllLadderInIntervention)
				return false;
			if (e instanceof AllTruckInIntervention)
				return false;
			if (e instanceof FireEnd)
				return false;
			if (e instanceof FireAlarm)
				return false;
			if (e instanceof FireGeneralAlarm)
				return false;
			if (e instanceof FireFirstAlarm)
				return false;
			if (e instanceof FireSecondAlarm)
				return false;
			if (e instanceof LadderAvailable)
				return false;
			if (e instanceof RequestInterventionBuilding)
				return false;
			if (e instanceof RequestInterventionHouse)
				return false;
			if (e instanceof TruckAvailable)
				return false;
		}
		if (isSamu == false) {
			if (e instanceof AllMedecinInIntervention)
				return false;
			if (e instanceof AllSamuInIntervention)
				return false;
			if (e instanceof ConsciousFall)
				return false;
			if (e instanceof HealthAlarm)
				return false;
			if (e instanceof MedecinAvailable)
				return false;
			if (e instanceof RequestIntervention)
				return false;
			if (e instanceof SamuAvailable)
				return false;
			if (e instanceof SignalManual)
				return false;
		}
		if (isTraffic == false) {
			if (e instanceof RequestPriority)
				return false;
			if (e instanceof VehiculePass)
				return false;
			if (e instanceof WaitingVehiculePass)
				return false;
		}
		if (e instanceof RequestIntervention) {
			String s = (String) e.getPropertyValue("nextStation");

			if (s != null && isExec(s))
				return true;
			return false;
		}
		if (e instanceof ConsciousFall) {
			String s = (String) e.getPropertyValue("nextStation");
			if (s != null && isExec(s))
				return true;
			return false;
		}
		if (e instanceof RequestInterventionBuilding) {
			String s = (String) e.getPropertyValue("nextBarracks");

			if (s != null && isExec(s))
				return true;
			return false;
		}
		if (e instanceof RequestInterventionHouse) {
			String s = (String) e.getPropertyValue("nextBarracks");

			if (s != null && isExec(s))
				return true;
			return false;
		}
		if (e instanceof FireSecondAlarm) {
			String s = (String) e.getPropertyValue("nextBarracks");

			if (s != null && isExec(s))
				return true;
			return false;
		}
		if (e instanceof RequestPriority) {
			IntersectionPosition is = (IntersectionPosition) e.getPropertyValue("intersection");

			if (is != null && isExec(is.toString()))
				return true;
			return false;
		}
		return true;
	}

	/**
	 * isExec() est une méthode interne vérifiant que le nom attendu est celui d'un
	 * des emetteurs du correlateur
	 * 
	 * @param s de type String
	 * @return boolean
	 */

	private boolean isExec(String s) {
		Iterator<String> it = map_state_exec.keySet().iterator();
		String sub;

		while (it.hasNext()) {
			sub = it.next();
			if (sub.equals(s))
				return true;
		}
		return false;
	}

	/**
	 * receiveEvent() est une méthode qui permet de recevoir un événement depuis le
	 * bus et le rajoute dans Event Base en executant fireAllOn() elle prend en
	 * parametre :
	 * 
	 * @param emitterURI de type String
	 * @param e          de type EventI
	 */
	@Override
	public void receiveEvent(String emitterURI, EventI e) throws Exception {
		if (Data.DEBUG)
			System.out.println("CorrelatorTrace : Entre dans Receive Event");
		if (filter(e) == false)
			return;
		rule_fire.acquire();
		this.event_base.addEvent(e);
		Iterator<String> it = map_state_exec.keySet().iterator();
		String sub;
		int iter = 0;

		while (it.hasNext()) {
			sub = it.next();
			CorrelatorStateI corr = map_state_exec.get(sub);
			if (Data.DEBUG)
				System.out.println("CorrelatorTrace : Event recu " + e);
			this.rule_bases.get(iter++).fireAllOn(event_base, corr);
		}
		rule_fire.release();
		if (Data.DEBUG)
			System.out.println("CorrelatorTrace : Sors dans Receive Event");
	}

	/**
	 * receiveEvents() est une méthode qui permet de recevoir des événements depuis
	 * le bus et le rajoute dans Event Base en executant fireAllOn() elle prend en
	 * parametre :
	 * 
	 * @param emitterURI de type String
	 * @param e          de type tableau d'EventI
	 */
	@Override
	public void receiveEvents(String emitterURI, EventI[] e) throws Exception {
		for (EventI iter : event_list) {
			if (filter(iter) == true)
				this.event_base.addEvent(iter);
		}
		Iterator<String> it = map_state_exec.keySet().iterator();
		String sub;
		int iter = 0;
		while (it.hasNext()) {
			sub = it.next();
			CorrelatorStateI corr = map_state_exec.get(sub);
			this.rule_bases.get(iter++).fireAllOn(event_base, corr);
		}
	}

	/**
	 * execute est une méthode qui permet d'appeler la méthode execute de la façade
	 * 
	 * @param a        de type ActionI
	 * @param params   de type Serializable[]
	 * @param uri_exec de type String
	 * @throws Exception
	 */
// pas l' execute du composant
	public void execute(ActionI a, Serializable[] params, String uri_exec) throws Exception {
		// System.out.println(uri_exec+" : "+this.map_action_executionOBP);
		if (Data.DEBUG)
			System.out.println("CorrelatorTrace : Execute : " + this.map_action_executionOBP.get(uri_exec));
		if (this.map_action_executionOBP.containsKey(uri_exec)) {
			// System.out.println(this.map_action_executionOBP.get(uri_exec));
			this.map_action_executionOBP.get(uri_exec).execute(a, params);
		} else {
			System.out.println("CorrelatorTrace : Erreur CorrelatorState non connecté à son executeur");
			return;
		}
	}

	/**
	 * sendEvent est une méthode qui permet au correlateur de renvoyer dans le bus
	 * l'événement qui a été construit par une règle
	 * 
	 * @param e de type EventI
	 */
	public void sendEvent(EventI e) {
		try {
			this.event_emissionOBP.sendEvent(this.correlatorID, e);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * acceptResult est une méthode qui fait la connexion avec les façades lorsque
	 * celles ci répondent par cette méthode
	 * 
	 * @param method de type ResponseMethod
	 * @param result de type Object
	 */

	@Override
	public void acceptResult(ResponseMethod method, Object result) throws Exception {

		switch (method) {
		case RegisterCorrelator:
			assert result instanceof String;

			String event_emission_IBP = (String) result;
			this.doPortConnection(event_emissionOBP.getPortURI(), event_emission_IBP,
					ConnectorEventEmission.class.getCanonicalName());
			break;
		case GetExecutor:
			assert result instanceof String[];
			String[] tab = (String[]) result;

			map_action_executionOBP.get(tab[0]).publishPort();
			this.doPortConnection(map_action_executionOBP.get(tab[0]).getPortURI(), tab[1],
					ConnectorActionExecution.class.getCanonicalName());
			break;

		default:
			break;
		}

	}
}
