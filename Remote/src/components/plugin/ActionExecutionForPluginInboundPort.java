package components.plugin;



import java.io.Serializable;

import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionExecutionImpl;
import components.interfaces.ActionI;
import components.interfaces.ResponseI;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;

public class ActionExecutionForPluginInboundPort extends AbstractInboundPort implements ActionExecutionCI {

		private static final long serialVersionUID = 1L;
	public ActionExecutionForPluginInboundPort(ComponentI owner,
    String pluginURI) throws Exception {
		super(ActionExecutionCI.class, owner, pluginURI, null);
	}

	public ActionExecutionForPluginInboundPort(String uri,
	ComponentI owner,
	String pluginURI
	) throws Exception
{
	super(uri, ActionExecutionCI.class, owner, pluginURI, null);
	}

	@Override
	public void execute(ActionI a, Serializable[] params) throws Exception {
		
		
		
		this.getOwner().runTask(
				o -> {	try {
					this.getOwner().handleRequest(new AbstractComponent.AbstractService<ResponseI>(this.getPluginURI()) {
						@Override
						public ResponseI call() throws Exception {
							((ActionExecutionImpl)
										this.getServiceProviderReference()).execute(a,params);
							return null;
						}
					});
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	}
	
	
}
