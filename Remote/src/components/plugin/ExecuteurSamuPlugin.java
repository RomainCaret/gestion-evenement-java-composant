package components.plugin;

import fr.sorbonne_u.components.AbstractPlugin;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.cps.smartcity.connections.SAMUActionConnector;
import fr.sorbonne_u.cps.smartcity.connections.SAMUActionOutboundPort;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.SAMUActionCI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfSAMURessources;
import components.CEPBus;
import components.connector.ConnectorCEPbusManagement;
import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionExecutionImpl;
import components.interfaces.ActionI;
import components.interfaces.CEPBusManagementCI;
import components.interfaces.ResponseI;
import components.interfaces.SamuStationActions;
import components.ports.CEPBusManagementOutboundPort;
import descriptor.Data;

import java.io.Serializable;

// -----------------------------------------------------------------------------
/**
 * The class <code>MapPlugin</code> implements the map component side plug-in
 * for the <code>MapCI</code> component interface and associated ports and
 * connectors.
 *
 * <p><strong>Description</strong></p>
 * 
 * <p>
 * This implementation forces calls to the services to pass by the plug-in, but
 * for this example, that would not be necessary. The inbound port could simply
 * call the implementation methods of the component without calling the plug-in
 * methods. This is done only to illustrate the case where the calls need to
 * pass by the plug-in implementation.
 * </p>
 * <p>
 * A good way to know when to pass by the plug-in implementation and when to
 * call directly the component is to note that here all of the methods simply
 * call their corresponding ones in the component as if we were implementing a
 * delegation pattern. In such a case, calling the component methods directly
 * would save one indirection. On the other hand, if some processing or
 * housekeeping work needs to be done at each call in the plug-in, then it
 * is necessary to pass by plug-in implementations of the methods.
 * </p>
 * <p>
 * To obtain the simpler implementation, this plug-in would not implement
 * (in the Java sense) the interface <code>MapImplementationI</code> and
 * the related methods. It would use the original inbound port
 * <code>MapInboundPort</code> rather than <code>MapInboundPortForPlugin</code>.
 * </p>
 * 
 * <p><strong>Invariant</strong></p>
 * 
 * <pre>
 * invariant		true
 * </pre>
 * 
 * <p>Created on : 2019-03-21</p>
 * 
 * @author	<a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
public class			ExecuteurSamuPlugin
extends 	AbstractPlugin implements ActionExecutionImpl
{
	// -------------------------------------------------------------------------
	// Plug-in variables and constants
	// -------------------------------------------------------------------------

	private static final long serialVersionUID = 1L;
	/** the inbound port which calls will be on this plug-in.				*/

    protected CEPBusManagementOutboundPort cepbusOBP;

	protected ActionExecutionForPluginInboundPort action_executionIBP;

    /** identifier of the corresponding SAMU station. */
	protected String stationId;
	// -------------------------------------------------------------------------
	// Life cycle
	// -------------------------------------------------------------------------
	protected SAMUActionOutboundPort actionOBP;
	protected String actionInboundPortURI;
	private int num_bus;

	/**
	 * create a map plug-in.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code executorServiceURI != null}
	 * post	true			// no postcondition.
	 * </pre>
	 *
	 */
	public				ExecuteurSamuPlugin(String s, String actionInboundPortURI, int num_bus)
	{
        super();
		this.actionInboundPortURI = actionInboundPortURI;
        this.stationId = s;
        this.num_bus = num_bus;
		
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractPlugin#installOn(fr.sorbonne_u.components.ComponentI)
	 */
	@Override
	public void			installOn(ComponentI owner) throws Exception
	{
		super.installOn(owner);
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractPlugin#initialise()
	 */
	@Override
	public void			initialise() throws Exception
	{
		super.initialise();
		// Add interfaces and create ports
		
        this.addOfferedInterface(ActionExecutionCI.class);
        if (this.getOwner().isRequiredInterface(CEPBusManagementCI.class) == false)
        	this.addRequiredInterface(CEPBusManagementCI.class);
        this.addRequiredInterface(SAMUActionCI.class);
 
		this.actionOBP = new SAMUActionOutboundPort(this.getOwner());
		this.actionOBP.publishPort();
        this.cepbusOBP = new CEPBusManagementOutboundPort(this.getOwner());
		this.cepbusOBP.publishPort();
		this.action_executionIBP = new ActionExecutionForPluginInboundPort(this.getOwner(),this.getPluginURI());
		this.action_executionIBP.publishPort();
		this.getOwner().doPortConnection(this.actionOBP.getPortURI(), this.actionInboundPortURI,
		SAMUActionConnector.class.getCanonicalName());
        this.getOwner().doPortConnection(this.cepbusOBP.getPortURI(), Data.uri_cepbus_mip + num_bus,
					ConnectorCEPbusManagement.class.getCanonicalName());
        this.cepbusOBP.registerExecutor(this.stationId, this.action_executionIBP.getPortURI());
        
	}

    /**
	 * @see fr.sorbonne_u.components.AbstractPlugin#finalise()
	 */
	@Override
	public void			finalise() throws Exception
	{
		this.getOwner().doPortDisconnection(this.cepbusOBP.getPortURI());
		this.getOwner().doPortDisconnection(this.actionOBP.getPortURI());
	}
	/**
	 * @see fr.sorbonne_u.components.AbstractPlugin#uninstall()
	 */
	@Override
	public void			uninstall() throws Exception
	{
		this.actionOBP.unpublishPort();
		this.action_executionIBP.unpublishPort();
		this.cepbusOBP.unpublishPort();
		
        this.action_executionIBP.destroyPort();
        this.cepbusOBP.destroyPort();
        this.actionOBP.destroyPort();

        this.removeOfferedInterface(ActionExecutionCI.class);
        this.removeRequiredInterface(SAMUActionCI.class);
		if (this.getOwner().isRequiredInterface(CEPBusManagementCI.class) == true)
    		this.removeRequiredInterface(CEPBusManagementCI.class);
	}


	public CEPBusManagementCI	getCEPBusManagementServicesReference()
	{
		return this.cepbusOBP;
	}
    
    @Override
	public void execute(ActionI a , Serializable[] params) throws Exception{
		assert a instanceof SamuStationActions;
		assert params != null && params.length == 3 && params[0] instanceof AbsolutePosition
		&& params[1] instanceof String && params[2] instanceof TypeOfSAMURessources;
		AbsolutePosition pos = (AbsolutePosition) params[0];
		String pers = (String)params[1];
		if (pers.equals(""))
			pers = null;
		TypeOfSAMURessources type = (TypeOfSAMURessources)params[2]; 
		switch ((SamuStationActions)a) {
			case SendHealthIntervention: this.actionOBP.triggerIntervention(pos,pers, type); break;
		}
		return ;
	}
}
// -----------------------------------------------------------------------------
