package components.plugin;

import fr.sorbonne_u.components.AbstractPlugin;
import fr.sorbonne_u.components.ComponentI;

import components.CEPBus;
import components.connector.ConnectorActionExecution;
import components.connector.ConnectorCEPbusManagement;
import components.connector.ConnectorEventEmission;
import components.interfaces.CEPBusManagementCI;
import components.interfaces.EventEmissionCI;
import components.interfaces.ResultReceptionCI;
import components.interfaces.ResultReceptorI;
import components.ports.CEPBusManagementOutboundPort;
import components.ports.EventEmissionOutboundPort;
import components.ports.ResultReceptionInboundPort;
import descriptor.Data;
import descriptor.ResponseMethod;

// -----------------------------------------------------------------------------
/**
 * The class <code>MapPlugin</code> implements the map component side plug-in
 * for the <code>MapCI</code> component interface and associated ports and
 * connectors.
 *
 * <p><strong>Description</strong></p>
 * 
 * <p>
 * This implementation forces calls to the services to pass by the plug-in, but
 * for this example, that would not be necessary. The inbound port could simply
 * call the implementation methods of the component without calling the plug-in
 * methods. This is done only to illustrate the case where the calls need to
 * pass by the plug-in implementation.
 * </p>
 * <p>
 * A good way to know when to pass by the plug-in implementation and when to
 * call directly the component is to note that here all of the methods simply
 * call their corresponding ones in the component as if we were implementing a
 * delegation pattern. In such a case, calling the component methods directly
 * would save one indirection. On the other hand, if some processing or
 * housekeeping work needs to be done at each call in the plug-in, then it
 * is necessary to pass by plug-in implementations of the methods.
 * </p>
 * <p>
 * To obtain the simpler implementation, this plug-in would not implement
 * (in the Java sense) the interface <code>MapImplementationI</code> and
 * the related methods. It would use the original inbound port
 * <code>MapInboundPort</code> rather than <code>MapInboundPortForPlugin</code>.
 * </p>
 * 
 * <p><strong>Invariant</strong></p>
 * 
 * <pre>
 * invariant		true
 * </pre>
 * 
 * <p>Created on : 2019-03-21</p>
 * 
 * @author	<a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
public class			EmitterPlugin
extends 	AbstractPlugin
implements ResultReceptorI
{
	// -------------------------------------------------------------------------
	// Plug-in variables and constants
	// -------------------------------------------------------------------------

	private static final long serialVersionUID = 1L;
	/** the inbound port which calls will be on this plug-in.				*/

    protected CEPBusManagementOutboundPort cepbusOBP;

	protected EventEmissionOutboundPort event_emissionOBP;
	
	protected ResultReceptionForPluginInboundPort rrip;

    /** identifier of the corresponding SAMU station. */
	protected String stationId;
	// -------------------------------------------------------------------------
	// Life cycle
	// -------------------------------------------------------------------------
	private int num_bus;

	/**
	 * create a map plug-in.
	 * 
	 * <p><strong>Contract</strong></p>
	 * 
	 * <pre>
	 * pre	{@code executorServiceURI != null}
	 * post	true			// no postcondition.
	 * </pre>
	 *
	 */
	public				EmitterPlugin(String s, int num_bus)
	{
        super();
        this.stationId = s;
        this.num_bus = num_bus;
		
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractPlugin#installOn(fr.sorbonne_u.components.ComponentI)
	 */
	@Override
	public void			installOn(ComponentI owner) throws Exception
	{
		super.installOn(owner);
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractPlugin#initialise()
	 */
	@Override
	public void			initialise() throws Exception
	{
		super.initialise();

		// Add interfaces and create ports
        this.addRequiredInterface(EventEmissionCI.class);
        this.addOfferedInterface(ResultReceptionCI.class);
		if (this.getOwner().isRequiredInterface(CEPBusManagementCI.class) == false)
 	       this.addRequiredInterface(CEPBusManagementCI.class);
		
        this.cepbusOBP = new CEPBusManagementOutboundPort(this.getOwner());
        this.cepbusOBP.publishPort();
		this.event_emissionOBP = new EventEmissionOutboundPort(this.getOwner());
		this.event_emissionOBP.publishPort();
		
		this.rrip = new ResultReceptionForPluginInboundPort(this.getOwner(),this.getPluginURI());
		this.rrip.publishPort();

		//Connections
        this.getOwner().doPortConnection(this.cepbusOBP.getPortURI(), Data.uri_cepbus_mip + num_bus,
					ConnectorCEPbusManagement.class.getCanonicalName());
                    
        this.cepbusOBP.registerEmitter(stationId, this.rrip.getPortURI());
	}

    /**
	 * @see fr.sorbonne_u.components.AbstractPlugin#finalise()
	 */
	@Override
	public void			finalise() throws Exception
	{
		this.getOwner().doPortDisconnection(this.cepbusOBP.getPortURI());
		this.getOwner().doPortDisconnection(this.event_emissionOBP.getPortURI());
	}
	/**
	 * @see fr.sorbonne_u.components.AbstractPlugin#uninstall()
	 */
	@Override
	public void			uninstall() throws Exception
	{
		this.event_emissionOBP.unpublishPort();
		this.cepbusOBP.unpublishPort();
		this.rrip.unpublishPort();
		
        this.event_emissionOBP.destroyPort();
        this.cepbusOBP.destroyPort();
        this.rrip.destroyPort();
        
        this.removeOfferedInterface(ResultReceptionCI.class);
        this.removeRequiredInterface(EventEmissionCI.class);
		if (this.getOwner().isRequiredInterface(CEPBusManagementCI.class) == true)
        	this.removeRequiredInterface(CEPBusManagementCI.class);
	}

	public EventEmissionCI	getEventEmissionServicesReference()
	{
		return this.event_emissionOBP;
	}


	public CEPBusManagementCI	getCEPBusManagementServicesReference()
	{
		return this.cepbusOBP;
	}
	
	@Override
	public void acceptResult(ResponseMethod method, Object result) throws Exception {
		
		
		switch (method) {
		case RegisterEmitter:
			assert result instanceof String;
			String inboundportBus = (String) result;
			this.getOwner().doPortConnection(this.event_emissionOBP.getPortURI(), inboundportBus, 
					ConnectorEventEmission.class.getCanonicalName());
			
		default:
			break;
		}
		
	}
}
// -----------------------------------------------------------------------------
