package components.plugin;


import components.interfaces.ResponseI;
import components.interfaces.ResultReceptionCI;
import components.interfaces.ResultReceptorI;
import descriptor.ResponseMethod;
import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;


public class ResultReceptionForPluginInboundPort extends AbstractInboundPort implements ResultReceptionCI{

	private static final long serialVersionUID = 1L;
	public ResultReceptionForPluginInboundPort(ComponentI owner,
    String pluginURI) throws Exception {
		super(ResultReceptionCI.class, owner, pluginURI, null);
	}
	@Override
	public void acceptResult(ResponseMethod method, Object result) throws Exception {
		this.getOwner().runTask(
				o -> {	try {
					this.getOwner().handleRequest(new AbstractComponent.AbstractService<ResponseI>(this.getPluginURI()) {
						@Override
						public ResponseI call() throws Exception {
							((ResultReceptorI)
										this.getServiceProviderReference()).acceptResult(method, result);;
							return null;
						}
					});
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
		
	}
	
}




