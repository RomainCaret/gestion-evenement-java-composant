package components.ports;

import components.CEPBus;
import components.interfaces.CEPBusManagementCI;
import components.interfaces.CEPBusManagementImpl;
import components.interfaces.EventEmissionImpl;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;

public class CEPBusManagementInboundPort extends AbstractInboundPort implements CEPBusManagementCI {

	public CEPBusManagementInboundPort(String uri, ComponentI owner) throws Exception {
		super(uri, CEPBusManagementCI.class, owner);
		assert owner instanceof CEPBusManagementImpl; 
	}

	public CEPBusManagementInboundPort(ComponentI owner) throws Exception {
		super(CEPBusManagementCI.class, owner);
		assert owner instanceof CEPBusManagementImpl;
	}

	private static final long serialVersionUID = 1L;

	@Override
	public void registerEmitter(String uri, String inboundPortResponseURI) throws Exception {

		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).registerEmitter(uri, inboundPortResponseURI);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	}
 
	@Override
	public void unregisterEmitter(String uri) throws Exception {
		
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).unregisterEmitter(uri);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	}

	@Override
	public void registerCorrelator(String uri, String inboundPortURI, String inboundPortResponseURI) throws Exception {
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).registerCorrelator(uri, inboundPortURI,inboundPortResponseURI);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	
	}

	@Override
	public void unregisterCorrelator(String uri) throws Exception {
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).unregisterCorrelator(uri);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });

	}

	@Override
	public void registerExecutor(String uri, String inboundPortURI) throws Exception {
		
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).registerExecutor(uri, inboundPortURI);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });

	}

	@Override
	public void getExecutorInboundPortURI(String uri, String inboundPortResponseURI) throws Exception {
		
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).getExecutorInboundPortURI(uri, inboundPortResponseURI);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	}

	@Override
	public void unregisterExecutor(String uri) throws Exception {
		
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).unregisterExecutor(uri);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	}

	@Override
	public void subscribe(String subscriberURI, String emitterURI) throws Exception {
		
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).subscribe(subscriberURI, emitterURI);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });

	}

	@Override
	public void unsubscribe(String subscriberURI, String emitterURI) throws Exception {
		this.getOwner().runTask(
				o -> {	try {
					((CEPBusManagementImpl) o).unsubscribe(subscriberURI, emitterURI);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });

	}

}
