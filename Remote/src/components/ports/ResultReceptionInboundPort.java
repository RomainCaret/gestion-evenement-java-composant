package components.ports;

import components.interfaces.ResultReceptionCI;
import components.interfaces.ResultReceptorI;
import descriptor.ResponseMethod;

import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;


public class			ResultReceptionInboundPort
extends		AbstractInboundPort
implements	ResultReceptionCI
{
	private static final long serialVersionUID = 1L;

	public				ResultReceptionInboundPort(ComponentI owner)
	throws Exception
	{
		super(ResultReceptionCI.class, owner);
		assert	owner instanceof ResultReceptorI;
	}

	public				ResultReceptionInboundPort(String uri, ComponentI owner)
	throws Exception
	{
		super(uri, ResultReceptionCI.class, owner);
		assert	owner instanceof ResultReceptorI;
	}

	@Override
	public void acceptResult(ResponseMethod method, Object result) throws Exception
	{
		this.getOwner().runTask(
						o -> {	try {
									((ResultReceptorI)o).
											acceptResult(method, result);
								} catch (Exception e) {
									throw new RuntimeException(e);
								}
							 });
	}
}
// -----------------------------------------------------------------------------
