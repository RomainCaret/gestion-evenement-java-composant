package components.ports;

import components.interfaces.CEPBusManagementCI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;

public class CEPBusManagementOutboundPort extends AbstractOutboundPort implements CEPBusManagementCI {

	private static final long serialVersionUID = 1L;

	public CEPBusManagementOutboundPort(String uri, ComponentI owner) throws Exception {
		super(uri, CEPBusManagementCI.class, owner);
	}

	public CEPBusManagementOutboundPort(ComponentI owner) throws Exception {
		super(CEPBusManagementCI.class, owner);
	}
  
	@Override
	public void registerEmitter(String uri, String inboundPortResponseURI) throws Exception {
		((CEPBusManagementCI) this.getConnector()).registerEmitter(uri, inboundPortResponseURI);
	}

	@Override
	public void unregisterEmitter(String uri) throws Exception {
		((CEPBusManagementCI) this.getConnector()).unregisterEmitter(uri);
	}

	@Override
	public void registerCorrelator(String uri, String inboundPortURI, String inboundPortResponseURI) throws Exception {
		((CEPBusManagementCI) this.getConnector()).registerCorrelator(uri, inboundPortURI,inboundPortResponseURI );
	}

	@Override
	public void unregisterCorrelator(String uri) throws Exception {
		((CEPBusManagementCI) this.getConnector()).unregisterCorrelator(uri);
	}

	@Override
	public void registerExecutor(String uri, String inboundPortURI) throws Exception {
		((CEPBusManagementCI) this.getConnector()).registerExecutor(uri, inboundPortURI);
	}

	@Override
	public void getExecutorInboundPortURI(String uri, String inboundPortResponseURI) throws Exception {
		((CEPBusManagementCI) this.getConnector()).getExecutorInboundPortURI(uri, inboundPortResponseURI);
	}

	@Override
	public void unregisterExecutor(String uri) throws Exception {
		((CEPBusManagementCI) this.getConnector()).unregisterExecutor(uri);
	}

	@Override
	public void subscribe(String subscriberURI, String emitterURI) throws Exception {
		((CEPBusManagementCI) this.getConnector()).subscribe(subscriberURI, emitterURI);
	}

	@Override
	public void unsubscribe(String subscriberURI, String emitterURI) throws Exception {
		((CEPBusManagementCI) this.getConnector()).unsubscribe(subscriberURI, emitterURI);
	}

}
