package components.ports;

import components.interfaces.ActionExecutionCI;
import components.interfaces.EventEmissionCI;
import event.interfaces.EventI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;

public class EventEmissionOutboundPort extends AbstractOutboundPort implements EventEmissionCI {

	
	
	private static final long serialVersionUID = 1L;

	public EventEmissionOutboundPort(String uri, ComponentI owner) throws Exception {
		super(uri, ActionExecutionCI.class, owner);
	}

	public EventEmissionOutboundPort(ComponentI owner) throws Exception {
		super(EventEmissionCI.class, owner);
	}
	
	@Override
	public void sendEvent(String emitterURI, EventI event) throws Exception {
		((EventEmissionCI) this.getConnector()).sendEvent(emitterURI,event);

	}

	@Override
	public void sendEvents(String emitterURI, EventI[] events) throws Exception {
		((EventEmissionCI) this.getConnector()).sendEvents(emitterURI,events);
	}

}
