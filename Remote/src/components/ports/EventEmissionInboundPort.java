package components.ports;

import components.CEPBus;
import components.interfaces.EventEmissionCI;
import components.interfaces.EventEmissionImpl;
import event.interfaces.EventI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;

public class EventEmissionInboundPort extends AbstractInboundPort implements EventEmissionCI {

	private static final long serialVersionUID = 1L;

	public EventEmissionInboundPort(String uri, ComponentI owner) throws Exception {
		super(uri, EventEmissionCI.class, owner);
		assert owner instanceof EventEmissionImpl;
	}

	public EventEmissionInboundPort(ComponentI owner) throws Exception {
		super(EventEmissionCI.class, owner);
		assert owner instanceof EventEmissionImpl;
	} 

	@Override
	public void sendEvent(String emitterURI, EventI event) throws Exception {		
		this.getOwner().runTask(
				o -> {	try {
					((EventEmissionImpl) o).sendEvent(emitterURI, event);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	}

	@Override
	public void sendEvents(String emitterURI, EventI[] events) throws Exception {
		this.getOwner().runTask(
				o -> {	try {
					((EventEmissionImpl) o).sendEvents(emitterURI, events);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });

	}

}
