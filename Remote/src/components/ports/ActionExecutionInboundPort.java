package components.ports;

import java.io.Serializable;

import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionExecutionImpl;
import components.interfaces.ActionI;
import components.interfaces.EventEmissionImpl;
import components.interfaces.ResponseI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;

public class ActionExecutionInboundPort extends AbstractInboundPort implements ActionExecutionCI {

		private static final long serialVersionUID = 1L;
	public ActionExecutionInboundPort(String uri, ComponentI owner) throws Exception {
		super(uri, ActionExecutionCI.class, owner);
		assert	owner instanceof ActionExecutionImpl;
	}

	public ActionExecutionInboundPort(ComponentI owner) throws Exception {
		super(ActionExecutionCI.class, owner);
		assert	owner instanceof ActionExecutionImpl;
	}

	@Override
	public void execute(ActionI a, Serializable[] params) throws Exception {
		//return this.getOwner().handleRequest(c -> ((ActionExecutionImpl) c).execute(a,params));
		this.getOwner().runTask(
				o -> {	try {
					((ActionExecutionImpl) o).execute(a,params);
						} catch (Exception e) {
							e.printStackTrace();
						}
					 });
	}
	
	
}
