package components.ports;

import components.Correlator;
import components.interfaces.EventEmissionImpl;
import components.interfaces.EventReceptionCI;
import components.interfaces.EventReceptionImpl;
import event.interfaces.EventI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;

public class EventReceptionInboundPort extends AbstractInboundPort implements EventReceptionCI {

	private static final long serialVersionUID = 1L;

	public EventReceptionInboundPort(String uri, ComponentI owner) throws Exception {
		super(uri,  EventReceptionCI.class, owner);
		assert owner instanceof EventReceptionImpl;
	} 

	public EventReceptionInboundPort(ComponentI owner) throws Exception {
		super(EventReceptionCI.class, owner);
		assert owner instanceof EventReceptionImpl;
	}

	@Override
	public void receiveEvent(String emitterURI, EventI e) throws Exception {
		
		this.getOwner().runTask(
				o -> {	try {
					((EventReceptionImpl) o).receiveEvent(emitterURI, e);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					 });

	}

	@Override
	public void receiveEvents(String emitterURI, EventI[] e) throws Exception {
		this.getOwner().runTask(
				o -> {	try {
					((EventReceptionImpl) o).receiveEvents(emitterURI, e);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					 });

	}

}
