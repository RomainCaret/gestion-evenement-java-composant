package components.ports;

import components.interfaces.EventReceptionCI;
import event.interfaces.EventI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;

public class EventReceptionOutboundPort extends AbstractOutboundPort implements EventReceptionCI {

	private static final long serialVersionUID = 1L;

	public EventReceptionOutboundPort(String uri, ComponentI owner) throws Exception {
		super(uri, EventReceptionCI.class, owner);
	}

	public EventReceptionOutboundPort(ComponentI owner) throws Exception {
		super(EventReceptionCI.class, owner);
	}

	@Override
	public void receiveEvent(String emitterURI, EventI e) throws Exception {
		((EventReceptionCI) this.getConnector()).receiveEvent(emitterURI, e);

	}

	@Override
	public void receiveEvents(String emitterURI, EventI[] e) throws Exception {
		((EventReceptionCI) this.getConnector()).receiveEvents(emitterURI, e);

	}

}
