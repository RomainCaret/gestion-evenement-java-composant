package components.ports;

import components.interfaces.ResultReceptionCI;
import descriptor.ResponseMethod;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;


public class			ResultReceptionOutboundPort
extends		AbstractOutboundPort
implements	ResultReceptionCI
{
	private static final long serialVersionUID = 1L;

	public				ResultReceptionOutboundPort(ComponentI owner)
	throws Exception
	{
		super(ResultReceptionCI.class, owner);
	}

	public				ResultReceptionOutboundPort(
		String uri,
		ComponentI owner
		) throws Exception
	{
		super(uri, ResultReceptionCI.class, owner);
	}


	@Override
	public void acceptResult(ResponseMethod method, Object result) throws Exception {
		((ResultReceptionCI)this.getConnector()).acceptResult(method, result);
		
	}
}
