package components.ports;

import java.io.Serializable;

import components.CEPBus;
import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionI;
import components.interfaces.ResponseI;
import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractOutboundPort;

public class ActionExecutionOutboundPort extends AbstractOutboundPort implements ActionExecutionCI {

	
	private static final long serialVersionUID = 1L;
	
	public ActionExecutionOutboundPort(String uri, ComponentI owner) throws Exception {
		super(uri, ActionExecutionCI.class, owner);
	}

	public ActionExecutionOutboundPort(ComponentI owner) throws Exception {
		super(ActionExecutionCI.class, owner);
	}

	@Override
	public void execute(ActionI a, Serializable[] params) throws Exception{
		((ActionExecutionCI) this.getConnector()).execute(a,params);
	}
	


}
