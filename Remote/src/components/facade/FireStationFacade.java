package components.facade;

// Copyright Jacques Malenfant, Sorbonne Universite.
// Jacques.Malenfant@lip6.fr
//
// This software is a computer program whose purpose is to provide a
// basic component programming model to program with components
// distributed applications in the Java programming language.
//
// This software is governed by the CeCILL-C license under French law and
// abiding by the rules of distribution of free software.  You can use,
// modify and/ or redistribute the software under the terms of the
// CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
// URL "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL-C license and that you accept its terms.

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.cps.smartcity.connections.FireStationActionConnector;
import fr.sorbonne_u.cps.smartcity.connections.FireStationActionOutboundPort;
import fr.sorbonne_u.cps.smartcity.connections.FireStationNotificationInboundPort;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.interfaces.FireStationActionCI;
import fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationCI;
import fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFire;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFirefightingResource;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;

import java.io.Serializable;
import java.time.LocalTime;

import components.CEPBus;
import components.connector.ConnectorCEPbusManagement;
import components.connector.ConnectorEventEmission;
import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionExecutionImpl;
import components.interfaces.ActionI;
import components.interfaces.CEPBusManagementCI;
import components.interfaces.EventEmissionCI;

import components.interfaces.FireStationActions;
import components.interfaces.ResponseI;
import components.ports.ActionExecutionInboundPort;
import components.ports.CEPBusManagementOutboundPort;
import components.ports.EventEmissionOutboundPort;
import descriptor.BasicSimSmartCityDescriptor;
import descriptor.Data;
import event.classes.fireStation.AllLadderInIntervention;
import event.classes.fireStation.AllTruckInIntervention;
import event.classes.fireStation.FireAlarm;
import event.classes.fireStation.FireEnd;
import event.classes.fireStation.LadderAvailable;
import event.classes.fireStation.TruckAvailable;
import event.interfaces.AtomicEventI;

// -----------------------------------------------------------------------------
/**
 * The class <code>FireStationFacade</code> implements a component that can
 * inspire the programming of events emitter and action executors components in
 * the CEP system applied to the smart city application.
 *
 * <p>
 * <strong>Description</strong>
 * </p>
 * 
 * <p>
 * <strong>Invariant</strong>
 * </p>
 * 
 * <pre>
 * invariant		true
 * </pre>
 * 
 * <p>
 * Created on : 2022-01-27
 * </p>
 * 
 * @author <a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
@OfferedInterfaces(offered = { FireStationNotificationCI.class, ActionExecutionCI.class })
@RequiredInterfaces(required = { FireStationActionCI.class, CEPBusManagementCI.class, EventEmissionCI.class })
public class FireStationFacade extends AbstractComponent implements FireStationNotificationImplI, ActionExecutionImpl {
	// -------------------------------------------------------------------------
	// Constants and variables
	// -------------------------------------------------------------------------

	/** identifier of the corresponding fire station. */
	protected String stationId;
	/** URI of the action inbound port. */
	protected String actionInboundPortURI;
	/** notifihjcation inbound port. */
	protected FireStationNotificationInboundPort notificationIBP;
	/** action outbound port. */
	protected FireStationActionOutboundPort actionOBP;

	protected CEPBusManagementOutboundPort cepbusOBP;

	protected EventEmissionOutboundPort event_emissionOBP;

	protected ActionExecutionInboundPort action_executionIBP;

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * create a fire station facade component.
	 * 
	 * <p>
	 * <strong>Contract</strong>
	 * </p>
	 * 
	 * <pre>
	 * pre	{@code BasicSimSmartCityDescriptor.isValidFireStationId(stationId)}
	 * pre	{@code notificationInboundPortURI != null && !notificationInboundPortURI.isEmpty()}
	 * pre	{@code actionInboundPortURI != null && !actionInboundPortURI.isEmpty()}
	 * post	true		// no postcondition.
	 * </pre>
	 *
	 * @param stationId                  identifier of the corresponding fire
	 *                                   station.
	 * @param notificationInboundPortURI URI of the notification inbound port to be
	 *                                   used by this facade component.
	 * @param actionInboundPortURI       URI of the action inbound port of the proxy
	 *                                   component.
	 * @throws Exception <i>to do</i>.
	 */
	protected FireStationFacade(String stationId, String notificationInboundPortURI, String actionInboundPortURI)
			throws Exception {
		super(2, 0);

		assert BasicSimSmartCityDescriptor.isValidFireStationId(stationId);
		assert notificationInboundPortURI != null && !notificationInboundPortURI.isEmpty();
		assert actionInboundPortURI != null && !actionInboundPortURI.isEmpty();

		this.stationId = stationId;
		this.actionInboundPortURI = actionInboundPortURI;
		this.notificationIBP = new FireStationNotificationInboundPort(notificationInboundPortURI, this);
		this.notificationIBP.publishPort();
		this.actionOBP = new FireStationActionOutboundPort(this);
		this.actionOBP.publishPort();
		this.cepbusOBP = new CEPBusManagementOutboundPort(this);
		this.cepbusOBP.publishPort();
		this.event_emissionOBP = new EventEmissionOutboundPort(this);
		this.event_emissionOBP.publishPort();
		this.action_executionIBP = new ActionExecutionInboundPort(this);
		this.action_executionIBP.publishPort();
	}

	// -------------------------------------------------------------------------
	// Component life-cycle
	// -------------------------------------------------------------------------

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#start()
	 */
	@Override
	public synchronized void start() throws ComponentStartException {
		super.start();

		try {
			this.doPortConnection(this.actionOBP.getPortURI(), this.actionInboundPortURI,
					FireStationActionConnector.class.getCanonicalName());
			this.doPortConnection(this.cepbusOBP.getPortURI(), Data.uri_cepbus_mip,
					ConnectorCEPbusManagement.class.getCanonicalName());

		} catch (Exception e) {
			throw new ComponentStartException(e);
		}
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#execute()
	 */
	@Override
	public synchronized void execute() throws Exception {
		/*String inboundportBus = this.cepbusOBP.registerEmitter(stationId);
		this.doPortConnection(this.event_emissionOBP.getPortURI(), inboundportBus,
				ConnectorEventEmission.class.getCanonicalName());
		this.cepbusOBP.registerExecutor(this.stationId, this.action_executionIBP.getPortURI());*/
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#finalise()
	 */
	@Override
	public synchronized void finalise() throws Exception {
		this.doPortDisconnection(this.cepbusOBP.getPortURI());
		this.doPortDisconnection(this.actionOBP.getPortURI());
		this.doPortDisconnection(this.event_emissionOBP.getPortURI());
		super.finalise();
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#shutdown()
	 */
	@Override
	public synchronized void shutdown() throws ComponentShutdownException {
		try {
			this.notificationIBP.unpublishPort();
			this.actionOBP.unpublishPort();
			this.event_emissionOBP.unpublishPort();
			this.action_executionIBP.unpublishPort();
			this.cepbusOBP.unpublishPort();
		} catch (Exception e) {
			throw new ComponentShutdownException(e);
		}
		super.shutdown();
	}

	// -------------------------------------------------------------------------
	// Component services implementation
	// -------------------------------------------------------------------------

	@Override
	public void execute(ActionI a, Serializable[] params) throws Exception {

		assert a instanceof FireStationActions;
		assert params != null && params.length == 2 && params[0] instanceof AbsolutePosition
				&& params[1] instanceof TypeOfFirefightingResource;
		AbsolutePosition pos = (AbsolutePosition) params[0];
		TypeOfFirefightingResource type = (TypeOfFirefightingResource) params[1];
		switch ((FireStationActions) a) {
		case SendFirstAlarm:
			this.actionOBP.triggerFirstAlarm(pos, type);
			break;
		case SendSecondAlarm:
			this.actionOBP.triggerSecondAlarm(pos);
			break;
		case SendGeneralAlarm:
			this.actionOBP.triggerGeneralAlarm(pos);
			break;
		}
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#fireAlarm(fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition,
	 *      java.time.LocalTime, fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFire)
	 */
	@Override
	public void fireAlarm(AbsolutePosition position, LocalTime occurrence, TypeOfFire type) throws Exception {
		AtomicEventI fa = new FireAlarm(occurrence, type, position);
		if (Data.DEBUG)
			System.out.println(
					"Fire alarm of type " + type + " received from position " + position + " at " + occurrence);
		this.event_emissionOBP.sendEvent(this.stationId, fa);

	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#endOfFire(fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition,
	 *      java.time.LocalTime)
	 */
	@Override
	public void endOfFire(AbsolutePosition position, LocalTime occurrence) throws Exception {
		
		 AtomicEventI fe = new FireEnd(occurrence, position); 
		 if (Data.DEBUG)
			 System.out.println("End of fire received from position " + position + " at "
		  + occurrence); 
		 this.event_emissionOBP.sendEvent(this.stationId, fe);
		 
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#requestPriority(fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition,
	 *      fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority,
	 *      java.lang.String, fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition,
	 *      java.time.LocalTime)
	 */
	@Override
	public void requestPriority(IntersectionPosition intersection, TypeOfTrafficLightPriority priority,
			String vehicleId, AbsolutePosition destination, LocalTime occurrence) throws Exception {
		/*
		 * AtomicEventI fe = new FireEnd(occurrence, priority, position,intersection);
		 * 
		 * if (Data.DEBUG) System.out.println("priority " + priority +
		 * " requested for vehicle " + vehicleId + " at intersection " + intersection +
		 * " towards " + destination + " at " + occurrence);
		 * this.event_emissionOBP.sendEvent(this.stationId, fe);
		 */

	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#atDestination(java.lang.String,
	 *      java.time.LocalTime)
	 */
	@Override
	public void atDestination(String vehicleId, LocalTime occurrence) throws Exception {
		if (Data.DEBUG)
			System.out.println("Vehicle " + vehicleId + " has arrived at destination.");
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#atStation(java.lang.String,
	 *      java.time.LocalTime)
	 */
	@Override
	public void atStation(String vehicleId, LocalTime occurrence) throws Exception {
		if (Data.DEBUG)
			System.out.println("Vehicle " + vehicleId + " has arrived at station.");
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#notifyNoStandardTruckAvailable(java.time.LocalTime)
	 */
	@Override
	public void notifyNoStandardTruckAvailable(LocalTime occurrence) throws Exception {

		AtomicEventI nta = new AllTruckInIntervention(occurrence);
		if (Data.DEBUG)
			System.out.println("No standard truck available received at " + occurrence);
		this.event_emissionOBP.sendEvent(this.stationId, nta);
	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#notifyStandardTrucksAvailable(java.time.LocalTime)
	 */
	@Override
	public void notifyStandardTrucksAvailable(LocalTime occurrence) throws Exception {
		AtomicEventI ta = new TruckAvailable(occurrence);

		if (Data.DEBUG)
			System.out.println("Standard trucks available received at " + occurrence);
		this.event_emissionOBP.sendEvent(this.stationId, ta);

	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#notifyNoHighLadderTruckAvailable(java.time.LocalTime)
	 */
	@Override
	public void notifyNoHighLadderTruckAvailable(LocalTime occurrence) throws Exception {
		AtomicEventI nla = new AllLadderInIntervention(occurrence);

		if (Data.DEBUG)
			System.out.println("No high ladder truck available received at " + occurrence);
		this.event_emissionOBP.sendEvent(this.stationId, nla);

	}

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.FireStationNotificationImplI#notifyHighLadderTrucksAvailable(java.time.LocalTime)
	 */
	@Override
	public void notifyHighLadderTrucksAvailable(LocalTime occurrence) throws Exception {
		AtomicEventI la = new LadderAvailable(occurrence);

		if (Data.DEBUG)
			System.out.println("High ladder trucks available received at " + occurrence);
		this.event_emissionOBP.sendEvent(this.stationId, la);

	}

}
// -----------------------------------------------------------------------------
