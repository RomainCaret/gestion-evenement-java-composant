package components.facade;

// Copyright Jacques Malenfant, Sorbonne Universite.
// Jacques.Malenfant@lip6.fr
//
// This software is a computer program whose purpose is to provide a
// basic component programming model to program with components
// distributed applications in the Java programming language.
//
// This software is governed by the CeCILL-C license under French law and
// abiding by the rules of distribution of free software.  You can use,
// modify and/ or redistribute the software under the terms of the
// CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
// URL "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL-C license and that you accept its terms.

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.cps.smartcity.grid.Direction;
import fr.sorbonne_u.cps.smartcity.connections.TrafficLightActionConnector;
import fr.sorbonne_u.cps.smartcity.connections.TrafficLightActionOutboundPort;
import fr.sorbonne_u.cps.smartcity.connections.TrafficLightNotificationInboundPort;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TrafficLightActionCI;
import fr.sorbonne_u.cps.smartcity.interfaces.TrafficLightNotificationCI;
import fr.sorbonne_u.cps.smartcity.interfaces.TrafficLightNotificationImplI;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;
import java.io.Serializable;
import java.time.LocalTime;

import components.CEPBus;
import components.connector.ConnectorCEPbusManagement;
import components.connector.ConnectorEventEmission;
import components.interfaces.ActionExecutionCI;
import components.interfaces.ActionExecutionImpl;
import components.interfaces.ActionI;
import components.interfaces.CEPBusManagementCI;
import components.interfaces.EventEmissionCI;
import components.interfaces.FireStationActions;
import components.interfaces.ResponseI;
import components.ports.ActionExecutionInboundPort;
import components.ports.CEPBusManagementOutboundPort;
import components.ports.EventEmissionOutboundPort;
import descriptor.BasicSimSmartCityDescriptor;
import descriptor.Data;

// -----------------------------------------------------------------------------
/**
 * The class <code>TrafficLightFacade</code>
 *
 * <p>
 * <strong>Description</strong>
 * </p>
 * 
 * <p>
 * <strong>Invariant</strong>
 * </p>
 * 
 * <pre>TrafficLightNotificationCI
 * invariant		true
 * </pre>
 * 
 * <p>
 * Created on : 2022-02-13
 * </p>
 * 
 * @author <a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
@OfferedInterfaces(offered = { TrafficLightNotificationCI.class , ActionExecutionCI.class })
@RequiredInterfaces(required = { TrafficLightActionCI.class ,  CEPBusManagementCI.class, EventEmissionCI.class})
public class TrafficLightFacade extends AbstractComponent implements TrafficLightNotificationImplI, ActionExecutionImpl{
	// -------------------------------------------------------------------------
	// Constants and variables
	// -------------------------------------------------------------------------

	protected String stationId;
	/** position of the traffic light. */
	protected IntersectionPosition position;
	/** URI of the action inbound port. */
	protected String actionInboundPortURI;
	/** notification inbound port. */
	protected TrafficLightNotificationInboundPort notificationIBP;
	/** action outbound port. */
	protected TrafficLightActionOutboundPort actionOBP;

	protected CEPBusManagementOutboundPort cepbusOBP;

	protected EventEmissionOutboundPort event_emissionOBP;

	protected ActionExecutionInboundPort action_executionIBP;
	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	protected TrafficLightFacade(IntersectionPosition position, String notificationInboundPortURI,
			String actionInboundPortURI, String stationID) throws Exception {
		super(2, 0);

		assert position != null && BasicSimSmartCityDescriptor.isInCity(position);
		assert actionInboundPortURI != null && !actionInboundPortURI.isEmpty();
		assert notificationInboundPortURI != null && !notificationInboundPortURI.isEmpty();

		this.stationId = stationID;
		this.position = position;
		this.actionInboundPortURI = actionInboundPortURI;
		this.notificationIBP = new TrafficLightNotificationInboundPort(notificationInboundPortURI, this);
		this.notificationIBP.publishPort();
		this.actionOBP = new TrafficLightActionOutboundPort(this);
		this.actionOBP.publishPort();
		this.cepbusOBP = new CEPBusManagementOutboundPort(this);
		this.cepbusOBP.publishPort();
		this.event_emissionOBP = new EventEmissionOutboundPort(this);
		this.event_emissionOBP.publishPort();
		this.action_executionIBP = new ActionExecutionInboundPort(this);
		this.action_executionIBP.publishPort();
	}

	// -------------------------------------------------------------------------
	// Component life-cycle
	// -------------------------------------------------------------------------

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#start()
	 */
	@Override
	public synchronized void start() throws ComponentStartException {
		super.start();

		try {
			this.doPortConnection(this.actionOBP.getPortURI(), this.actionInboundPortURI,
					TrafficLightActionConnector.class.getCanonicalName());
			this.doPortConnection(this.cepbusOBP.getPortURI(), Data.uri_cepbus_mip,
					ConnectorCEPbusManagement.class.getCanonicalName());
		
			
		} catch (Exception e) {
			throw new ComponentStartException(e);
		}
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#execute()
	 */
	@Override
	public synchronized void execute() throws Exception {
		/*String inboundportBus = this.cepbusOBP.registerEmitter(stationId);
		this.doPortConnection(this.event_emissionOBP.getPortURI(), inboundportBus, 
				ConnectorEventEmission.class.getCanonicalName());
		
		this.cepbusOBP.registerExecutor(this.stationId, this.actionInboundPortURI);	*/
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#finalise()
	 */
	@Override
	public synchronized void finalise() throws Exception {
		this.doPortDisconnection(this.actionOBP.getPortURI());
		this.doPortDisconnection(this.cepbusOBP.getPortURI());
		this.doPortDisconnection(this.event_emissionOBP.getPortURI());
	
		super.finalise();
	}

	/**
	 * @see fr.sorbonne_u.components.AbstractComponent#shutdown()
	 */
	@Override
	public synchronized void shutdown() throws ComponentShutdownException {
		try {
			this.actionOBP.unpublishPort();
			this.notificationIBP.unpublishPort();
			this.event_emissionOBP.unpublishPort();
			this.action_executionIBP.unpublishPort();
			this.cepbusOBP.unpublishPort();
		} catch (Exception e) {
			throw new ComponentShutdownException(e);
		}
		super.shutdown();
	}

	// -------------------------------------------------------------------------
	// Component services implementation
	// -------------------------------------------------------------------------

	/**
	 * @see fr.sorbonne_u.cps.smartcity.interfaces.TrafficLightNotificationImplI#vehiclePassage(java.lang.String,
	 *      fr.sorbonne_u.cps.smartcity.grid.Direction, java.time.LocalTime)
	 */
	@Override
	public  void execute(ActionI a , Serializable[] params) throws Exception{
		
		assert a instanceof FireStationActions;
		
	}
	@Override
	public void vehiclePassage(String vehicleId, Direction d, LocalTime occurrence) throws Exception {
		if (Data.DEBUG)
			System.out.println("Traffic light at " + this.position + " receives the notification of the passage of "
				+ vehicleId + " in the direction of " + d + " at " + occurrence);
	}
}
// -----------------------------------------------------------------------------
