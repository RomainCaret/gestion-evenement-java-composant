package correlator.classe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import components.Correlator;
import components.interfaces.FireStationActions;
import correlator.interfaces.FireCorrelatorStateI;
import descriptor.BasicSimSmartCityDescriptor;
import descriptor.Data;
import descriptor.SmartCityDescriptorSmall;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.SmartCityDescriptor;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfFirefightingResource;
import rule.interfaces.CorrelatorStateI;

public class FireCorrelatorState implements CorrelatorStateI, FireCorrelatorStateI {

	/**
	 * FireCorrelatorState est le constructeur de la classe FireCorrelatorState
	 * 
	 * @param localisation de type AbsolutePosition
	 */

	private AbsolutePosition localisation;
	private boolean ladder_available = true;
	private boolean truck_available = true;
	private String exec_uri;
	private Correlator correlator;

	public FireCorrelatorState(AbsolutePosition localisation, String exec_uri) {
		this.localisation = localisation;
		this.exec_uri = exec_uri;
	}

	/**
	 * getExec_uri est une méthode qui permet de récupérer l'URI de l'exécuteur
	 * 
	 * @return exec_uri de type String
	 */

	public String getExec_uri() {
		return exec_uri;
	}

	/**
	 * getLocalisation est une méthode qui permet de récupérer la localisation d'un
	 * événement, elle retourne une AbsolutePosition
	 * 
	 * @return AbsolutePosition
	 */
	public AbsolutePosition getLocalisation() {
		return localisation;
	}

	/**
	 * isLadder_available est une méthode qui test les disponibilités des
	 * échelles,elle retourne un booléen
	 * 
	 * @return boolean
	 */

	public boolean isLadder_available() {
		return ladder_available;
	}

	/**
	 * setLadder_available est une méthode qui modifie l'état de disponibilité des
	 * échelles, elle prend en paramètre un boolean
	 * 
	 * @param medecin_available
	 */

	public void setLadder_available(boolean ladder_available) {
		this.ladder_available = ladder_available;
	}

	/**
	 * isTruck_available est une méthode qui test les disponibilités des camions,
	 * elle retourne un boolean
	 * 
	 * @return boolean
	 */

	public boolean isTruck_available() {
		return truck_available;

	}

	/**
	 * setTruck_available est une méthode qui modifie l'état de disponibilité des
	 * camions, elle prend en paramètre un boolean
	 * 
	 * @param truck_available
	 */

	public void setTruck_available(boolean truck_available) {
		this.truck_available = truck_available;
	}

	/**
	 * getBarracks_nearby_available est une méthode qui permet de tester s'il y'a
	 * une caserne proche non sollicité par l'événement
	 * 
	 * @param e de type EventI
	 * @return boolean
	 */

	public String getBarracks_nearby_available(EventI e) {
		ArrayList<String> list_Barrack_Deja_Visit;
		list_Barrack_Deja_Visit = (ArrayList<String>) e.getPropertyValue("BarrackDejaVisit");
		if (list_Barrack_Deja_Visit == null)
			list_Barrack_Deja_Visit = new ArrayList<String>();
		double distance_min = -1;
		String samu_min = "";
		double distance;

		if (Data.numeroCVM == 3) {
			Iterator<String> iterStation = SmartCityDescriptorSmall.createFireStationIdIterator();
			while (iterStation.hasNext()) {
				String id = iterStation.next();
				if (list_Barrack_Deja_Visit.contains(id) == false && exec_uri.equals(id) == false) {
					distance = SmartCityDescriptorSmall.distance(exec_uri, id);
					if (distance < distance_min || distance_min == -1) {
						distance_min = distance;
						samu_min = id;
					}
				}
			}
		}

		if (Data.numeroCVM == 2) {
			Iterator<String> iterStation = SmartCityDescriptor.createFireStationIdIterator();
			while (iterStation.hasNext()) {
				String id = iterStation.next();
				if (list_Barrack_Deja_Visit.contains(id) == false && exec_uri.equals(id) == false) {
					distance = SmartCityDescriptor.distance(exec_uri, id);
					if (distance < distance_min || distance_min == -1) {
						distance_min = distance;
						samu_min = id;
					}
				}
			}
		}
		if (Data.numeroCVM == 1) {
			Iterator<String> iterStation = BasicSimSmartCityDescriptor.createFireStationIdIterator();
			while (iterStation.hasNext()) {
				String id = iterStation.next();
				if (list_Barrack_Deja_Visit.contains(id) == false && exec_uri.equals(id) == false) {
					distance = BasicSimSmartCityDescriptor.distance(exec_uri, id);
					if (distance < distance_min || distance_min == -1) {
						distance_min = distance;
						samu_min = id;
					}
				}
			}
		}
		if (distance_min == -1)
			return null;
		return samu_min;
	}

	public void triggerHouseFirstAlarm(AbsolutePosition position) throws Exception {

		Serializable[] args = { position, TypeOfFirefightingResource.StandardTruck };
		this.correlator.execute(FireStationActions.SendFirstAlarm, args, this.exec_uri);
	}

	public void triggerBuildingFirstAlarm(AbsolutePosition position) throws Exception {
		if (Data.DEBUG)
			System.out.println("FireCorrelatorTrace : (triggerBuildingFirstAlarm) : this.correlator : "+this.correlator);
		Serializable[] args = { position, TypeOfFirefightingResource.HighLadderTruck };
		
		this.correlator.execute(FireStationActions.SendFirstAlarm, args, this.exec_uri);
	}

	public void triggerFireGeneralAlarm(AbsolutePosition position) throws Exception {
		Serializable[] args = { position };
		this.correlator.execute(FireStationActions.SendGeneralAlarm, args, this.exec_uri);
	}

	public void triggerFireSecondAlarm(AbsolutePosition position) throws Exception {
		Serializable[] args = { position, TypeOfFirefightingResource.StandardTruck };
		this.correlator.execute(FireStationActions.SendSecondAlarm, args, this.exec_uri);
	}

	public void sendEvent(EventI e) {
		this.correlator.sendEvent(e);
	}

	/**
	 * inZone est une méthode qui prend en paramètre une position et qui renvoi un
	 * boolean
	 * 
	 * @param p1 de type AbsolutePosition
	 * @return boolean
	 */

	public boolean inZone(AbsolutePosition p1) {

		if (Data.numeroCVM == 1)
			return BasicSimSmartCityDescriptor.dependsUpon(p1, exec_uri);

		if (Data.numeroCVM == 2)
			return SmartCityDescriptor.dependsUpon(p1, exec_uri);

		if (Data.numeroCVM == 3)
			return SmartCityDescriptorSmall.dependsUpon(p1, exec_uri);
		System.out.println("ERREUR DE FOU (inZone FireCorrelator)");
		return false;
	}

	@Override
	public void setCorrelator(Correlator correlator) {
		this.correlator = correlator;
	}

}
