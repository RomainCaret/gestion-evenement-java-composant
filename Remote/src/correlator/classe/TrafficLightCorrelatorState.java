package correlator.classe;

import java.util.Iterator;

import components.Correlator;
import correlator.interfaces.TrafficLightCorrelatorStateI;
import descriptor.BasicSimSmartCityDescriptor;
import descriptor.Data;
import descriptor.SmartCityDescriptor;
import descriptor.SmartCityDescriptorSmall;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.grid.Direction;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;
import rule.interfaces.CorrelatorStateI;

public class TrafficLightCorrelatorState implements CorrelatorStateI, TrafficLightCorrelatorStateI {

	/**
	 * TrafficLightCorrelatorState est le constructeur de la classe
	 * TrafficLightCorrelatorState
	 * 
	 * @param localisation de type AbsolutePosition
	 * 
	 */

	private AbsolutePosition localisation;
	private String exec_uri;
	private TypeOfTrafficLightPriority typeOfTrafficLight;
	private Correlator correlator;

	public TrafficLightCorrelatorState(AbsolutePosition localisation, String exec_uri) {
		this.localisation = localisation;
		this.exec_uri = exec_uri;
	}

	/**
	 * getExec_uri est une méthode qui permet de récupérer l'URI de l'exécuteur
	 * 
	 * @return String
	 */

	public String getExec_uri() {
		return exec_uri;
	}

	/**
	 * getLocalisation est une méthode qui permet de récupérer la localisation d'un
	 * événement, elle retourne une AbsolutePosition
	 * 
	 * @return AbsolutePosition
	 */
	public AbsolutePosition getLocalisation() {
		return localisation;
	}

	public void sendEvent(EventI e) {
	}

	public void setIntersection(TypeOfTrafficLightPriority typeOfTrafficLight) {
		this.typeOfTrafficLight = typeOfTrafficLight;
	}

	public IntersectionPosition getIntersection() {
		return (IntersectionPosition) localisation;
	}

	public AbsolutePosition getNextIntersection(Direction d) {
		double locX = localisation.getX();
		double locY = localisation.getY();

		if (d == null)
			return null;
		switch (d) {
			case E:
				locX++;
				break;
			case N:
				locY--;
				break;
			case S:
				locY++;
				break;
			case W:
				locX--;
				break;
		}

		
		if (Data.numeroCVM == 3) {
			Iterator<IntersectionPosition> trafficLightsIterator = SmartCityDescriptorSmall
					.createTrafficLightPositionIterator();
			while (trafficLightsIterator.hasNext()) {
				IntersectionPosition p = trafficLightsIterator.next();
				if (p.getX() == locX && p.getY() == locY)
						return p;
			}
			return null;
		}
		if (Data.numeroCVM == 2) {
			Iterator<IntersectionPosition> trafficLightsIterator = SmartCityDescriptor
					.createTrafficLightIdIterator();
			while (trafficLightsIterator.hasNext()) {
				IntersectionPosition p = trafficLightsIterator.next();
				if (p.getX() == locX && p.getY() == locY)
						return p;
			}
			return null;
		}
		if (Data.numeroCVM == 1) {
			Iterator<IntersectionPosition> trafficLightsIterator = BasicSimSmartCityDescriptor
					.createTrafficLightIdIterator();
			while (trafficLightsIterator.hasNext()) {
				IntersectionPosition p = trafficLightsIterator.next();
				if (p.getX() == locX && p.getY() == locY)
						return p;
			}
			return null;
		}
		return null;
	}

	/**
	 * inZone est une méthode qui prend en paramètre une position et qui renvoi un
	 * boolean
	 * 
	 * @param p1 de type AbsolutePosition
	 * @return boolean
	 */

	public boolean inZone(AbsolutePosition p1) {
		return true;
	}

	@Override
	public void setCorrelator(Correlator correlator) {
		this.correlator = correlator;

	}

}
