package correlator.classe;

import components.Correlator;
import components.interfaces.SamuStationActions;
import correlator.interfaces.HealthCorrelatorStateI;
import descriptor.BasicSimSmartCityDescriptor;
import descriptor.Data;
import descriptor.SmartCityDescriptorSmall;
import fr.sorbonne_u.cps.smartcity.SmartCityDescriptor;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfSAMURessources;
import rule.interfaces.CorrelatorStateI;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class HealthCorrelatorState implements CorrelatorStateI, HealthCorrelatorStateI {

	/**
	 * HealthCorrelatorState est le constructeur de la classe HealthCorrelatorState
	 * 
	 * @param localisation de type AbsolutePosition
	 */
	public static final double ZONE = 4;
	private AbsolutePosition localisation;
	private boolean medecin_available = true;
	private boolean samu_available = true;
	private Correlator correlator;
	private String exec_uri;

	public HealthCorrelatorState(AbsolutePosition localisation, String exec_uri) {
		this.localisation = localisation;
		this.exec_uri = exec_uri;
	}

	/**
	 * getExec_uri est une méthode qui permet de récupérer l'URI de l'exécuteur
	 * 
	 * @return String
	 */

	public String getExec_uri() {
		return exec_uri;
	}

	/**
	 * getLocalisation est une méthode qui permet de récupérer la localisation d'un
	 * événement, elle retourne une AbsolutePosition
	 * 
	 * @return AbsolutePosition
	 */
	public AbsolutePosition getLocalisation() {
		return localisation;
	}

	/**
	 * isMedecin_available est une méthode qui test les disponibilités des médecins,
	 * elle retourne un boolean
	 * 
	 * @return boolean
	 */

	public boolean isMedecin_available() {
		return medecin_available;
	}

	/**
	 * setMedecin_available est une méthode qui modifie l'état de disponibilité des
	 * médecins, elle prend en paramètre un boolean
	 * 
	 * @param medecin_available
	 */

	public void setMedecin_available(boolean medecin_available) {
		this.medecin_available = medecin_available;
	}

	/**
	 * isSamu_available est une méthode qui test les disponibilités du SAMU, elle
	 * retourne un boolean
	 * 
	 * @return boolean
	 */

	public boolean isSamu_available() {
		return samu_available;
	}

	/**
	 * setMedecin_available est une méthode qui modifie l'état de disponibilité du
	 * SAMU, elle prend en paramètre un boolean
	 * 
	 * @param samu_available
	 */

	public void setSamu_available(boolean samu_available) {
		this.samu_available = samu_available;
	}

	/**
	 * getSamu_nearby_available est une méthode qui permet de tester s'il y'a un
	 * Samu proche non sollicité par l'événement
	 * 
	 * @param e de type EventI
	 * @return boolean
	 */

	public String getSamu_nearby_available(EventI e) {
		ArrayList<String> list_Samu_Deja_Visit;
		list_Samu_Deja_Visit = (ArrayList<String>) e.getPropertyValue("SamuDejaVisit");
		double distance_min = -1;
		String samu_min = "";
		double distance;

		if (Data.numeroCVM == 3) {
			Iterator<String> iterStation = SmartCityDescriptorSmall.createSAMUStationIdIterator();
			while (iterStation.hasNext()) {
				String id = iterStation.next();
				if ((list_Samu_Deja_Visit == null || list_Samu_Deja_Visit.contains(id) == false) && exec_uri.equals(id) == false) {
					distance = SmartCityDescriptorSmall.distance(exec_uri, id);
					if (distance < distance_min || distance_min == -1) {
						distance_min = distance;
						samu_min = id; 
					}
				}
			}
		}

		if (Data.numeroCVM == 2) {
			Iterator<String> iterStation = SmartCityDescriptor.createSAMUStationIdIterator();
			while (iterStation.hasNext()) {
				String id = iterStation.next();
				if ((list_Samu_Deja_Visit == null || list_Samu_Deja_Visit.contains(id) == false) && exec_uri.equals(id) == false) {
					distance = SmartCityDescriptor.distance(exec_uri, id);
					if (distance < distance_min || distance_min == -1) {
						distance_min = distance;
						samu_min = id;
					}
				}
			}
		}
		if (Data.numeroCVM == 1) {
			Iterator<String> iterStation = BasicSimSmartCityDescriptor.createSAMUStationIdIterator();
			while (iterStation.hasNext()) {
				String id = iterStation.next();
				if ((list_Samu_Deja_Visit == null || list_Samu_Deja_Visit.contains(id) == false) && exec_uri.equals(id) == false) {
					distance = BasicSimSmartCityDescriptor.distance(exec_uri, id);
					if (distance < distance_min || distance_min == -1) {
						distance_min = distance;
						samu_min = id;
					}
				}
			}
		}
		if (distance_min == -1)
			return null;
		return samu_min;
	}

	public void triggerIntervention(AbsolutePosition position, String personID, TypeOfSAMURessources ambulance)
			throws Exception {
		if (personID == null)
			personID = "";
		Serializable[] args = { position, personID, ambulance };
		this.correlator.execute(SamuStationActions.SendHealthIntervention, args, this.exec_uri);
	}

	public void sendEvent(EventI e) {
		this.correlator.sendEvent(e);
	}

	/**
	 * inZone est une méthode qui prend en paramètre une position et qui renvoi un
	 * boolean
	 * 
	 * @param p1 de type AbsolutePosition
	 * @return boolean
	 */

	public boolean inZone(AbsolutePosition p1) {

		if (Data.numeroCVM == 1)

			return BasicSimSmartCityDescriptor.dependsUpon(p1, exec_uri);

		if (Data.numeroCVM == 2)

			return SmartCityDescriptor.dependsUpon(p1, exec_uri);

		if (Data.numeroCVM == 3)

			return SmartCityDescriptorSmall.dependsUpon(p1, exec_uri);
		System.out.println("ERREUR DE FOU (inZone HealthCorrelator)");
		return false;
	}

	@Override
	public void setCorrelator(Correlator correlator) {
		this.correlator = correlator;

	}

}
