package correlator.interfaces;

import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;

public interface FireCorrelatorStateI {

	public AbsolutePosition getLocalisation();

	public boolean isLadder_available();

	public void setLadder_available(boolean ladder_available);

	public boolean isTruck_available();

	public void setTruck_available(boolean truck_available);
	
	public String getBarracks_nearby_available(EventI e);

	public void triggerHouseFirstAlarm(AbsolutePosition position) throws Exception;

	public void triggerBuildingFirstAlarm(AbsolutePosition position) throws Exception;

	public void triggerFireSecondAlarm(AbsolutePosition position) throws Exception;
	
	public void triggerFireGeneralAlarm(AbsolutePosition position) throws Exception;

	public void sendEvent(EventI e);

	public boolean inZone(AbsolutePosition p1);
}
