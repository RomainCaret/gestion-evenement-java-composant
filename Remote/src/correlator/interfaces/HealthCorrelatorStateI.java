package correlator.interfaces;

import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfSAMURessources;

public interface HealthCorrelatorStateI {

	public AbsolutePosition getLocalisation();

	public boolean isMedecin_available();

	public void setMedecin_available(boolean medecin_available);

	public boolean isSamu_available();

	public void setSamu_available(boolean samu_available);

	public String getSamu_nearby_available(EventI e);

	public void triggerIntervention(AbsolutePosition position, String personID, TypeOfSAMURessources ambulance)
	throws Exception;

	public void sendEvent(EventI e);

	public boolean inZone(AbsolutePosition p1);
}
