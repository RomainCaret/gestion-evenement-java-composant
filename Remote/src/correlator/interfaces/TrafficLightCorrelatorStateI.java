package correlator.interfaces;

import components.Correlator;
import event.interfaces.EventI;
import fr.sorbonne_u.cps.smartcity.grid.AbsolutePosition;
import fr.sorbonne_u.cps.smartcity.grid.Direction;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.interfaces.TypeOfTrafficLightPriority;

public interface TrafficLightCorrelatorStateI {

	public AbsolutePosition getLocalisation();

	public void setIntersection(TypeOfTrafficLightPriority typeOfTrafficLight);

	public void sendEvent(EventI e);

	public IntersectionPosition getIntersection();

	public AbsolutePosition getNextIntersection(Direction d);

	public boolean inZone(AbsolutePosition p1);

	public void setCorrelator(Correlator correlator);
}
