package dcvm;


// Copyright Jacques Malenfant, Sorbonne Universite.
// Jacques.Malenfant@lip6.fr
//
// This software is a computer program whose purpose is to provide a
// basic component programming model to program with components
// distributed applications in the Java programming language.
//
// This software is governed by the CeCILL-C license under French law and
// abiding by the rules of distribution of free software.  You can use,
// modify and/ or redistribute the software under the terms of the
// CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
// URL "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL-C license and that you accept its terms.

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.AbstractPort;
import fr.sorbonne_u.cps.smartcity.grid.IntersectionPosition;
import fr.sorbonne_u.cps.smartcity.utils.TimeManager;
import rule.interfaces.CorrelatorStateI;

import java.io.File;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import components.CEPBus;
import components.Correlator;
import components.emitterExec.FireStationEmitterExecuteur;
import components.emitterExec.SAMUStationEmitterExecuteur;
import components.emitterExec.TrafficLightEmitterExecuteur;
import correlator.classe.FireCorrelatorState;
import correlator.classe.HealthCorrelatorState;
import correlator.classe.TrafficLightCorrelatorState;
import descriptor.Data;
import descriptor.SmartCityDescriptorSmall;
import fr.sorbonne_u.components.AbstractComponent;

// -----------------------------------------------------------------------------
/**
 * The class <code>DCVM</code> implements a distributed example of replication
 * to dispatch calls among servers.
 *
 * <p><strong>Description</strong></p>
 * 
 * <p><strong>Invariant</strong></p>
 * 
 * <pre>
 * invariant	true
 * </pre>
 * 
 * <p>Created on : 2020-03-02</p>
 * 
 * @author	<a href="mailto:Jacques.Malenfant@lip6.fr">Jacques Malenfant</a>
 */
public class			SmartCityDCVM
extends		AbstractSmartCityDCVM
{
	public				SmartCityDCVM(String[] args) throws Exception
	{
		super(args);
	}

	/**
	 * @see fr.sorbonne_u.components.cvm.AbstractDistributedCVM#instantiateAndPublish()
	 */
	@Override
	public void			instantiateAndPublish() throws Exception
	{
		if (thisJVMURI.equals("JVM1")) {
			System.out.println("JVM1");
			ArrayList<String> tab_samu_id = new ArrayList<String>();
			ArrayList<String> tab_samu_id_unique = new ArrayList<String>();
	
			ArrayList<String> tab_firestation_id = new ArrayList<String>();
			ArrayList<String> tab_firestation_id_unique = new ArrayList<String>();
			
			ArrayList<String> tab_traffic_id = new ArrayList<String>();
			ArrayList<String> tab_traffic_id_unique = new ArrayList<String>();
	
			ArrayList<CorrelatorStateI> cs_samu = new ArrayList<CorrelatorStateI>();
			ArrayList<CorrelatorStateI> cs_firestation = new ArrayList<CorrelatorStateI>();
			ArrayList<CorrelatorStateI> cs_traffic = new ArrayList<CorrelatorStateI>();
			HashMap<String, CorrelatorStateI> map_state_exec = new HashMap<String, CorrelatorStateI>();
			Data.numeroCVM = 3;
	
	
			// create an iterator over valid fire station identifiers, which in turn
			// allow to perform operations on the smart city descriptor to get
			// information about them
			AbstractComponent.createComponent(CEPBus.class.getCanonicalName(), new Object[] { Data.uri_cepbus1,0 });
			AbstractComponent.createComponent(CEPBus.class.getCanonicalName(), new Object[] { Data.uri_cepbus2,1 });
			
			if (Data.test == true)
				AbstractComponent.createComponent(CEPBus.class.getCanonicalName(), new Object[] { Data.uri_cepbus3,2 });
			
	
			Iterator<String> fireStationIdsIterator = SmartCityDescriptorSmall.createFireStationIdIterator();
			
			int num_cepbus = 0;
			while (fireStationIdsIterator.hasNext()) {
				String fireStationId = fireStationIdsIterator.next();
				// generate an inbound port URI to be used by the facade component
				// and passed to the proxy components
				String notificationInboundPortURI = AbstractPort.generatePortURI();
				// register the notification inbound port URI to be used when
				// creating proxy components
				tab_firestation_id.add(fireStationId);
				cs_firestation.add(new FireCorrelatorState(SmartCityDescriptorSmall.getPosition(fireStationId), fireStationId));
				this.register(fireStationId, notificationInboundPortURI);
				
				
				// create the facade component for a fire station, passing the
				// notification inbound port URI to be used to create its port
				// and the services inbound port URI of the proxy component to
				// connect its service outbound port properly to the proxy
				AbstractComponent.createComponent(FireStationEmitterExecuteur.class.getCanonicalName(), new Object[] { fireStationId,
						notificationInboundPortURI, SmartCityDescriptorSmall.getActionInboundPortURI(fireStationId),num_cepbus});
				num_cepbus = (num_cepbus +1)%Data.nb_bus;
	//			AbstractComponent.createComponent(FireStationFacade.class.getCanonicalName(), new Object[] { fireStationId,
		//				notificationInboundPortURI, BasicSimSmartCityDescriptor.getActionInboundPortURI(fireStationId) });
			}
			num_cepbus = 0;
			Iterator<String> samuStationsIditerator = SmartCityDescriptorSmall.createSAMUStationIdIterator();
			while (samuStationsIditerator.hasNext()) {
				String samuStationId = samuStationsIditerator.next();
				tab_samu_id.add(samuStationId);
				cs_samu.add(new HealthCorrelatorState(SmartCityDescriptorSmall.getPosition(samuStationId), samuStationId));
	
				String notificationInboundPortURI = AbstractPort.generatePortURI();
				this.register(samuStationId, notificationInboundPortURI);
				//AbstractComponent.createComponent(SAMUStationFacade.class.getCanonicalName(), new Object[] { samuStationId,
					//	notificationInboundPortURI, BasicSimSmartCityDescriptor.getActionInboundPortURI(samuStationId) });
				AbstractComponent.createComponent(SAMUStationEmitterExecuteur.class.getCanonicalName(), new Object[] { samuStationId,
						notificationInboundPortURI, SmartCityDescriptorSmall.getActionInboundPortURI(samuStationId),num_cepbus });
				num_cepbus = (num_cepbus +1)%Data.nb_bus;
			}
	
			Iterator<IntersectionPosition> trafficLightsIterator = SmartCityDescriptorSmall
					.createTrafficLightPositionIterator();
			while (trafficLightsIterator.hasNext()) {
				IntersectionPosition p = trafficLightsIterator.next();
				tab_traffic_id.add(p.toString());
				cs_traffic.add(new TrafficLightCorrelatorState(p,p.toString()));
				
				String notificationInboundPortURI = AbstractPort.generatePortURI();
				this.register(p.toString(), notificationInboundPortURI);
	//			AbstractComponent.createComponent(TrafficLightFacade.class.getCanonicalName(), new Object[] { p,
		//				notificationInboundPortURI, BasicSimSmartCityDescriptor.getActionInboundPortURI(p), p.toString() });
				AbstractComponent.createComponent(TrafficLightEmitterExecuteur.class.getCanonicalName(), new Object[] { p,
						notificationInboundPortURI, SmartCityDescriptorSmall.getActionInboundPortURI(p), p.toString(),1 });
			}
			
			String nomcorr = "uri_correlator";
			int cpt_corr = 0;
			num_cepbus = 0;
			for (int i = 0; i < tab_samu_id.size(); i++) {
				
				map_state_exec.put(tab_samu_id.get(i), cs_samu.get(i));
				tab_samu_id_unique.add(tab_samu_id.get(i));
				AbstractComponent.createComponent(Correlator.class.getCanonicalName(),
						new Object[] { nomcorr+cpt_corr, tab_samu_id_unique.clone(), map_state_exec.clone(),num_cepbus});
				map_state_exec.clear();
				tab_samu_id_unique.clear();
				num_cepbus = (num_cepbus +1)%Data.nb_bus;
				cpt_corr++;
			}
	
			num_cepbus = 0;
			for (int i = 0; i < tab_firestation_id.size(); i++) {
				map_state_exec.put(tab_firestation_id.get(i), cs_firestation.get(i));
				tab_firestation_id_unique.add(tab_firestation_id.get(i));
				AbstractComponent.createComponent(Correlator.class.getCanonicalName(),
						new Object[] { nomcorr+cpt_corr, tab_firestation_id_unique.clone(), map_state_exec.clone(),num_cepbus });// Création du composant FireStationCorrelateur
				map_state_exec.clear();
				tab_firestation_id_unique.clear();
				num_cepbus = (num_cepbus +1)%Data.nb_bus;
				cpt_corr++;
			}
			/*
			for (int i = 0; i < tab_traffic_id.size(); i++) {
				map_state_exec.put(tab_traffic_id.get(i), cs_traffic.get(i));
				tab_traffic_id_unique.add(tab_traffic_id.get(i));
				tab_traffic_id_unique.addAll(tab_samu_id);
				tab_traffic_id_unique.addAll(tab_firestation_id);
				
				AbstractComponent.createComponent(Correlator.class.getCanonicalName(),
						new Object[] { nomcorr+cpt_corr, tab_traffic_id_unique.clone(), map_state_exec.clone(),2 });// Création du composant TrafficLightCorrelateur
				map_state_exec.clear();
				tab_traffic_id_unique.clear();
				cpt_corr++;
			}*/



		} else if (thisJVMURI.equals("JVM2")) {
			
			System.out.println("JVM2");
			
			if (Data.test == false)
				AbstractComponent.createComponent(CEPBus.class.getCanonicalName(), new Object[] { Data.uri_cepbus3,2 });
			
		} else if (thisJVMURI.equals("JVM3")) {
			
			System.out.println("JVM3");
		}
		super.instantiateAndPublish();
	}

	public static void main(String[] args)
	{
		try {
			System.out.println("DCVM");
			
            // start time, in the logical time view; the choice is arbitrary
			simulatedStartTime = LocalTime.of(12, 0);
			// end time, in the logical time view; the chosen value must allow
			// the whole test scenario to be executed within the logical time
			// period between the start and the end times; the actual duration
			// of the program execution also depends upon the acceleration
			// factor defined in the class TimeManager
			simulatedEndTime = LocalTime.of(12, 0).plusMinutes(30);
			
			SmartCityDCVM dcvm = new SmartCityDCVM(args);
			dcvm.startStandardLifeCycle(START_DELAY + TimeManager.get().computeExecutionDuration());
			Thread.sleep(10000L);
			System.exit(0);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
// -----------------------------------------------------------------------------
