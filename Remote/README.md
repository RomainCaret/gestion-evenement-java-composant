# Remote

Projet CPS

## Lancer le projet

Pour lancer le projet, lancer la commande ./start-all
Elle va lancer les 3 JVM simultanément avec les BUS.

## Explication de l'éxecution

L'éxécution se passe ainsi :
On a les expected rules qui sont écrites par les proxy et qui sont donc les règles attendues et
l'application des rules réelles qui sont exécutées par notre base de rule. Cela permet d'observer rapidement si l'execution est correct.
Certaines rules (S16 à S20 ou F15 à F20) sont des rules lancées par les proxy lorqu'il n'y a plus d'ambulances ou d'echelles et ne sont donc
pas dans les expected des proxys (bien que ce soit des rules attendues)

Dans Data, on peut changer Data.test à la valeur false pour avoir une éxecution avec un bus dans une autre JVM et une visualisation des passages entre les bus
du système. De base sa valeur est à true car les 3 bus se situent dans la même JVM. 
